#!/usr/bin/python3
# -*- coding: utf-8 -*-
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import sys, getopt, re, locale, collections
from collections import deque
from sys import modules

# Определение автономного запуска
if "rdflib" in modules: autonomous = False # Если rdflib уже импртирован, то вход был из другого скрипта
else: autonomous = True # Инициализация настройки. При вызове из другого модуля передается копия settings оттуда

try: # Локализация
    import gettext
    syslanguage, syscodepage = locale.getdefaultlocale(['LC_ALL', 'LANG'])
    if not syslanguage: syslanguage = 'en' # Язык по умолчанию, если системная переменная не установлена
    syslanguage = re.sub('_.*', '', syslanguage)
    try:
        filtrans = gettext.translation('rdfilter'); filtrans.install()
    except FileNotFoundError:
        try: # Наверно это Windows
            filtrans = gettext.translation('rdfilter', localedir='locale', languages=[syslanguage]); filtrans.install()
        except FileNotFoundError:
            if not syslanguage == 'ru': print ("Gettext localization file locale/%s/LC_MESSAGES/rdfilter.mo not available." % syslanguage)
            def _(i): return i
except ModuleNotFoundError:
    def _(i): return i # Заглушка для функции локализации при отсутствии gettext
    print (_("Для поддержки перевода интерфейса необходим модуль Gettext."))

try:
    import tkinter as tk
    import tkinter.font as tkFont
    from tkinter import *
    from tkinter import ttk
    from tkinter import scrolledtext
    from tkinter import messagebox
    from tkinter import filedialog
    if autonomous:
        fltwin = tk.Tk()
        fltwin.withdraw()
except ModuleNotFoundError:
    fltwin = None
    print (_("Для работы программы в интерактивном режиме необходим модуль Tkinter."))

try:
    import rdflib
    from rdflib import URIRef
    from rdflib.term import Literal
    from rdflib.namespace import XSD
except ModuleNotFoundError:
    print(_("Для работы программы необходим модуль rdflib для Python\nУстановка:\npip (--user) install rdflib"))
    exit()

try:
    import graphviz
    from graphviz import *

except ModuleNotFoundError:
    pass

try:
    import json
except ModuleNotFoundError:
    pass

try:
    import os
#    os.environ["PATH"] += os.pathsep + '/usr/bin/dot'
except ModuleNotFoundError:
    pass
#from appdirs import * # https://pypi.org/project/appdirs/1.2.0/

try: # Программа сравнения графов
    import rdfcomp
except ModuleNotFoundError:
    print (_("Не найден файл программы сравнения графов rdfcomp.py"))

#settings # Сохраняемые в файл параметры. Заполняется в init_settings
#rtparam # Определяемые при запуске параметры
#context # Набор

########################
def init_settings(settings, rtparam): # Создать полный набор параметров по умолчанию
    settings.clear()
    settings.update({'formats': ['turtle', 'nt', 'xml', 'pretty-xml', 'etalog']}) # Список поддерживаемых форматов RDF
    settings.update({'format': 'turtle'}) # Формат RDF по умолчанию для rdflib (turtle, n3...)
    settings.update({'fontscale': 1}) # Масштаб ширифта

    rtparam.clear()
    if "os" in modules:
        rtparam.update({'username': os.getlogin()}) # При запуске из другого модуля имя д.б. уже в переданных settings
        rtparam.update({'cfgfile': "semtools_settings_%s.json" % rtparam['username']}) # При запуске из другого модуля имя д.б. уже в переданных settings
    else:
        message('err', _("Не удается определить текущее имя пользователя. Возможны проблемы из-за конфликтов имен временных файлов, если несколько человек запустят программу одновременно."))
        rtparam.update({'username': ''}) # Имя пользователя в системе для имен врем. файлов настроек. Определяется позже.
        rtparam.update({'cfgfile': "semtools_settings.json"}) # Имя файла настроек. Определяется позже.

    if "graphviz" in modules: rtparam.update({'have_graphviz': True})
    else: rtparam.update({'have_graphviz': False})

    if "etalog" in modules: rtparam.update({'have_etalog': True})
    else: rtparam.update({'have_etalog': False})

    if "rdfcomp" in modules: rtparam.update({'have_rdfcomp': True})
    else: rtparam.update({'have_rdfcomp': False})

    rtparam.update({'have_rdfilter': True})

    if "tkinter" in modules:
        rtparam.update({'rdfcomp_guimode': 'True'})
        rtparam.update({'rdfilter_guimode': 'True'})
    else:
        rtparam.update({'rdfcomp_guimode': 'False'})
        rtparam.update({'rdfilter_guimode': 'False'})
    rtparam.update({'rdfcomp_cmpmode': 'standard'})  # (standard, eraseind, ignoreind) Режим сравнения по умолчанию
    #rtparam.update({'syslanguage': 'en'}) # Язык по умолчанию. Определяется позже.
    #rtparam.update({'syscodepage': 'UTF-8'}) # Кодировка по умолчанию. Определяется позже.
    rtparam.update({'sems': 'all'}) # all/bsems/isems/esems Какую часть графа от ЭТАП4 использовать Basic и Intermediate может не быть совсем

def fill_missing_settings(settings, rtparam): # Восполнить отсутствующие параметры умолчаниями
    defsettings = {}; defrtparam = {}
    init_settings(defsettings, defrtparam)
    for p in defsettings:
        if not p in settings: settings.update({p: defsettings[p]})
    for p in defrtparam:
        if not p in rtparam: rtparam.update({p: defrtparam[p]})

def save_settings(settings, rtparam): # Записать конфигурацию в файл
    with open(rtparam['cfgfile'], 'w') as configfile:
        json.dump(settings, configfile, ensure_ascii=False, indent=4, sort_keys=True)

def read_settings(settings, rtparam): # Прочесть конфигурацию из файла
    with open(rtparam['cfgfile']) as configfile:
        settings = json.load(configfile)

#########################

global fdict; fdict = {} # словарь полных URI (ОБЪЕКТЫ) для поиска (строится на основе filters и srcg['edict'] - словарь сущностей (СТРОКИ) в исходном графе), также нужен для подсветки в graphviz
global filters; filters = {} # (СТРОКИ) условия фильтрации графов в человекочитаемом виде



def message(type, msg): # Показать сообщение
    print (msg)
    if type == 'err': mb = messagebox.showerror(title=_("Ошибка"), message=msg)
    if type == 'inf': mb = messagebox.showinfo(title=None, message=msg)

def append_str(a, b, delim):
    if b:
        if a: return a+delim+b
        else: return b
    else: return a

def unquot(s):
    l = len(s)
    if l > 0:
        if s[l-1] == '"': s = s[:l-1]
        if s[0] == '"': s = s[1:]
    return s

def deep_update(source, overrides): # Дополнение многоуровневого словаря или подобной структуры. Источник модифицируется "по месту"
    for key, value in overrides.items():
        if isinstance(value, collections.abc.Mapping) and value:
            returned = deep_update(source.get(key, {}), value)
            source[key] = returned
        else:
            source[key] = overrides[key]
    return source

def RDFobj(nd): # Преобразование строки с полным именем RDF сущности в объект URIRef или XSD литерал для хранилища троек
    if re.search('#', nd): n = URIRef(nd)
    elif re.search('\^\^xsd\:', nd): # Есть xsd тип данных
        m = re.match('^(.*)\^\^xsd\:(.*)$', nd)
        n = Literal(unquot(m.group(1)), datatype='XSD.' + m.group(2))
    elif re.match('^[0-9]+$', nd): n = Literal(unquot(nd), datatype=XSD.integer) # Аварийный выход - нет ни префикса, ни xsd типа
    elif re.match('^[0-9\,\.]+$', nd): n = Literal(unquot(nd), datatype=XSD.float)
    else: n = Literal(unquot(nd), datatype=XSD.string)
    return n

def parseURI(uri): # Нарезка на префикс, имя класса и номер инивида
    ns = ""; pfx = ""; e = ""; i = ""
    uri = str(uri) # Объекты xsd String в текст

    p = re.match("(.*?)#(.*)", uri)
    if p:
        ns = p.group(1)
        uri = p.group(2)
    else:
        p = re.match("(.*?):(.*)", uri)
        if p:
            pfx = p.group(1)
            uri = p.group(2)

    p = re.match("(.*)_(\d+_\d+)$", uri)
    if not p: p = re.match("(.*)_(\d+)$", uri)
    if not p: p = re.match("(.*)_(\?[A-Z\d]*)$", uri, re.IGNORECASE)
    if p:
        e = p.group(1)
        i = p.group(2)
    else: e = uri
    return ns, pfx, e, i

def getprefix (g, q): # Получение префикса по namespace URI
    for pfx, ns in g['grf'].namespaces():
        ns  = re.sub('#$', '', ns)
        if q == ns: return pfx
    return ''

def makerdfdict(g): # Создание словаря сущностей полученного графа и сбор статистики
    cnodes = {}

    g['edict'].clear()
    for s,p,o in g['grf']:  # Перебор всех триплетов для сборки словаря сущностей  - глобальная переменная
        s = str(s); p = str(p); o = str(o) # Преобразование объектов RDF в строки для сравнения с вводимыми пользователем строками

        ns, pfx, e, i = parseURI(s)  # Здесь pfx всегда пустй
        deep_update(g['edict'], {'ent': {s: {'ns': ns, 'e': e, 'i': i }}}) # компоненты полного уникального имени инд.сущности и ее роль в триплете
        deep_update(g['edict'], {'cls': {e: {'ns': {ns: None}, 'i': {i: None}, 'ent': {s: None}}}})
        deep_update(g['edict'], {'ind': {i: {'e': {e: None}, 'ent': {s: None}}}})
        deep_update(g['edict'], {'iref': {append_str(e, i, '_'): {'ent': {s: None}}}})
        ns, pfx, e, i = parseURI(o)
        deep_update(g['edict'], {'ent': {o: {'ns': ns, 'e': e, 'i': i }}})
        deep_update(g['edict'], {'cls': {e: {'ns': {ns: None}, 'i': {i: None}, 'ent': {o: None}}}})
        deep_update(g['edict'], {'ind': {i: {'e': {e: None}, 'ent': {o: None}}}})
        deep_update(g['edict'], {'iref': {append_str(e, i, '_'): {'ent': {s: None}}}})
        ns, pfx, e, i = parseURI(p)
        deep_update(g['edict'], {'rls': {p: {'ns': ns, 'e': e, 'i': i }}})
        deep_update(g['edict'], {'rel': {e: {'ns': {ns: None}, 'rls': {p: None}}}})

        if p == 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type': cnodes.update({o: ''}) # Подсчет узлов обзначающих классы индивидов

    if 'ent' in g['edict']:
        for ind in g['edict']['ent']: # Дополняем префиксами
            e = g['edict']['ent'][ind]['e']
            pfx = getprefix(g, g['edict']['ent'][ind]['ns'])
            deep_update(g['edict'], {'ent': {ind: {'pf': pfx}}})
            deep_update(g['edict'], {'cls': {e: {'pf': {pfx: None}}}})
            deep_update(g['edict'], {'pfx': {pfx: {'e': {e: None}}}})

            # ent - полные URI всех узлов
            # rls - все типы связей
            # ind - индекс по числовым суффиксам индивидов
            # cls - индекс по именам узлов без суффикосов и префиксов (совпадают с именами классов нумерованных индивидов)
            # iref - индекс по именам индивидов с числовыми суффиксами, но без префиксов
            # pfx - префикс пространства имен, сокращение для ns - префиксной части URI

    g['stat'].clear()
    g['stat'].update({'triples': len(g['grf'])}) # число триплетов
    if g['stat']['triples'] > 0:
        g['stat'].update({'nodes': len(g['edict']['ent'])}) # число узлов
        g['stat'].update({'cnodes': len(cnodes)}) # число явно указанных классов
        g['stat'].update({'inodes': g['stat']['nodes'] - g['stat']['cnodes']}) # число индивидов

def bookmarksegments(g, fmt): # Определение наличия и запоминание начала и конца сегментов Базовой, Промежуточной, Расширенной Сем.С от ЭТАП4
    pos = 0
    txtend = len(g['txt'])
    if 'bmarks' in g: del g['bmarks'] # Стираем старые закладки
    if fmt != 'etalog' and fmt != 'turtle': return # Деление текста на сегменты комментариями работает только в эталоге и turtle
    else: deep_update(g, {'bmarks': {'ns': ''}})

    for line in g['txt'].splitlines(True):
        if fmt == 'turtle' and re.match(r'@*PREFIX', line):
            g['bmarks']['ns'] = append_str(g['bmarks']['ns'], line, '') # Сохраняем префиксы
        if re.match('(#|\/\/) *Basic semantic structure.*', line):
            deep_update(g, {'bmarks': {'bsems': {'start': pos, 'end': txtend}}})
            deep_update(g, {'bmarks': {'bisems': {'start': pos, 'end': txtend}}})
        if re.match('(#|\/\/) *Intermediate semantic structure.*', line):
            deep_update(g, {'bmarks': {'bsems': {'end': pos}}}) # БСемс идет перед ИСемс
            deep_update(g, {'bmarks': {'isems': {'start': pos, 'end': txtend}}})
            deep_update(g, {'bmarks': {'iesems': {'start': pos, 'end': txtend}}})
        if re.match('(#|\/\/) *Enhanced semantic structure.*', line):
            deep_update(g, {'bmarks': {'bisems': {'end': pos}}})
            deep_update(g, {'bmarks': {'isems': {'end': pos}}}) # БСемс идет перед ИСемс
            deep_update(g, {'bmarks': {'esems': {'start': pos, 'end': txtend}}})
            break
        pos = pos + len(line) # Не надо обрезать \n

def extractsegment(g, fmt, sems): # Извлечение части текста, соответствующей заданному сегменту Базовой, Промежуточной или Расширенной Сем.С от ЭТАП4
    if fmt != 'turtle' and fmt != 'etalog': return g['txt']
    if 'bmarks' in g:
        if sems in g['bmarks']:
            txt = ''
            if fmt == 'turtle' and 'ns' in g['bmarks']: txt = g['bmarks']['ns']
            txt += g['txt'][g['bmarks'][sems]['start']:g['bmarks'][sems]['end']]  # Вырезка подстроки по закладкам # g['bmarks'][sems]['start'] - g['bmarks'][sems]['end']
            return txt
        elif sems == 'all': return g['txt']
        else: # Если есть закладки, но требуемого сегмента нет - жалуемся
            if sems == 'bsems': message ('err', _("В графе не выделена базовая структура"))
            elif sems == 'isems' or sems == 'bisems' or sems == 'iesems': message ('err', _("В графе не выделена промежуточная структура"))
            else: message ('err', _("В графе не выделена запрошенная структура %s" % sems))
            return ''
    else: return g['txt']

def parserdf(g, fmt, sems, username): # Чтение RDF (разбираемый текст, формат, получаемый граф и его словарь)
    g['grf'].remove( (None, None, None) ) # Стирание старого
    bookmarksegments(g, fmt)
    if fmt == 'etalog':
        parseetalog(extractsegment(g, fmt, sems), g['grf'], username)
        makerdfdict(g) # Строим словарь сущностей в графе. g['edict'] меняется
        if len(g['grf']) > 0: return True
    else:
        try:
           g['grf'].parse(data = extractsegment(g, fmt, sems), format = fmt)
           makerdfdict(g) # Строим словарь сущностей в графе. g['edict'] меняется
           if len(g['grf']) > 0: return True
        except rdflib.plugins.parsers.notation3.BadSyntax:
            g['grf'].remove( (None, None, None) ) # Чтобы не было частичного результата
            message('err', _("Нарушен формат RDF\n"))
    return False

def serializerdf(fmt, g): # Получить текст из хранилища rdflib
    if len(g['grf']) > 0:
        if fmt == 'etalog': return get_etalog_text(g['grf']).encode('utf-8') # Получаем str
        else: return g['grf'].serialize(format = fmt)
    return ''

def init_graphdata(): # Инициалиация структуры данных граф + метаданные
    return {'src': {}, 'nl': '', 'txt': '', 'grf': rdflib.Graph(), 'edict': {}, 'stat': {}}
    # src - записи о происхождении графа для информирования пользователя. Ключи: file app label tgt_file tgt_sent
    # nl - исходный текст на естественном языке, если есть.
    # txt - исходный текст графа, меняется только до parserdf
    # grf - хранилище троек rdflib
    # edict - словарь сущностей и связей в графе (см. makerdfdict), хранит URI и их части в виде СТРОК str
    # stat - статистические данные (см. makerdfdict)
    # * bmarks - сведения о начале и конце внутри txt сегментов базовой сем. стуктуры, если выделен комментарием, и промежуточной структуры, Присутствует только если выделены сегменты

def clear_graph(g, master): # Очистка графа + копирование префиксов из другого графа (если указан)
    g['grf'].remove( (None, None, None) )
    if master:
        for pfx, ns in master['grf'].namespaces(): g['grf'].bind(pfx, ns) # Инициализация пустого списка g и копирование в него префиксов
    g.update({'txt': ''})
    g.update({'nl': ''})
    g['src'].clear()
    g['edict'].clear()
    g['stat'].clear()
    if 'bmarks' in g: del g['bmarks']


####  Поиск цепочек


def get_neighbors(g, n, lst, bl): # Временный список узлов графа для поиска цепочек из хранилища RDF

    for s, p, o in g['grf'].triples((n, None, None)):
        if o and not p in bl and not o in bl: lst.append(o)
    for s, p, o in g['grf'].triples((None, None, n)):
        if s and not p in bl and not s in bl: lst.append(s)
    return lst

def find_path(graph, start, goal, bl): # Поиск кратчайшей цепочки между думя узлами в графе (здесь все узлы - объекты RDF)
    lst = []
    if start == goal:
        return [start]
    visited = {start}
    queue = deque([(start, [])])

    while queue:
        current, path = queue.popleft()
        visited.add(current)

        lst = get_neighbors(graph, current, lst, bl)
        for neighbor in lst:
            if neighbor == goal:
                return path + [current, neighbor]
            if neighbor in visited:
                continue
            queue.append((neighbor, path + [current]))
            visited.add(neighbor)
        lst.clear
    return None  # цепочка не найдена

#### Получение URI объектов для собственно фильтрации (fdict)
def get_rdf_objects(d, ref, lst, expand): # Получение списка URI сущностей из словаря графа, соответствующих запросу (одному запросу для фильтра)
    var = ''
    ns, pfx, e, i = parseURI(ref)
    if re.match('\?', i): # переменные на ? = любой номер
        var = i;
        i = ''
    try:
        for ind in d: # Все индивидуальные сущности/отношения (URI) в исходном графе. Пустые переменные == *
            if not e or e == d[ind]['e']:
                if (expand and not i) or i == d[ind]['i']: # флаг expand разрешает возвращать все индивиды при отсутствии номера индивида в ref
                    if not pfx or pfx == d[ind]['pf']:
                        lst.update({RDFobj(ind): None}) # Сбор списка из объектов RDF, который спользуется для построения fdict
    except KeyError: pass


def entity_filters_to_fdict(d, flt, fd): # Получение списка сущностей из набора условий в filters (=flt) и дополнение fdict
    for ref in flt:
        lst = {}; get_rdf_objects(d, ref, lst, True)
        deep_update(fd, {flt[ref]['action']: lst}) # здесь fd = fdict[cls/rel]


def path_to_triplets(path, fd, chn): # Преобразование цепочки URI в списки для отбора троек в fdict
    if len(path) < 2: return
    for n in path: deep_update(fd, {'+': {chn: {n: None}}})

def find_all_paths(graph, start, goal, fd, chn):
    bl=[]
    bl.append(RDFobj('http://www.w3.org/1999/02/22-rdf-syntax-ns#type')) # Исключение цепочек через общий класс Human_1_1 - Human - Human_1_2

    n = 0 # Счетчик числа находимых цепочек
    path = find_path(graph, start, goal, bl)
    if not path: # Возможно, задан узел класса, цепочку к/от которого нельзя построить без связей type
        bl.pop()
        path = find_path(graph, start, goal, bl)
        if not path: return
    path_to_triplets(path, fd, chn)
    path.pop(0); path.pop() # Отбросим начало и конец (start & goal)
    if not path: return
    for node in path: # Все промежуточные узлы по очереди отбрасываем
        bl.append(node)
        spath = find_path(graph, start, goal, bl) # Ищем доп. цепочки без отброшенных промежуточных узлов
        if not spath: bl.pop() # Не запрещать единственный путь
        else:
            n += 1
            path_to_triplets(spath, fd, chn)
            if n > 5: return # Не более 5 разных цепочек

def chain_filters_to_fdict(d, flt, fd): # Получение списков сущностей для отбора триплетов из набора условий в filters (=flt)
    try:
        for chn in flt:
            slst = {}; get_rdf_objects(d['ent'], flt[chn]['s'], slst, False)  # Чтобы не получать класс, а не все индивиды класса, при запросе без номера индивида
            elst = {}; get_rdf_objects(d['ent'], flt[chn]['e'], elst, False)  # вместо пустого номера здесь передается ' '

            pairs = {}
            for sn in slst:
                for en in elst:
                    if sn and en and not sn == en: pairs.update({sn: en}) # Пара URI индивидов (объектов RDF)  для поиска цепочек
            for sn in pairs: find_all_paths(srcg, sn, pairs[sn], fd, chn) # Для всех подходящих пар индивидов ищем соединяющие их цепочки узлов
    except KeyError: return


def triplet_filters_to_fdict(d, flt, fd): # Получение списков сущностей для отбора триплетов из набора условий в filters (=flt)
    try:
        for triplet in flt: # секция srcg['edict'] - строки, секция filters - строки, на выходе объекты RDF
            slst = {}; get_rdf_objects(d['ent'], flt[triplet]['s'], slst, True)
            rlst = {}; get_rdf_objects(d['rls'], flt[triplet]['r'], rlst, True)
            olst = {}; get_rdf_objects(d['ent'], flt[triplet]['o'], olst, True)
            deep_update(fd, {flt[triplet]['action']: {triplet: {'s': slst, 'r': rlst, 'o': olst}}})
    except KeyError: return

############ Подержка SPARQL
def get_newvar(techvarcnt, name, varlst): # Получить уникальное имя для новой переменной
    if not name in techvarcnt: techvarcnt.update({name: 0})
    while True:
        techvarcnt[name] += 1
        var = '?' + name + str(techvarcnt[name])
        if not var in varlst['vars']: break  # Предотвращаем случайное совпадение новой переменной с уже имеющейся
    return var


def get_prefix(d, ref, crefs): # Узнать префикс для класса (переменной)
    for pfx in d['pfx']:
        if pfx:
            lst = {}; get_rdf_objects(d['ent'], append_str(pfx, ref, ':'), lst, False)
            if len(lst) > 0: deep_update(crefs, { append_str(pfx, ref, ':'): None}) # Если есть в графе узлы с префиксом pfx


def get_sparql_tq(d, key, flt, triplet, role, varlst): # Получение шаблона(ов) для выбора одного узла для SPARQL запроса
    if re.match('\?', flt[triplet][role]): # получена простая переменная
        var = flt[triplet][role]
        deep_update(varlst, {'vars': {var: {'triplets': {triplet: {role: None}}}}})
        deep_update(varlst, {'triplets': {triplet: {role: var}}})
    else:
        ns, pfx, e, i = parseURI(flt[triplet][role])
        if e in d[key] and re.match('\?', i): # получен запрос на выбор индивидов класса  e (key - cls|rel)
            var = i
            if 'vars' in varlst and var in varlst['vars'] and 'type' in varlst['vars'][var] and not varlst['vars'][var]['type'] == e:
                message('err', _('Переменная %s не может относиться к двум разным классам %s и %s.') % (var, varlst['vars'][var]['type'], e) )
                return False
            else:
                deep_update(varlst, {'vars': {var: {'triplets': {triplet: {role: None}}}}})
                deep_update(varlst, {'vars': {var: {'type': e}}}) # Голое имя класса, которое еще требует префикса
                deep_update(varlst, {'triplets': {triplet: {role: var}}})
    return True


def get_typeconst(var, e, d):
    crefs = {}
    get_prefix(d, e, crefs) # Возьмем все найденные префиксы сущностей из классов с именем e
    if len(crefs) == 1: return var + ' rdf:type ' + ''.join(crefs.keys()) + '. ' # Отсев случаев разночтения классов
    elif len(crefs) == 0: message('err', _('Не удалось сформировать корректный запрос SPARQL.\nДля переменной %s (%s) не удается найти URI.\n') % (var, e)); return ''
    else: message('err', _('Не удалось сформировать корректный запрос SPARQL.\nПеременной %s может быть приписано более одного URI.\n%s может означать: ') % (var, e) +' / '.join(crefs.keys()) ); return ''


def get_triparts(d, flt, lst, triplet, pos, key, varlst, techvarcnt, newtypeconsts): # получение списка возможных значений одного члена триплета-шаблона в запросе SPARQL
    if pos in varlst['triplets'][triplet]: lst.update({varlst['triplets'][triplet][pos]: None}) # формируемая часть шаблона содержит переменную
    elif flt[triplet][pos] == '': lst.update({get_newvar(techvarcnt, 'star', varlst): None})# в запросе было '*', т.е. надо ввести новый пустой узел
    else:
        ns, pfx, e, i = parseURI(flt[triplet][pos])
        if i == '' or re.match('^\d[\d\_]*$', i) or key == 'rls': get_rdf_objects(d[key], flt[triplet][pos], lst, False) # Узлы классов или индивидов без номера (и type), индивиды с номером или отношения. Если подбирается URI отношения, то не будет суффикса индивида
        else: # Узлы обозначающие индивидов с переменной вместо номера
            techvar = get_newvar(techvarcnt, 'var', varlst)
            lst.update({techvar: None})
            const = get_typeconst(techvar, e, d)
            if const: newtypeconsts.update({const: None})


def get_triconst(d, flt, varlst, triplet, techvarcnt): # Получаем шаблон триплета с переменной из filters и разворачиваем в список подходящих для SPARQL шаблонов
    qs = ''
    newtypeconsts = {} # Для ограничений типов вновь вводимых технических переменных (Limb -> ?var1 + ?var1 rdf:type common:Limb)
    slst = {}; get_triparts(d, flt, slst, triplet, 's', 'ent', varlst, techvarcnt, newtypeconsts)
    rlst = {}; get_triparts(d, flt, rlst, triplet, 'r', 'rls', varlst, techvarcnt, newtypeconsts)
    olst = {}; get_triparts(d, flt, olst, triplet, 'o', 'ent', varlst, techvarcnt, newtypeconsts)

    for s in slst:
        s = str(s)
        for r in rlst:
            r = str(r)
            for o in olst:
                o = str(o)
                if not re.match("[\?_]", s) and re.search("#", s): s = '<' + s + '>'
                if not re.match("[\?_]", r) and re.search("#", r): r = '<' + r + '>'
                if not re.match("[\?_]", o) and re.search("#", o): o = '<' + o + '>'
                qs += ' '.join([s,r,o]) + '. '

    for t in newtypeconsts: qs += t # Добавим ограничения типа для вновь созданных технических пременных из запроса
    if not qs: message('err', _('Не удалось преобразовать шаблон триплета\n%s\nSPARQL запрос нельзя сформировать.') % triplet); return ''

    return qs

def make_sparql_query_select_from_tripletfilters(d, flt, varlst): # Порождение SPARQL-запроса SELECT из шаблонов поиска триплетов в filters['tri'] с переменными
    techvarcnt = {} # Хранит номера для уникальности добавляемых переменных
    for triplet in flt: # секция srcg['edict'] - строки, секция filters - строки
        if not (get_sparql_tq(d, 'cls', flt, triplet, 's', varlst) 
            and get_sparql_tq(d, 'rls', flt, triplet, 'r', varlst) 
            and get_sparql_tq(d, 'cls', flt, triplet, 'o', varlst)): return '', False

    if len(varlst) < 1: return '', True # Переменных не обнаружено

    # Сборка запроса Sparql (varlst - список переменных и ссылок на шаблоны троек в flt-filters)
    query = 'select '

    qpos = 0
    for var in varlst['vars']: # Список переменных
        query = append_str(query, var, ' ')
        deep_update(varlst, {'qpos': {qpos: var}}); qpos += 1 # Запомним позицию каждой переменной
    query += ' where {'

    for var in varlst['vars']: # Список ограничений на тип (класс) переменной (Если есть)
        if 'type' in varlst['vars'][var]:
            const = get_typeconst(var, varlst['vars'][var]['type'], d)
            if const: query += const
            else: return '', False

    for triplet in varlst['triplets']: # Список шаблонов триплетов
        const = get_triconst(d, flt, varlst, triplet, techvarcnt)
        if const: query += const
        else: return '', False

    query += '}'
    return query, True


def apply_sparql_query_select(g, query, varlst, qres): # Применение sparql-запроса и построение списка вариант решения - переменная - значение
    print (_("SPARQL запрос:"))
    print (query)
    rows = g['grf'].query(query)
    if len(rows) < 1: message('inf', _('Граф не содержит фрагмента соответствующего заданным критериям поиска триплетов.\nПолучен пустой ответ на SPARQL запрос.'))
    rowcnt = 0
    for row in rows:
        rowcnt += 1
        print (_('Ответ %s:') % rowcnt)
        for qpos in varlst['qpos']: # Сопоставление переменных и найденных значений
            var = varlst['qpos'][qpos]
            val = str(row[qpos])
            deep_update(qres, {rowcnt: {var: val}})
            print ("	", var, val)


def replace_tripletflt_with_vars(flt, qres, varlst): # Заменить шаблоны с переменными на новые построенные по результатам запроса
    for solution in qres: # Перебор вариантов решения
        for triplet in varlst['triplets']: # Все триплеты с переменными
            newtriplet = append_str(triplet, str(solution), '_') # ID нового шаблона
            for var in qres[solution]: # Перебор переменных
                val = qres[solution][var]
                p = re.match("(.*?)#(.*)", val)
                if p: val = p.group(2)  # Отрезаем namespace

                if triplet in varlst['vars'][var]['triplets']:
                    for role in varlst['vars'][var]['triplets'][triplet]:
                        deep_update(flt, {newtriplet: {role: val}}) # Если в триплете есть переменная var, то вставляем часть ее содержащую

            for key in ('s', 'r', 'o', 'action'):
                if not key in flt[newtriplet]: deep_update(flt, {newtriplet: {key: flt[triplet][key]}}) # Копируем остальные части (без переменных - как было)

    for triplet in varlst['triplets']: flt.pop(triplet) # Стираем замененные шаблоны


def triplet_filters_resolve_sparql(g, d, flt, fd): # Отбор триплетов с переменными, формирование SPARQL запроса SELECT для их разрешения (совместного) и замена переменных на найденные узлы
    varlst = {} # Списки триплетов с переменными, переменных и их типов. Формируется и используется при генерации запроса query
    qres = {} # Результаты запроса
    query, res = make_sparql_query_select_from_tripletfilters(d, flt, varlst)
    if res:
        if query: # Если запрос построен
            apply_sparql_query_select(g, query, varlst, qres)
            replace_tripletflt_with_vars(flt, qres, varlst)
        return True # Пустая строка sparql query может означать, что не было переменных в задании на поиск триплетов
    else: return False # Не удалось построить запрос

#### Проверка соответствия триплетов фильтрам
def test_match_whitelist (fdict, key, ind): # Проверка одного индивида по белому списку
    if key in fdict:
        if '+' in fdict[key]: # Есть белый список
            if ind in fdict[key]['+']: return True
            else: return False
    return True # Если нет fdict[rel][+] - нет ограничения


def test_match_blacklist (fdict, key, ind): # Проверка одного индивида по черному списку
    if key in fdict:
        if '-' in fdict[key]: # Есть черный список
            if ind in fdict[key]['-']: return False
            else: return True
    return True # Если нет fdict[rel][-] - нет ограничения

def test_match_chain_triplets (fdict, key, s, p, o): # Проверка триплета
    if key in fdict:
        if '+' in fdict[key]: # Есть белый список
            for chn in fdict[key]['+']:
                if s in fdict[key]['+'][chn] and o in fdict[key]['+'][chn]: return True # Любые связи между узлами входящими в цепочку chn
            return False
    return True

def test_match_triplet_whitelist (fdict, key, s, p, o): # Проверка триплета
    if key in fdict:
        if '+' in fdict[key]: # Есть белый список
            for triplet in fdict[key]['+']:
                if s in fdict[key]['+'][triplet]['s'] and p in fdict[key]['+'][triplet]['r'] and o in fdict[key]['+'][triplet]['o']: return True
            return False
    return True

def test_match_triplet_blacklist (fdict, key, s, p, o): # Проверка триплета
    if key in fdict:
        if '-' in fdict[key]: # Есть черный список
            for triplet in fdict[key]['-']:
                if s in fdict[key]['-'][triplet]['s'] and p in fdict[key]['-'][triplet]['r'] and o in fdict[key]['-'][triplet]['o']: return False
    return True

####
def apply_filter(g, og, filters, fdict): # (исх.граф, целевой граф, словарь, строки фильтров) Создание отфильтрованного графа (с результатами поиска)
    fdict.clear()
    fdict.update({'cls': {}, 'rel': {}, 'chn': {}, 'tri': {}})
    if 'cls' in filters: entity_filters_to_fdict(g['edict']['ent'], filters['cls'], fdict['cls']) # Получение списка URI для отбора триплетов из набора условий в filters (=f)
    if 'rel' in filters: entity_filters_to_fdict(g['edict']['rls'], filters['rel'], fdict['rel'])
    if 'chn' in filters: chain_filters_to_fdict(g['edict'], filters['chn'], fdict['chn'])
    if 'tri' in filters:
        if triplet_filters_resolve_sparql(g, g['edict'], filters['tri'], fdict['tri']): # Преобразование переменных в шаблоны
            triplet_filters_to_fdict(g['edict'], filters['tri'], fdict['tri'])
        else: message('inf', _('Поиск отменён из-за ошибки.\nИсправьте, пожалуйста, шаблоны поиска триплетов.')); return

    clear_graph(og, g) # Инициализация пустого списка outg и копирование в него префиксов srcg

    for s,p,o in g['grf']:  # Перебор всех триплетов
        passed=False; blacklisted=False

        if 'chn' in filters and '+' in fdict['chn']: # Фильтр цепочек
            if test_match_chain_triplets (fdict, 'chn', s, p, o): # В цепочках найденные URI - строки, а из графа получаем объекты
                            passed=True

        if not passed:  # Фильтры сущностей и отношений
            clspassed = False; relpassed = False

            if 'cls' in filters:
                if test_match_blacklist (fdict, 'cls', s) and test_match_blacklist (fdict, 'cls', o): # достаточно False от любого из черных списков
                    if test_match_whitelist (fdict, 'cls', s) or test_match_whitelist (fdict, 'cls', o):
                            clspassed=True
                else: blacklisted=True
            else: clspassed = True

            if 'rel' in filters:
                if test_match_blacklist (fdict, 'rel', p): # достаточно False от любого из черных списков
                    if test_match_whitelist (fdict, 'rel', p):
                            relpassed=True
                else: blacklisted=True
            else: relpassed = True

            if ('cls' in filters or 'rel' in filters) and clspassed and relpassed: passed = True # Общий результат фильтра сущностей

        if 'tri' in filters: # and not passed:  # Фильтр триплетов
            if test_match_triplet_blacklist (fdict, 'tri', s, p, o):
                if test_match_triplet_whitelist (fdict, 'tri', s, p, o):
                            passed=True
                            blacklisted=False # Разрешаем фильтру триплетов находить то, что попало в черный список фильтра сущностей
            else: blacklisted=True

        if not filters: passed = True # Без фильтрации, если нет критериев отбора

        if passed and not blacklisted:
            og['grf'].add( (s, p, o) )

    makerdfdict(og)

#### Поддержка чтения формата Эталог

def appendkey(dic, key, s):
    if key not in dic:
        dic[key] = s
    else:
        dic[key] += s

def convertvar(s): # Конверсия class_1_1 (или ?class_1_1) в Class_1_1   # Переменные без цифры в конце не распознаются
#fix
    if re.match('[0-9]$', s):
        if s and s[0] == '?': s = re.sub(r'^\?', '', s)
        return s.title()
    else: return s

def converttypedec(triple): # Конверсия декларации индивида вида "Class class_1_1" (или "Class ?class_1_1") в "Class_1_1 rf:type Class"
#fix
    if len(triple) > 1 and re.match('^[A-Z]+', triple[0]):
        if re.match('^[a-z].*[0-9]$', triple[1]) or re.match('^\?.*[0-9]$', triple[1]):
            return [convertvar(triple[1]), 'type', triple[0]]
    return triple

def cleantxt(txt): # удаление комментариев и пустых строк
    ctxt = ''
    for line in txt.splitlines():
        line = re.sub(r' *//.*', '', line)
        line = line.rstrip()
        if (line): # Очищенные от пустых строк и комменариев строки
            if ctxt:
                ctxt += "\n"
            ctxt += line
    return ctxt

def parsesublocks(txt, etastruct, cref, ref): # Выделение блоков сокращенных троек
    quot = False
    if txt:
     for c in txt: # по символам
        if c == '"': # Игнорируем разделители внутри кавычек
            if not quot: quot = True
            else: quot = False

        if (c == '.' or c == ',') and not quot:
            while ref in etastruct['blk']: ref += 1 # след. незанятый номер
            cref = ref # Переход к новому блоку
        else:
            appendkey(etastruct['blk'], cref, c)  # Сбор текста скобочных и внешних блоков
    else:
        appendkey(etastruct['blk'], cref, '') # Пустой блок. Этого не должно быть
        print (_("Пустой блок %s" % cref))

def parseblocks(txt, etastruct): # Выделение скобочных блоков
    ref = 0 # Счетчик найденных блоков (только растет)
    cref = ref # Номер текущего собираемого блока
    refpath = [] # Учет вложенности скобок
    refpath.append(cref)
    quot = 0
    brblocks = {}

    if txt:
     for c in txt: # по символам
        if c == '"': # Игнорируем разделители внутри кавычек
            if quot: quot = 1
            else: quot = 0

        if c == '(' and not quot:
            refpath.append(cref) # Запомним номер родителького блока
            ref += 1   # Начат новый блок
            reflabel = "^^ref:" + str(ref) # текстовая ссылка на другой блок
            appendkey(brblocks, cref, reflabel)
            cref = ref  # Переходим к новому блоку
        elif c == ')' and not quot:
            cref = refpath.pop() # Закончен блок - возврат к предыдущему

        else:
            appendkey(brblocks, cref, c)  # Сбор текста скобочных блоков

     ref += 1
     for cref in brblocks: # Перебираем скобочные блоки и делим их на пакеты троек
        parsesublocks(brblocks[cref], etastruct, cref, ref)

def parseline(line): # Разбор одной строки на элементы
    quot = 0
    elem = 0
    buf = ''
    triple = []

    for c in line:
        if c == '"': # Игнорируем пробелы внутри кавычек
            if quot: quot = 1
            else: quot = 0
        if c == ' ' and not quot: # пробелы - разделитель
            if elem and len(buf) > 0:
                triple.append(buf) # Запомним член тройки и начнем сборку нового
                buf = ''
            elem = 0
        else: elem = 1
        if elem and not c == '\t': buf += c
    if len(buf): triple.append(buf)  # Последний элемент
    triple = converttypedec(triple) # Только декларации переменных в первлй строке. ?переменные конвертируются потом
    return triple

def parsetriplets(txt, blkref, etastruct): # Разбор одной строки на члены троек
    elems = []

    for line in txt.splitlines(): # Собираем все элементы троек в одну цепочку
        elems += parseline(line)         # Строки состоящие из одних пробелов дают пустые списки элементов и игнорируются

    if len(elems) > 0:
        head = convertvar(elems[0]) # Первый элемент, который является субъектом всех троек блока
        triplet = []
        global c
        c = 1 # отсчет позиций в тройках 0-1-2 1-2 1-2
        for e in elems:
            if c == 2:
                triplet.append(head)
                triplet.append(convertvar(e))
            if c == 3:
                triplet.append(convertvar(e))
                if not blkref in etastruct['triples']:
                    etastruct['triples'][blkref] = []
                etastruct['triples'][blkref].append([triplet[0], triplet[1], triplet[2]]) # Помещаем собранный триплет в общую структуру
                triplet.clear()
                c = 1
            c += 1

def fillnamespaces(g, etastruct, username): # Подготовка пространства имен для создаваемого графа RDF
    #g.bind('xsd', 'http://www.w3.org/2001/XMLSchema#')
    #g.bind('rdfs', 'http://www.w3.org/2000/01/rdf-schema#')
    g.bind('rdf', 'http://www.w3.org/1999/02/22-rdf-syntax-ns#')
    g.bind('common', 'http://proling.iitp.ru/ontologies/ontoetap.owl#')
    g.bind('owl', 'http://www.w3.org/2002/07/owl#')
    g.bind('md', 'http://proling.iitp.ru/ontologies/metadata.owl#')
    g.bind('football', 'http://proling.iitp.ru/ontologies/football.owl#')  # Лучше удалить
    g.bind(username, 'http://proling.iitp.ru/factbases/%s#' % username)
    for pfx, ns in g.namespaces(): # Словарик замены префиксов (etastruct['ns'])
        etastruct['ns'][str(pfx)] = str(ns)

def addprefix(e, etastruct, username): # Приписываем префиксы
    if len(e) > 0:
        if e == "Thing": return etastruct['ns']['owl'] + e
        elif e == "sameAs": return etastruct['ns']['owl'] + e
        elif e == "type": return etastruct['ns']['rdf'] + e
        elif re.search('\^\^xsd\:', e): return e # "xxx"^^xsd:string integer ... просто передаем далее в RDFObj
        elif e[0] == '"' and e[len(e)-1] == '"': # Просто закавыченная строка - угадываем тип сами
            m = unquot(e)
            if re.match('^[0-9]+$', m): return e + '^^xsd:integer'
            elif re.match('^[0-9\.\,]+$', m): return e + '^^xsd:float'
            else: return e + '^^xsd:string'
        elif re.search('\:', e): # Если получен ns префикс
            try:
                m = re.match('^(.*)\:(.*)')
                if m.group(1) in etastruct['ns']: return etastruct['ns'][m.group(1)] + m.group(2)
            except:
                return etastruct['ns']['common'] + re.sub('.*\:', '', e) 
        elif re.match('.*_[0-9]$', e): return etastruct['ns'][username] + e # индивиды в Этап
        else: return etastruct['ns']['common'] + e
    else: message('err', _("Ошибка разбора формата Эталог. Член триплета - пустая строка."))
    return e

def blockunref(e, etastruct): # Заменяем ссылку на блок первым концептом в блоке
    if re.match('\^\^ref\:', e): # Найдена ссылка на блок
        ref = int(re.sub('\^\^ref\:', '', e))
        if ref in etastruct['triples']:
            t = etastruct['triples'][ref][0]
            e = t[0]
        else: message('err', _("Ошибка разбора формата Эталог. Неверная ссылка на скобочный блок."))
    return e


def parseetalog(txt, g, username): # Анализ текста на Эталог

    etastruct = {'ref': 0, 'blk': {}, 'triples': {}, 'ns': {}} # Хранилище блоков, на которые делится текст # ref - счетчик нумерации блоков 
    fillnamespaces(g, etastruct, username)
    txt = cleantxt(txt) # текст очищен и снова собран в строку
    parseblocks(txt, etastruct)

    for blkref in etastruct['blk']: # Перебираем отдельные блоки и восстанавливаем из них триплеты. Наполняется etastruct['triples']
        #print ('BLK:', blkref, '\n', etastruct['blk'][blkref])
        parsetriplets(etastruct['blk'][blkref], blkref, etastruct)

    for blkref in etastruct['triples']: # Перебираем триплеты
        for tr in etastruct['triples'][blkref]:
            triplet = []
            for e in tr: # члены триплетов
                e = blockunref(e, etastruct) # восстанавливаем ссылки на блоки ^^ref:N -> Class
                e = addprefix(e, etastruct, username) # Добавляем префиксы и типы данных, где не было
                triplet.append(RDFobj(e))
            g.add( (triplet[0], triplet[1], triplet[2]) )

#### Поддержка формата Эталог

def extract_name(uri):
    name = uri[uri.find('#') + 1:]
    if isinstance(uri, rdflib.URIRef):
#        if name.count('_') == 2: return '?' + name[0].lower() + name[1:]
        if name.count('_') == 2: return name[0].lower() + name[1:]
        else: return name
    else: return '"' + uri + '"'


def get_sort_name(term):
    name = extract_name(term)
    parts = name.split('_')
    if len(parts) > 2 and int(parts[1]) == 0: sort_name = 'Z' + parts[0]
    else: sort_name = 'A' + parts[0]
    return sort_name + (str(int(parts[1])*10000 + int(parts[2])) if len(parts) > 2 else '')

def get_etalog_text(graph):
    expression = ''
    top_nodes, parents = calculate_heads(graph)
    top_nodes = sorted(top_nodes, key=get_sort_name)
    children = collections.defaultdict(dict)
    for child in parents:
        parent, property = parents[child]
        children[parent][child] = property
    for top_node in top_nodes:
        term_expression = get_term_expression(graph, top_node, children, 0, True, False)
        if len(expression) == 0: expression = term_expression
        else: expression += ",\n\n" + term_expression
    if len(expression) > 0: expression += ".\n"
    return expression + "\n"

def calculate_heads(graph):
    subjects = {subject for subject, property, value in graph}
    values = {value for subject, property, value in graph}
    top_nodes = subjects - values
    heads = {head: (head, 0) for head in top_nodes}
    parents = {}
    for head in top_nodes: calculate_sub_heads(graph, head, heads, parents)
    additional_top_nodes = (subjects | values) - heads.keys()
    while additional_top_nodes:
        random_head = sorted(additional_top_nodes, key=get_sort_name)[0]
        top_nodes.add(random_head)
        heads.update({random_head: (random_head, 0)})
        calculate_sub_heads(graph, random_head, heads, parents)
        additional_top_nodes = (subjects | values) - heads.keys()
    return top_nodes, parents

def calculate_sub_heads(graph, term, heads, parents):
    delta_cost = 2 + extract_name(term).count('_0_')
    for _, property, child in graph.triples((term, None, None)):
        if child not in heads or heads[child][1] > heads[term][1] + delta_cost:
            heads[child] = (heads[term][0], heads[term][1] + delta_cost)
            parents[child] = (term, property)
            calculate_sub_heads(graph, child, heads, parents)

def get_term_expression(graph, term, children, level, is_cascade, is_embedded):
    text = extract_name(term)
    has_class = False
    properties = []
    triples = list(graph.triples((term, None, None)))
    if not triples: return text
    is_local_cascade = is_cascade
    if len(triples) == 1: is_local_cascade = False
    for _, property_term, child in triples:
        if property_term == rdflib.term.URIRef('http://www.w3.org/1999/02/22-rdf-syntax-ns#type'):
            property_term = rdflib.term.URIRef('http://proling.iitp.ru/ontologies/ontoetap.owl#isA')
            if not has_class:
                text = extract_name(child) + ' ' + text
                has_class = True
        else:
            property = ''
            if child in children[term] and children[term][child] == property_term: term_string = get_term_expression(graph, child, children, level + 1, is_cascade, True)
            else: term_string = extract_name(child)
            if is_local_cascade or is_cascade and term_string.count(' ') > 1: property += "\n" + ' ' *(level * 4 + 2)
            else: property += " "
            property += extract_name(property_term)
            if '\n' in term_string: property += "\n" + ' ' * (level * 4 + 3) + term_string
            else: property += " " + term_string
            properties.append(property)

    properties.sort()
    text += ''.join(properties)
    return ('(' if is_embedded else '') + text + (('\n' if is_embedded else '') + ' ' * (level * 4 - 1) if is_cascade and '\n' in text else '') + (')' if is_embedded else '')

#### Чтение и запись файлов

def read_rdffile(inputfile): # Чтение RDF из файла
    txt = []
    try:
        f1 = open(inputfile, mode='r', encoding='utf-8-sig')
        f1.read(4)
        f1.seek(0)
    except UnicodeDecodeError:
        f1.close()
        f1 = open(inputfile, mode='r', encoding='utf-16')
    except FileNotFoundError:
        message('inf', _("Файл не найден\n%s") % inputfile)
        return ''

    for line in f1.readlines():
        line = line.rstrip('\r\n') # Удаление переносов строки (чтобы не мешал стандарт windows)
        txt.append(line)
    f1.close()
    return "\n".join(txt)

def save_rdffile(outputfile, txt): # Сохранение RDF
    f2 = open(outputfile, mode='w', newline=os.linesep)
    print(txt.decode('utf-8-sig'), file=f2)
    f2.flush()
    f2.close()

###################################### Интерфейс

class FilterGUI(object):

    def __init__ (self, fltwin, srcg, outg, filterq, settings, rtparam, context): # Граф. интерфейс
        self.fltwin = fltwin
        self.draw_gui(self.fltwin, srcg, outg, settings, rtparam)

        if not filterq is None: # Если передан критерий поиска - запустить поиск. filterq содержит готовые строки запросов, которые вставляются в UI rdfilter
            if 'cls' in filterq:
                self.sentflstentry.delete(0, tk.END) # Обновляем строку в поле запроса на поиск сущности
                self.sentflstentry.insert(0, filterq['cls'])
            if 'rls' in filterq:
                self.srelflstentry.delete(0, tk.END) # Обновляем строку в поле запроса на поиск отношений
                self.srelflstentry.insert(0, filterq['rls'])
            if 'chn' in filterq:
                self.schentry.delete(0, tk.END) # Обновляем строку в поле запроса на поиск цепочек
                self.schentry.insert(0, filterq['chn'])
            self.filter_and_box_update(self.fltwin, self.resbox, self.sentflstentry, self.srelflstentry, self.schentry, self.strilstentry, self.ressrclabel, self.resstatlabel, srcg, outg, settings['format'])

    def draw_gui(self, fltwin, srcg, outg, settings, rtparam): # Граф. интерфейс

        OPEN = """
#define open_width 48
#define open_height 48
static unsigned char open_bits[] = {
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0xc0, 0xff, 0xff, 0xff, 0x7f, 0x00, 0xe0, 0xff, 0xff, 0xff, 0x7f, 0x00,
   0xe0, 0xff, 0xff, 0xff, 0x7f, 0x00, 0xe0, 0x00, 0x00, 0x00, 0x70, 0x00,
   0xe0, 0x00, 0x00, 0x00, 0x70, 0x00, 0xe0, 0x00, 0x00, 0x00, 0x70, 0x00,
   0xe0, 0x00, 0x00, 0x00, 0x70, 0x00, 0xe0, 0xc0, 0xff, 0xff, 0xff, 0x3f,
   0xe0, 0xc0, 0xff, 0xff, 0xff, 0x3f, 0xe0, 0xc0, 0xff, 0xff, 0xff, 0x1f,
   0xe0, 0xe0, 0x00, 0x00, 0x00, 0x1c, 0xe0, 0xe0, 0x00, 0x00, 0x00, 0x1c,
   0xe0, 0xe0, 0x00, 0x00, 0x00, 0x0e, 0xe0, 0x70, 0x00, 0x00, 0x00, 0x0e,
   0xe0, 0x70, 0x00, 0x00, 0x00, 0x0e, 0xe0, 0x70, 0x00, 0x00, 0x00, 0x07,
   0xe0, 0x38, 0x00, 0x00, 0x00, 0x07, 0xe0, 0x38, 0x00, 0x00, 0x00, 0x07,
   0xe0, 0x38, 0x00, 0x00, 0x80, 0x03, 0xe0, 0x1c, 0x00, 0x00, 0x80, 0x03,
   0xe0, 0x1c, 0x00, 0x00, 0x80, 0x03, 0xe0, 0x1c, 0x00, 0x00, 0xc0, 0x01,
   0xe0, 0x0e, 0x00, 0x00, 0xc0, 0x01, 0xe0, 0x0e, 0x00, 0x00, 0xc0, 0x01,
   0xe0, 0x0e, 0x00, 0x00, 0xe0, 0x00, 0xe0, 0x07, 0x00, 0x00, 0xe0, 0x00,
   0xe0, 0xff, 0xff, 0xff, 0xff, 0x00, 0xe0, 0xff, 0xff, 0xff, 0x7f, 0x00,
   0xc0, 0xff, 0xff, 0xff, 0x7f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
"""

        CMP = """
#define compare2_width 48
#define compare2_height 48
static unsigned char compare2_bits[] = {
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x80, 0x03, 0x00, 0x00, 0x00, 0x00, 0x80, 0x03, 0x00, 0x00,
   0x00, 0x00, 0x80, 0x03, 0x00, 0x00, 0xe0, 0xff, 0x1f, 0xf0, 0xff, 0x07,
   0xe0, 0xff, 0x1f, 0xf0, 0xff, 0x07, 0xe0, 0xff, 0x9f, 0xf3, 0xff, 0x07,
   0xe0, 0x00, 0x80, 0x03, 0x00, 0x07, 0xe0, 0x00, 0x80, 0x03, 0x00, 0x07,
   0xe0, 0x00, 0x00, 0x00, 0x00, 0x07, 0xe0, 0x00, 0x00, 0x00, 0x00, 0x07,
   0xe0, 0x00, 0x80, 0x03, 0x00, 0x07, 0xe0, 0xe0, 0x9f, 0x03, 0x00, 0x07,
   0xe0, 0xe0, 0x9f, 0xf3, 0x0f, 0x07, 0xe0, 0x00, 0x00, 0xf0, 0x0f, 0x07,
   0xe0, 0x00, 0x00, 0x00, 0x00, 0x07, 0xe0, 0x00, 0x80, 0x03, 0x00, 0x07,
   0xe0, 0x00, 0x80, 0x03, 0x00, 0x07, 0xe0, 0xe0, 0x9f, 0x03, 0x00, 0x07,
   0xe0, 0xe0, 0x1f, 0xf0, 0x0f, 0x07, 0xe0, 0x00, 0x00, 0xf0, 0x0f, 0x07,
   0xe0, 0x00, 0x80, 0x03, 0x00, 0x07, 0xe0, 0x00, 0x80, 0x03, 0x00, 0x07,
   0xe0, 0x00, 0x80, 0x03, 0x00, 0x07, 0xe0, 0xe0, 0x1f, 0x00, 0x00, 0x07,
   0xe0, 0xe0, 0x1f, 0xf0, 0x0f, 0x07, 0xe0, 0x00, 0x80, 0xf3, 0x0f, 0x07,
   0xe0, 0x00, 0x80, 0x03, 0x00, 0x07, 0xe0, 0x00, 0x80, 0x03, 0x00, 0x07,
   0xe0, 0x00, 0x00, 0x00, 0x00, 0x07, 0xe0, 0x00, 0x00, 0x00, 0x00, 0x07,
   0xe0, 0x00, 0x80, 0xf3, 0x0f, 0x07, 0xe0, 0x00, 0x80, 0xf3, 0x0f, 0x07,
   0xe0, 0x00, 0x80, 0x03, 0x00, 0x07, 0xe0, 0x00, 0x00, 0x00, 0x00, 0x07,
   0xe0, 0x00, 0x00, 0x00, 0x00, 0x07, 0xe0, 0x00, 0x80, 0x03, 0x00, 0x07,
   0xe0, 0x00, 0x80, 0x03, 0x00, 0x07, 0xe0, 0x00, 0x80, 0x03, 0x00, 0x07,
   0xe0, 0xff, 0x1f, 0xf0, 0xff, 0x07, 0xe0, 0xff, 0x1f, 0xf0, 0xff, 0x07,
   0xe0, 0xff, 0x9f, 0xf3, 0xff, 0x07, 0x00, 0x00, 0x80, 0x03, 0x00, 0x00,
   0x00, 0x00, 0x80, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
"""

        EYE = """
#define eye_width 48
#define eye_height 48
static unsigned char eye_bits[] = {
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xf8, 0x1f, 0x00, 0x00,
   0x00, 0x00, 0xff, 0xff, 0x00, 0x00, 0x00, 0xe0, 0xff, 0xff, 0x07, 0x00,
   0x00, 0xf8, 0xff, 0xff, 0x1f, 0x00, 0x00, 0xfe, 0x1f, 0xf8, 0x7f, 0x00,
   0x00, 0x7f, 0x06, 0xe0, 0xfe, 0x00, 0x80, 0x1f, 0x00, 0xc0, 0xf9, 0x01,
   0xe0, 0x07, 0xe0, 0x87, 0xe3, 0x07, 0xf0, 0x03, 0xf0, 0x0f, 0xc3, 0x0f,
   0xf8, 0x00, 0xe0, 0x1f, 0x07, 0x1f, 0x7c, 0x20, 0xe0, 0x3f, 0x06, 0x3e,
   0x3e, 0x60, 0xcc, 0x3f, 0x06, 0x7c, 0x1f, 0x60, 0xfc, 0x3f, 0x06, 0xf8,
   0x1f, 0x60, 0xfc, 0x3f, 0x06, 0xf8, 0x3e, 0x60, 0xfc, 0x3f, 0x06, 0x7c,
   0x7c, 0x60, 0xfc, 0x3f, 0x06, 0x3e, 0xf8, 0xe0, 0xf8, 0x1f, 0x07, 0x1f,
   0xf0, 0xc3, 0xf0, 0x0f, 0xc3, 0x0f, 0xe0, 0xc7, 0xe1, 0x87, 0xe3, 0x07,
   0x80, 0x9f, 0x03, 0xc0, 0xf9, 0x01, 0x00, 0x7f, 0x07, 0xe0, 0xfe, 0x00,
   0x00, 0xfe, 0x1f, 0xf8, 0x7f, 0x00, 0x00, 0xf8, 0xff, 0xff, 0x1f, 0x00,
   0x00, 0xe0, 0xff, 0xff, 0x07, 0x00, 0x00, 0x00, 0xff, 0xff, 0x00, 0x00,
   0x00, 0x00, 0xf8, 0x1f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
"""
        SEARCH = """
#define search_width 48
#define search_height 48
static unsigned char search_bits[] = {
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x7e, 0x00, 0x00, 0x00, 0x00, 0xc0, 0xff, 0x07, 0x00, 0x00,
   0x00, 0xf0, 0xff, 0x1f, 0x00, 0x00, 0x00, 0xfc, 0xff, 0x7f, 0x00, 0x00,
   0x00, 0xfe, 0xff, 0xff, 0x00, 0x00, 0x00, 0xff, 0x00, 0xfe, 0x01, 0x00,
   0x80, 0x1f, 0x00, 0xf8, 0x03, 0x00, 0xc0, 0x0f, 0x00, 0xe0, 0x07, 0x00,
   0xe0, 0x07, 0x00, 0xc0, 0x07, 0x00, 0xe0, 0x03, 0x00, 0x80, 0x0f, 0x00,
   0xf0, 0x01, 0x00, 0x00, 0x1f, 0x00, 0xf0, 0x01, 0x00, 0x00, 0x1f, 0x00,
   0xf8, 0x00, 0x00, 0x00, 0x1e, 0x00, 0xf8, 0x00, 0x00, 0x00, 0x3e, 0x00,
   0x78, 0x00, 0x00, 0x00, 0x3e, 0x00, 0x7c, 0x00, 0x00, 0x00, 0x3c, 0x00,
   0x7c, 0x00, 0x00, 0x00, 0x3c, 0x00, 0x7c, 0x00, 0x00, 0x00, 0x3c, 0x00,
   0x7c, 0x00, 0x00, 0x00, 0x3c, 0x00, 0x7c, 0x00, 0x00, 0x00, 0x3c, 0x00,
   0x7c, 0x00, 0x00, 0x00, 0x3c, 0x00, 0x78, 0x00, 0x00, 0x00, 0x3c, 0x00,
   0xf8, 0x00, 0x00, 0x00, 0x3e, 0x00, 0xf8, 0x00, 0x00, 0x00, 0x3e, 0x00,
   0xf8, 0x00, 0x00, 0x00, 0x1f, 0x00, 0xf0, 0x01, 0x00, 0x00, 0x1f, 0x00,
   0xf0, 0x01, 0x00, 0x80, 0x0f, 0x00, 0xe0, 0x03, 0x00, 0xc0, 0x0f, 0x00,
   0xe0, 0x07, 0x00, 0xe0, 0x1f, 0x00, 0xc0, 0x0f, 0x00, 0xf0, 0x3f, 0x00,
   0x80, 0x3f, 0x00, 0xfc, 0x7f, 0x00, 0x00, 0xff, 0x81, 0xff, 0xff, 0x00,
   0x00, 0xfe, 0xff, 0xff, 0xff, 0x01, 0x00, 0xf8, 0xff, 0xff, 0xff, 0x03,
   0x00, 0xe0, 0xff, 0xcf, 0xff, 0x07, 0x00, 0x80, 0xff, 0x81, 0xff, 0x0f,
   0x00, 0x00, 0x00, 0x00, 0xff, 0x1f, 0x00, 0x00, 0x00, 0x00, 0xfe, 0x3f,
   0x00, 0x00, 0x00, 0x00, 0xfc, 0x3f, 0x00, 0x00, 0x00, 0x00, 0xf8, 0x7f,
   0x00, 0x00, 0x00, 0x00, 0xf0, 0x7f, 0x00, 0x00, 0x00, 0x00, 0xe0, 0x7f,
   0x00, 0x00, 0x00, 0x00, 0xc0, 0x3f, 0x00, 0x00, 0x00, 0x00, 0x80, 0x1f,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x0e, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
"""
        PLUS = """
#define plus_width 12
#define plus_height 12
static unsigned char plus_bits[] = {
   0xf0, 0x00, 0x90, 0x00, 0x90, 0x00, 0x90, 0x00, 0x9f, 0x0f, 0x01, 0x08,
   0x01, 0x08, 0x9f, 0x0f, 0x90, 0x00, 0x90, 0x00, 0x90, 0x00, 0xf0, 0x00 };
"""
        MINUS = """
#define minus_width 12
#define minus_height 12
static unsigned char minus_bits[] = {
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff, 0x0f, 0x01, 0x08,
   0x01, 0x08, 0xff, 0x0f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
"""
        DEL = """
#define x_width 12
#define x_height 12
static unsigned char x_bits[] = {
   0x00, 0x00, 0x0e, 0x07, 0x92, 0x04, 0x62, 0x04, 0x04, 0x02, 0x08, 0x01,
   0x08, 0x01, 0x04, 0x02, 0x62, 0x04, 0x92, 0x04, 0x0e, 0x07, 0x00, 0x00 };
"""

        #self.fltwin.wm_geometry("1200x800") # Фикс. размер окна
        self.fltwin.deiconify()
        self.fltwin.title(_("Поиск по Семантическим структурам в формате RDF"))

        self.del_icon = BitmapImage(data=DEL, foreground="black", master=self.fltwin)
        self.plus_icon = BitmapImage(data=PLUS, foreground="black", master=self.fltwin)
        self.minus_icon = BitmapImage(data=MINUS, foreground="black", master=self.fltwin)
        self.openfile_icon = BitmapImage(data=OPEN, foreground="black", master=self.fltwin)
        self.search_icon = BitmapImage(data=SEARCH, foreground="black", master=self.fltwin)
        self.eye_icon = BitmapImage(data=EYE, foreground="black", master=self.fltwin)
        self.compare_icon = BitmapImage(data=CMP, foreground="black", master=self.fltwin)

        self.boxfont = tkFont.nametofont("TkDefaultFont")
        if int(self.boxfont.actual('size')) < 10: self.boxfont.configure(size=10)  # Если шрифт по-умолчанию слишком мелкий
        self.fltwin.option_add("*Font", self.boxfont)


        self.menu_bar = Menu(self.fltwin, relief=tk.FLAT) # Меню
        self.fltwin.config(menu=self.menu_bar)
        self.file_menu = Menu(self.menu_bar, tearoff=0)
        self.menu_bar.add_cascade(label=_("Файл"), menu=self.file_menu)
        self.sems_menu = Menu(self.menu_bar, tearoff=0)
        self.menu_bar.add_cascade(label=_("Структура"), menu=self.sems_menu)
        self.format_menu = Menu(self.menu_bar, tearoff=0)
        self.menu_bar.add_cascade(label=_("Формат"), menu=self.format_menu)
        self.font_menu = Menu(self.menu_bar, tearoff=0)
        self.menu_bar.add_cascade(label=_("Шрифт"), menu=self.font_menu)
        self.help_menu = Menu(self.menu_bar, tearoff=0)
        self.menu_bar.add_cascade(label=_("Справка"), menu=self.help_menu)



        self.sframe = Frame(self.fltwin) # Все элементы управления поиском
        self.sframe.pack(side = 'top', expand='n', fill='x', padx=6)

        self.sfframe = Frame(self.sframe) # Панель задания критериев поиска
        self.sfframe.pack(side = 'left', expand='y', fill='x', padx=6)

        self.sbframe = Frame(self.sframe)  # Панель кнопок
        self.sbframe.pack(side = 'left', expand='n', fill='both', padx=6)
        self.gofbutton = Button(self.sbframe, image=self.openfile_icon, text = _("Открыть"), relief=tk.FLAT)
        self.gofbutton.pack(side = 'top', pady=8)
        self.gsrbutton = Button(self.sbframe, image=self.search_icon, text = _("Найти"), relief=tk.FLAT)
        self.gsrbutton.pack(side = 'top', pady=10)
        self.gdrbutton = Button(self.sbframe, image=self.eye_icon, text = _("Показать"), relief=tk.FLAT)
        self.gdrbutton.pack(side = 'top', pady=8)

        self.sfiltframe = LabelFrame(self.sfframe, text=_(" Фильтр ")) # Панель задания фильтров
        self.sfiltframe.pack(side = 'top', expand='y', fill='x')

        self.sentfframe = Frame(self.sfiltframe) # Список фильтрации сущностей
        self.sentfframe.pack(side = 'top', expand='y', fill='x', padx=6)
        self.sentflabel = tk.Label(self.sentfframe, text=_("Сущности: "))
        self.sentflabel.pack(side = 'left')
        self.sentfdelbutton = Button(self.sentfframe, image=self.del_icon, relief=tk.FLAT, width=14, height=14)
        self.sentfdelbutton.pack(side = 'left', padx=0)
        self.sentflstentry = Entry(self.sentfframe, text='-EpistModality')
        self.sentflstentry.pack(side = 'left', expand='y', fill='x')
        self.sentfaddbutton = Button(self.sentfframe, image=self.plus_icon, relief=tk.FLAT, width=14, height=14)
        self.sentfaddbutton.pack(side = 'left', padx=0)
        self.sentfrembutton = Button(self.sentfframe, image=self.minus_icon, relief=tk.FLAT, width=14, height=14)
        self.sentfrembutton.pack(side = 'left', padx=0)
        self.sentfcentry = ttk.Combobox(self.sentfframe, width=20)
        self.sentfcentry.pack(side = 'left')
        self.sentfientry = ttk.Combobox(self.sentfframe, width=6)
        self.sentfientry.pack(side = 'left')

        self.srelfframe = Frame(self.sfiltframe) # Список фильтрации отношений
        self.srelfframe.pack(side = 'top', expand='y', fill='x', padx=6, pady=4)
        self.srelflabel = tk.Label(self.srelfframe, text=_("Отношения: "))
        self.srelflabel.pack(side = 'left')
        self.srelfdelbutton = Button(self.srelfframe, image=self.del_icon, relief=tk.FLAT, width=14, height=14)
        self.srelfdelbutton.pack(side = 'left', padx=0)
        self.srelflstentry = Entry(self.srelfframe, text='')
        self.srelflstentry.pack(side = 'left', expand='y', fill='x')
        self.srelfaddbutton = Button(self.srelfframe, image=self.plus_icon, relief=tk.FLAT, width=14, height=14)
        self.srelfaddbutton.pack(side = 'left', padx=0)
        self.srelfrembutton = Button(self.srelfframe, image=self.minus_icon, relief=tk.FLAT, width=14, height=14)
        self.srelfrembutton.pack(side = 'left', padx=0)
        self.srelfcentry = ttk.Combobox(self.srelfframe, width=20)
        self.srelfcentry.pack(side = 'left')


        self.schainframe = LabelFrame(self.sfframe, text=_(" Поиск цепочек ")) # Панель поиска цепочек
        self.schainframe.pack(side = 'top', expand='y', fill='x', pady=2)
        self.schdelbutton = Button(self.schainframe, image=self.del_icon, relief=tk.FLAT, width=14, height=14)
        self.schdelbutton.pack(side = 'left', padx=6)
        self.schentry = Entry(self.schainframe, text='')
        self.schentry.pack(side = 'left', expand='y', fill='x', pady=4)
        self.schaddbutton = Button(self.schainframe, image=self.plus_icon, relief=tk.FLAT, width=14, height=14)
        self.schaddbutton.pack(side = 'left', padx=0)
        self.sche1label = tk.Label(self.schainframe, text=_(" От: "))
        self.sche1label.pack(side = 'left')
        self.sche1entry = ttk.Combobox(self.schainframe, width=25)
        self.sche1entry.pack(side = 'left', pady=4)
        self.sche2label = tk.Label(self.schainframe, text=_(" До: "))
        self.sche2label.pack(side = 'left')
        self.sche2entry = ttk.Combobox(self.schainframe, width=25)
        self.sche2entry.pack(side = 'left', padx=6, pady=4)
        self.sche1entry.config(postcommand = lambda: self.update_combos_iref(srcg, self.sche1entry))
        self.sche2entry.config(postcommand = lambda: self.update_combos_iref(srcg, self.sche2entry))


        self.strifframe = LabelFrame(self.sfframe, text=_(" Поиск триплетов "))  # Панель поиска триплетов
        self.strifframe.pack(side = 'top', expand='y', fill='x', pady=2)

        self.stlstframe = Frame(self.strifframe) # Панель списка триплетов
        self.stlstframe.pack(side = 'top', expand='y', fill='x', padx=6, pady=0)
        self.strifdelbutton = Button(self.stlstframe, image=self.del_icon, relief=tk.FLAT, width=14, height=14)
        self.strifdelbutton.pack(side = 'left', padx=0)
        self.strilstentry = Entry(self.stlstframe, text='')
        self.strilstentry.pack(side = 'top', expand='y', fill='x', padx=6)

        self.stselframe = Frame(self.strifframe) # Панель ввода триплета
        self.stselframe.pack(side = 'top', expand='y', fill='x', padx=6, pady=2)

        self.strifaddbutton = Button(self.stselframe, image=self.plus_icon, relief=tk.FLAT, width=14, height=14)
        self.strifaddbutton.pack(side = 'left', padx=0)
        self.strifrembutton = Button(self.stselframe, image=self.minus_icon, relief=tk.FLAT, width=14, height=14)
        self.strifrembutton.pack(side = 'left', padx=0)

        self.ssframe1 = Frame(self.stselframe)
        self.ssframe1.pack(side = 'left', padx=10, pady=4)
        self.ss1label = tk.Label(self.ssframe1, text="S:", padx=5)
        self.ss1label.pack(side = 'left')
        self.ss1centry = ttk.Combobox(self.ssframe1, width=20)
        self.ss1centry.pack(side = 'left')
        self.ss1ientry = ttk.Combobox(self.ssframe1, width=6)
        self.ss1ientry.pack(side = 'left')
        self.ss1centry.config(postcommand = lambda: self.update_combos(srcg, None, self.ss1centry, self.ss1ientry, None, True, True))
        self.ss1ientry.config(postcommand = lambda: self.update_combos(srcg, None, self.ss1centry, self.ss1ientry, None, False, True))

        self.ssframe2 = Frame(self.stselframe)
        self.ssframe2.pack(side = 'left', padx=0, pady=4)
        self.ss2label = tk.Label(self.ssframe2, text="P:", padx=5)
        self.ss2label.pack(side = 'left')
        self.ss2rentry = ttk.Combobox(self.ssframe2, width=20)
        self.ss2rentry.pack(side = 'left')
        self.ss2rentry.config(postcommand = lambda: self.update_combos(srcg, None, None, None, self.ss2rentry, True, True))

        self.ssframe3 = Frame(self.stselframe)
        self.ssframe3.pack(side = 'left', padx=10, pady=4)
        self.ss3label = tk.Label(self.ssframe3, text="O:", padx=5)
        self.ss3label.pack(side = 'left')
        self.ss3centry = ttk.Combobox(self.ssframe3, width=20)
        self.ss3centry.pack(side = 'left')
        self.ss3ientry = ttk.Combobox(self.ssframe3, width=6)
        self.ss3ientry.pack(side = 'left')
        self.ss3centry.config(postcommand = lambda: self.update_combos(srcg, None, self.ss3centry, self.ss3ientry, None, True, True))
        self.ss3ientry.config(postcommand = lambda: self.update_combos(srcg, None, self.ss3centry, self.ss3ientry, None, False, True))


        self.vframe = Frame(self.fltwin, padx=4, pady=6, relief=tk.FLAT) # Два текстовых поля
        self.vframe.pack(expand='y', fill='both')
        self.vpane = PanedWindow(self.vframe, orient='horizontal')
        self.vsrcframe = Frame(self.vframe, bd=1, relief=tk.SUNKEN)
        self.srctoolframe = tk.Frame(self.vsrcframe, relief=tk.FLAT)
        self.srctoolframe.pack(side='top', expand='n',fill='x', padx=4)
        self.srccmbutton = Button(self.srctoolframe, image=self.compare_icon, text = _("Сравнить исходный граф"), relief=tk.FLAT)
        self.srccmbutton.pack(side = 'right', padx=2)

        self.srcinfframe = tk.Frame(self.srctoolframe, relief=tk.FLAT)
        self.srcinfframe.pack(side='right', expand='y',fill='x', padx=4)
        self.srcsrclabel = tk.Label(self.srcinfframe, anchor='w')
        self.srcsrclabel.pack(side='top', fill='x')
        self.srcstatlabel = tk.Label(self.srcinfframe, anchor='w')
        self.srcstatlabel.pack(side='top', fill='x')


        self.srcbox = scrolledtext.ScrolledText(self.vsrcframe, font=self.boxfont)
        self.srcbox.pack(side='top', expand='y', fill='both')
        self.srcbox.config(font=tkFont.Font(family=self.boxfont.actual('family'), size=int(self.boxfont.actual('size') * settings['fontscale'])))
        self.vpane.add(child=self.vsrcframe, stretch="always")

        self.vresframe = Frame(self.vframe, bd=1, relief=tk.SUNKEN)
        self.restoolframe = tk.Frame(self.vresframe, relief=tk.FLAT)
        self.restoolframe.pack(side='top', expand='n', fill='x', padx=4)
        self.rescmbutton = Button(self.restoolframe, image=self.compare_icon, text = _("Сравнить результат поиска"), relief=tk.FLAT)
        self.rescmbutton.pack(side = 'right', padx=2)

        self.resinfframe = tk.Frame(self.restoolframe, relief=tk.FLAT)
        self.resinfframe.pack(side='right', expand='y', fill='x', padx=4)
        self.ressrclabel = tk.Label(self.resinfframe, anchor='w')
        self.ressrclabel.pack(side='top', fill='x')
        self.resstatlabel = tk.Label(self.resinfframe, anchor='w')
        self.resstatlabel.pack(side='top', fill='x')
        self.resbox = scrolledtext.ScrolledText(self.vresframe, font=self.boxfont)
        self.resbox.pack(side='top', expand='y', fill='both')
        self.resbox.config(font=tkFont.Font(family=self.boxfont.actual('family'), size=int(self.boxfont.actual('size') * settings['fontscale'])))
        self.vpane.add(child=self.vresframe, stretch="always")
        self.vpane.pack(fill='both', expand='y')


        # Команды и обработка событий
        # Команды меню
        self.file_menu.add_command(label=_("Открыть"), command=lambda: self.open_file(self.fltwin, self.srcbox, self.srcsrclabel, self.srcstatlabel, srcg, settings, rtparam))
        self.file_menu.add_command(label=_("Сохранить"), command=lambda: self.save_file(self.fltwin, self.resbox, self.srcstatlabel, outg, settings))
        self.file_menu.add_command(label=_("Выход"), command=lambda: self.app_quit(self.fltwin))
        self.font_menu.add_command(label=_("Мелкий"), command=lambda: self.set_font(self.srcbox, self.resbox, self.boxfont, settings, 1))
        self.font_menu.add_command(label=_("Средний"), command=lambda: self.set_font(self.srcbox, self.resbox, self.boxfont, settings, 1.5))
        self.font_menu.add_command(label=_("Крупный"), command=lambda: self.set_font(self.srcbox, self.resbox, self.boxfont, settings, 2))
        self.help_menu.add_command(label=_("Описание"), command=lambda: self.show_help())

        fmtvar = tk.StringVar()
        fmtvar.set(settings['format'])
        fmtvar.trace_add('write', lambda var, indx, mode: self.set_format(self.fltwin, self.srcbox, self.resbox, fmtvar.get(), srcg, outg, settings))
        for f in settings['formats']: self.format_menu.add_radiobutton(label=f, value=f, variable=fmtvar)

        semsvar = tk.StringVar()
        semsvar.set(rtparam['sems'])
        semsvar.trace_add('write', lambda var, indx, mode: self.set_sems(semsvar.get(), self.fltwin, self.srcbox, self.resbox, self.sentflstentry, self.srelflstentry, self.schentry, self.strilstentry, self.srcsrclabel, self.srcstatlabel, self.ressrclabel, self.resstatlabel, srcg, outg, settings, rtparam))
        self.sems_menu.add_radiobutton(label=_("Всё"), value='all', variable=semsvar)
        self.sems_menu.add_radiobutton(label=_("Только базовая"), value='bsems', variable=semsvar)
        self.sems_menu.add_radiobutton(label=_("Только промежуточная"), value='isems', variable=semsvar)
        self.sems_menu.add_radiobutton(label=_("Только расширенная"), value='esems', variable=semsvar)
        self.sems_menu.add_radiobutton(label=_("Базовая и промежуточная"), value='bisems', variable=semsvar)
        self.sems_menu.add_radiobutton(label=_("Промежуточная и расширенная"), value='iesems', variable=semsvar)
#        self.sems_menu.bind('<FocusIn>', lambda event: self.update_submenu(self.sems_menu, semsvar, rtparam['semstructs'], {'bsems': _("Базовая"), 'isems': _("Расширенная"), 'esems': _("Полная")}, self.fltwin, self.srcbox, self.resbox))

        # Действия кнопок редактирования фильтров
        self.sentfcentry.config(postcommand = lambda: self.update_combos(srcg, None, self.sentfcentry, self.sentfientry, None, True, False))
        self.sentfientry.config(postcommand = lambda: self.update_combos(srcg, None, self.sentfcentry, self.sentfientry, None, False, False))
        self.sentfcentry.bind('<Escape>', lambda event: self.sentfcentry.delete(0, tk.END))
        self.srelfcentry.bind('<Escape>', lambda event: self.srelfcentry.delete(0, tk.END))
        self.sche1entry.bind('<Escape>', lambda event: self.sche1entry.delete(0, tk.END))
        self.sche2entry.bind('<Escape>', lambda event: self.sche2entry.delete(0, tk.END))
        self.ss1centry.bind('<Escape>', lambda event: self.ss1centry.delete(0, tk.END))
        self.ss2rentry.bind('<Escape>', lambda event: self.ss2rentry.delete(0, tk.END))
        self.ss3centry.bind('<Escape>', lambda event: self.ss3centry.delete(0, tk.END))

        self.sentfaddbutton.config(command = lambda: self.append_filter(srcg, self.sentflstentry, '+', None, self.sentfcentry, self.sentfientry, 'cls'))
        self.sentfrembutton.config(command = lambda: self.append_filter(srcg, self.sentflstentry, '-', None, self.sentfcentry, self.sentfientry, 'cls'))
        self.sentfdelbutton.config(command = lambda: self.clear_filter(self.sentflstentry, 'cls'))
        self.sentflstentry.bind('<FocusOut>', lambda event: self.update_filter(srcg, self.sentflstentry, 'cls'))
        self.sentflstentry.bind('<Delete>', lambda event: self.update_filter(srcg, self.sentflstentry, 'cls'))
        self.sentflstentry.bind('<Control-a>', self.entry_select_all)
        self.sentflstentry.bind('<Control-A>', self.entry_select_all)

        self.srelfcentry.config(postcommand = lambda: self.update_combos(srcg, None, None, None, self.srelfcentry, True, False))
        self.srelfaddbutton.config(command = lambda: self.append_filter(srcg, self.srelflstentry, '+', None, self.srelfcentry, None, 'rel'))
        self.srelfrembutton.config(command = lambda: self.append_filter(srcg, self.srelflstentry, '-', None, self.srelfcentry, None, 'rel'))
        self.srelfdelbutton.config(command = lambda: self.clear_filter(self.srelflstentry, 'rel'))
        self.srelflstentry.bind('<FocusOut>', lambda event: self.update_filter(srcg, self.srelflstentry, 'rel'))
        self.srelflstentry.bind('<Delete>', lambda event: self.update_filter(srcg, self.srelflstentry, 'rel'))
        self.srelflstentry.bind('<Control-a>', self.entry_select_all)
        self.srelflstentry.bind('<Control-A>', self.entry_select_all)

        self.schaddbutton.config(command = lambda: self.append_filter_chain(srcg, self.schentry, '', self.sche1entry, self.sche2entry))
        self.schdelbutton.config(command = lambda: self.clear_filter(self.schentry, 'chn'))
        self.schentry.bind('<FocusOut>', lambda event: self.update_filter_chain(srcg, self.schentry))
        self.schentry.bind('<Delete>', lambda event: self.update_filter_chain(srcg, self.schentry))
        self.schentry.bind('<Control-a>', self.entry_select_all)
        self.schentry.bind('<Control-A>', self.entry_select_all)

        self.strifaddbutton.config(command = lambda: self.append_filter_triplet(srcg, self.strilstentry, '+', self.ss1centry, self.ss1ientry, self.ss2rentry, self.ss3centry, self.ss3ientry))
        self.strifrembutton.config(command = lambda: self.append_filter_triplet(srcg, self.strilstentry, '-', self.ss1centry, self.ss1ientry, self.ss2rentry, self.ss3centry, self.ss3ientry))
        self.strifdelbutton.config(command = lambda: self.clear_filter(self.strilstentry, 'tri'))
        self.strilstentry.bind('<FocusOut>', lambda event: self.update_filter_triplet(srcg, self.strilstentry))
        self.strilstentry.bind('<Delete>', lambda event: self.update_filter_triplet(srcg, self.strilstentry))
        self.strilstentry.bind('<Control-a>', self.entry_select_all)
        self.strilstentry.bind('<Control-A>', self.entry_select_all)


        # Действия кнопок поиска и отрисовки
        self.gofbutton.config(command = lambda: self.open_file(self.fltwin, self.srcbox, self.srcsrclabel, self.srcstatlabel, srcg, settings, rtparam)) # Открыть файл
        self.gdrbutton.config(command = lambda: self.draw_graph(outg, rtparam))
        self.gdrbutton.bind('<Map>', lambda event: self.disable_unless(self.gdrbutton, rtparam['have_graphviz']))
        self.gsrbutton.config(command = lambda: self.filter_and_box_update(self.fltwin, self.resbox, self.sentflstentry, self.srelflstentry, self.schentry, self.strilstentry, self.ressrclabel, self.resstatlabel, srcg, outg, settings['format']))
        self.gsrbutton.bind_all('<Control-s>', lambda event: self.filter_and_box_update(self.fltwin, self.resbox, self.sentflstentry, self.srelflstentry, self.schentry, self.strilstentry, self.ressrclabel, self.resstatlabel, srcg, outg, settings['format']))
        self.gsrbutton.bind_all('<Control-S>', lambda event: self.filter_and_box_update(self.fltwin, self.resbox, self.sentflstentry, self.srelflstentry, self.schentry, self.strilstentry, self.ressrclabel, self.resstatlabel, srcg, outg, settings['format']))
        self.gsrbutton.bind_all('<Control-f>', lambda event: self.filter_and_box_update(self.fltwin, self.resbox, self.sentflstentry, self.srelflstentry, self.schentry, self.strilstentry, self.ressrclabel, self.resstatlabel, srcg, outg, settings['format']))
        self.gsrbutton.bind_all('<Control-F>', lambda event: self.filter_and_box_update(self.fltwin, self.resbox, self.sentflstentry, self.srelflstentry, self.schentry, self.strilstentry, self.ressrclabel, self.resstatlabel, srcg, outg, settings['format']))
        self.gsrbutton.bind_all('<Control-g>', lambda event: self.draw_graph(outg, rtparam))
        self.gsrbutton.bind_all('<Control-G>', lambda event: self.draw_graph(outg, rtparam))
        self.srccmbutton.config(command = lambda: self.call_compare_graphs(srcg, None, _("исходный граф"), settings, rtparam)) # Вызов программы сравнения
        self.srccmbutton.bind('<Map>', lambda event: self.disable_unless(self.srccmbutton, rtparam['have_rdfcomp']))
        self.rescmbutton.config(command = lambda: self.call_compare_graphs(outg, None, _("результат поиска"), settings, rtparam)) # Вызов программы сравнения
        self.rescmbutton.bind('<Map>', lambda event: self.disable_unless(self.rescmbutton, rtparam['have_rdfcomp']))

        self.srcbox.bind('<Map>', lambda event: self.box_and_status_update(self.fltwin, self.srcbox, self.srcsrclabel, self.srcstatlabel, srcg, settings['format']))
        self.srcbox.bind_all('<<Paste>>', lambda event: self.paste_and_box_update(self.srcbox, self.srcsrclabel, self.srcstatlabel, srcg, settings['format'], rtparam['sems'], rtparam['username'])) # Возможность вставить rdf для поиска
        self.srcbox.bind('<FocusOut>', lambda event: self.parse_and_box_update(self.srcbox, self.srcsrclabel, self.srcstatlabel, srcg, settings['format'], rtparam['sems'], rtparam['username']))  # Для обработки ручной редактуры rdf
        self.srcbox.bind('<Delete>', lambda event: self.parse_and_box_update(self.srcbox, self.srcsrclabel, self.srcstatlabel, srcg, settings['format'], rtparam['sems'], rtparam['username']))  # Для обработки ручной редактуры rdf
        self.srcbox.bind('<Control-x>', lambda event: self.parse_and_box_update(self.srcbox, self.srcsrclabel, self.srcstatlabel, srcg, settings['format'], rtparam['sems'], rtparam['username']))  # Для обработки ручной редактуры rdf
        self.srcbox.bind('<Control-X>', lambda event: self.parse_and_box_update(self.srcbox, self.srcsrclabel, self.srcstatlabel, srcg, settings['format'], rtparam['sems'], rtparam['username']))  # Для обработки ручной редактуры rdf
        self.srcbox.bind('<Control-a>', self.box_select_all)
        self.srcbox.bind('<Control-A>', self.box_select_all)
        self.srcbox.bind('<Control-r>', lambda event: self.box_update(self.fltwin, self.srcbox, srcg, settings['format']))
        self.srcbox.bind('<Control-R>', lambda event: self.box_update(self.fltwin, self.srcbox, srcg, settings['format']))

        #self.resbox.bind('<Map>',  lambda event: self.set_font(self.srcbox, self.resbox, self.boxfont, settings, settings['fontscale'])) # При первой отрисовке установить шрифт srcbox и resbox
        self.resbox.bind('<<Paste>>',  self.kill_event)
        self.resbox.bind('<Up>',  self.dummy_event)
        self.resbox.bind('<Down>',  self.dummy_event)
        self.resbox.bind('<Control-a>', self.box_select_all)
        self.resbox.bind('<Control-A>', self.box_select_all)
        self.resbox.bind('<Key>',  self.kill_event)

        #self.fltwin.mainloop()


######################################
#    def update_submenu(self, menu, var, lst, names, fltwin, srcbox, resbox): # Обновить пункты меню с радиокнопками
#        print("semsvar=",var)
#        self.sems_menu.add_radiobutton(label=_("Базовая"), value='bsems', variable=semsvar, command=lambda: self.set_sems(self.fltwin, self.srcbox, self.resbox, 'bsems', rtparam))
#        for rb in menu.enumerate(): print('	',rb)


    def set_sems(self, sems, fltwin, srcbox, resbox, sentflstentry, srelflstentry, schentry, strilstentry, srclabel, srcstatlabel, ressrclabel, resstatlabel, srcg, outg, settings, rtparam): # Переключение между сегментами графов от ЭТАП4
        rtparam.update({'sems': sems}) # Запомним выбор
        parsed = parserdf(srcg, settings['format'], sems, rtparam['username'])
        self.statusbar_update(srcg, srclabel, srcstatlabel)
        self.box_update(fltwin, srcbox, srcg, settings['format'])
        if parsed and len(outg['grf']) > 0: # Если есть какие-то результаты поиска - их надо обновить
            self.filter_and_box_update(fltwin, resbox, sentflstentry, srelflstentry, schentry, strilstentry, ressrclabel, resstatlabel, srcg, outg, settings['format'])

    def set_font(self, box1, box2, boxfont, settings, scale): # Изменяет размер шрифта для текстовых полей srcboх, resbox
        settings.update({'fontscale': scale})
        fname = boxfont.actual('family')
        fsize = int(boxfont.actual('size') * scale)
        box1.config(font=tkFont.Font(family=fname, size=fsize))
        box2.config(font=tkFont.Font(family=fname, size=fsize))

    def set_format(self, fltwin, sbox, rbox, f, srcg, outg, settings):
        settings.update({'format': f})
        self.box_update(fltwin, sbox, srcg, f)
        self.box_update(fltwin, rbox, outg, f)

    def get_items_by_key(self, cb, items, clear_ecb, suggest_var): # Сбор содержимого выпадающего списка cb с учетом ключа поиска
        lst = []
        skey = cb.get()
        for i in items.keys():
            if (skey and re.match(skey, i, re.IGNORECASE)) or not skey: lst.append(i)
        extra = []
        if suggest_var: extra = ['?a', '?b', '?c']
        cb['values'] = tuple(extra + sorted(lst))
        if clear_ecb: cb.delete(0, tk.END)

    def update_combos(self, g, nscb, ecb, icb, rcb, clear_ecb, suggest_var):
        if nscb and 'pfx' in g['edict']: nscb['values'] = tuple(sorted(g['edict']['pfx'].keys())) # TODO Нужно добавить взаимную фильтрацию!
        if ecb and 'cls' in g['edict']: self.get_items_by_key(ecb, g['edict']['cls'], clear_ecb, suggest_var)
        if icb:
            icb.delete(0, tk.END)
            e = ecb.get()
            if e and e in g['edict']['cls']:
                try:
                    extra = []
                    if suggest_var: extra = ['?x', '?y', '?z']
                    icb['values'] = tuple(extra + sorted( g['edict']['cls'][e]['i'].keys() ))
                except KeyError: pass
        if rcb and 'rel' in g['edict']: self.get_items_by_key(rcb, g['edict']['rel'], clear_ecb, suggest_var)

    def update_combos_iref(self, g, cb): # Списки человеческих имен индивидов для поиска цепочек
        self.get_items_by_key(cb, g['edict']['iref'], True, False)
        cb.delete(0, tk.END)

    def make_entity_txt(self, pfx, e, i): # Сборка человеческого имени сущности из компонентов
        ind = append_str(pfx, e, ':')
        ind = append_str(ind, i, '_')
        return ind

    def make_triplet_txt(self, sns, se, si, rns, r, ons, oe, oi): # Сборка текстового представления триплета - хэш-ключа
        s = self.make_entity_txt(sns, se, si)
        r = self.make_entity_txt(rns, r, '')
        o = self.make_entity_txt(ons, oe, oi)
        if not s: s = '*'
        if not r: r = '*'
        if not o: o = '*'
        return s+' '+r+' '+o

    def filters_to_string_ref(self, key): # Сбор строки фильтров цепочек или триплетов
        s = ''
        if key in filters:
            for ref in sorted(filters[key]):
                s = append_str(s, filters[key][ref]['action']+ref, ',') #flst_delim
        return s

    def getvals(self, pcb, ecb, icb): # Получение выбранных с помощью выпадающих списков значений для append_filters*
        e = ''; pfx = ''; i = ''
        if pcb: pfx = pcb.get()
        if ecb: e = ecb.get()
        if icb: i = icb.get()
        return pfx, e, i

    def valid_ref(self, g, pfx, e, i, action):
        try:
            if not action == '+' and not action == '-': return False
            if pfx and not pfx in g['edict']['pfx']: return False # Отсев несуществующих
            if e and not e in g['edict']['cls'] and not e in g['edict']['rel'] and not re.match('\?', e): return False
            if i and not i in g['edict']['ind'] and not re.match('\?', i): return False # Разрешаем ?имя_переменной в качестве номера индивида
            return True
        except KeyError: # Граф не загружен и его словарь пуст
            return False

    def prune_ref_list(self, f, newpfx, newe, newi, newaction): # Удаление противоречащих запросов при добавлении нового
        bads = []
        for s in f:
            ns, pfx, e, i = parseURI(s)
            action = f[s]['action']

            if pfx == newpfx and e == newe: # различия только в номере индивида
                if i == newi: bads.append(s) # Нельзя иметь две одинаковых сущности, особенно с разными действиями
                elif action == newaction:
                    if newi and i == '': bads.append(s) # Добавляется индивид, но уже есть запрос найти общий класс
                    if newi == '' and i: bads.append(s) # Добавляется класс, но уже есть запрос найти индивид этого класса

        for s in bads: f.pop(s)

    def append_filter(self, g, entry, action, pcb, ecb, icb, key): # Дополнение параметров фильтра по кнопке
        pfx, e, i = self.getvals(pcb, ecb, icb)
        if self.valid_ref(g, pfx, e, i, action):
            if key in filters: self.prune_ref_list(filters[key], pfx, e, i, action)
            s = self.make_entity_txt(pfx, e, i) # человеческое имя сущности
            deep_update(filters, {key: {s: {'action': action}}})
            entry.delete(0, tk.END)
            entry.insert(0, self.filters_to_string_ref(key))

    def update_filter(self, g, entry, key): # Перезапись параметров фильтра по тексту
        s = entry.get()
        self.clear_filter(entry, key)

        lst = s.rsplit(',') #flst_delim
        for s in lst:
            if s and re.match('^[\+\-]', s):
                action = s[:1]
                s = s[1:]
                ns, pfx, e, i = parseURI(s)
                if self.valid_ref(g, pfx, e, i, action):
                    if key in filters: self.prune_ref_list(filters[key], pfx, e, i, action)
                    deep_update(filters, {key: {s: {'action': action}}})
        entry.insert(0, self.filters_to_string_ref(key))

    def append_filter_chain(self, g, entry, action, stcb, endcb): # Дополнение параметров фильтра цепочек
        sn = stcb.get(); en = endcb.get()
        if sn and en and not sn == en:
            if sn in g['edict']['iref'] and en in g['edict']['iref']: # Защита от несуществующих в графе индивидов
                chn = append_str(sn, en, ' ')
                deep_update(filters, {'chn': {chn: {'s': sn}}})
                deep_update(filters, {'chn': {chn: {'e': en}}})
                deep_update(filters, {'chn': {chn: {'action': action}}})

        entry.delete(0, tk.END)
        entry.insert(0, self.filters_to_string_ref('chn'))

    def update_filter_chain(self, g, entry): # Перезапись параметров фильтра по тексту
        s = entry.get()
        self.clear_filter(entry, 'chn')

        lst = s.rsplit(',') #flst_delim # Список шаблонов триплетов
        for chn in lst:
            chn = chn.strip()
            if chn:
                sn = ''; en = ''
                try: sn, en = chn.rsplit(' ')
                except: return

                if (g['edict']['iref'] and sn and sn in g['edict']['iref']) and (en and en in g['edict']['iref']) and not sn == en: # оба узла существуют и отличаются друг от друга
                    deep_update(filters, {'chn': {chn: {'s': sn}}})
                    deep_update(filters, {'chn': {chn: {'e': en}}})
                    deep_update(filters, {'chn': {chn: {'action': ''}}})

        entry.delete(0, tk.END)
        entry.insert(0, self.filters_to_string_ref('chn'))


    def append_filter_triplet(self, g, entry, action, secb, sicb, rcb, oecb, oicb): # Дополнение параметров фильтра по кнопке триплетом
        spfx, se, si = self.getvals(None, secb, sicb)
        rpfx, r, ri = self.getvals(None, rcb, None)
        opfx, oe, oi = self.getvals(None, oecb, oicb)
        if se or oe or r:
            if self.valid_ref(g, spfx, se, si, action) and self.valid_ref(g, rpfx, r, ri, action) and self.valid_ref(g, opfx, oe, oi, action):  # Защита от несуществующих в графе сущностей
                triplet = self.make_triplet_txt(spfx, se, si, rpfx, r, opfx, oe, oi)
                deep_update(filters, {'tri': {triplet: {'s': self.make_entity_txt(spfx, se, si)}}})
                deep_update(filters, {'tri': {triplet: {'r': self.make_entity_txt(rpfx, r, ri)}}})
                deep_update(filters, {'tri': {triplet: {'o': self.make_entity_txt(opfx, oe, oi)}}})
                deep_update(filters, {'tri': {triplet: {'action': action}}})

        entry.delete(0, tk.END)
        entry.insert(0, self.filters_to_string_ref('tri'))


    def update_filter_triplet(self, g, entry): # Перезапись параметров фильтра по тексту
        s = entry.get()
        self.clear_filter(entry, 'tri')
        lst = s.rsplit(',') #flst_delim # Список шаблонов триплетов
        for triplet in lst:
            triplet = triplet.strip()
            if triplet:
                action = ''
                try: action = re.match('[\+\-]', triplet).group(0)
                except: pass
                triplet = re.sub(' +', ' ', triplet)
                triplet = re.sub(r'^[\+\-] *', '', triplet) # Уберем префикс action
                s = ''; p = ''; o = ''
                try: s, p, o = triplet.rsplit(' ')
                except: pass

                if s == '*': s = ''
                if p == '*': p = ''
                if o == '*': o = ''
                sns, spfx, se, si = parseURI(s)
                rns, rpfx, r, ri = parseURI(p)
                ons, opfx, oe, oi = parseURI(o)

                if action and (se or oe or r): # Требуется тип списка и хотя бы один член тройки
                    if self.valid_ref(g, spfx, se, si, action) and self.valid_ref(g, rpfx, r, ri, action) and self.valid_ref(g, opfx, oe, oi, action):  # Защита от несуществующих в графе сущностей
                        deep_update(filters, {'tri': {triplet: {'s': s}}})
                        deep_update(filters, {'tri': {triplet: {'r': p}}})
                        deep_update(filters, {'tri': {triplet: {'o': o}}})
                        deep_update(filters, {'tri': {triplet: {'action': action}}})

        entry.delete(0, tk.END)
        entry.insert(0, self.filters_to_string_ref('tri'))

    def clear_filter(self, entry, key):
        entry.delete(0, tk.END)
        if key in filters: filters.pop(key) # Здесь важно полностью удалить ключ, так как наличие ключей включает прцедуры фильтрации троек

    def entry_select_all(self, event): # Выделить весь текст в строке ввода
        entry = event.widget
        entry.select_range(0,END)
        return "break"

    def box_select_all(self, event): # Выделить весь текст в текстовом поле
        box = event.widget
        box.tag_add(tk.SEL, 1.0, tk.END)
        box.mark_set(tk.INSERT, 1.0)
        box.see(tk.INSERT)
        return "break"

    def srclabel_update(self, g, label): # Обновление строки источника данных графа
        if 'file' in g['src']: label.config(text=append_str(_("Файл:"), g['src']['file'], ' '), bg=self.fltwin.cget("bg"))
        elif 'app' in g['src']: label.config(text=append_str(_("Получено от"), g['src']['app'], ' '), bg='green yellow')
        elif 'label' in g['src']: label.config(text=g['src']['label'], bg=self.fltwin.cget("bg"))
        else: label.config(text=_("Нет данных"), bg='red')

    def statusbar_update(self, g, srclabel, statlabel):
        self.srclabel_update(g, srclabel)
        if 'triples' in g['stat'] and g['stat']['triples'] > 0:
            stattxt = _("%s триплетов") % str(g['stat']['triples'])
            if 'nodes' in g['stat']: stattxt += _(", %s узлов ") % str(g['stat']['nodes'])
            if 'inodes' in g['stat']: stattxt += _(" (%s индивидов / %s классов)") % (str(g['stat']['inodes']), str(g['stat']['cnodes']))
            statlabel.config(text = stattxt)
        else: statlabel.config(text = '')

     #   else: srclabel.config(text = _(" Нет данных или текст RDF не соответствует формату %s") % settings['format'])

    def box_update(self, fltwin, box, g, fmt): # Заменить текст в текстовом поле сериализацией графа
        box.delete(1.0, tk.END)
        fltwin.update()
        box.insert(1.0, serializerdf(fmt, g))

    def box_and_status_update(self, fltwin, box, srclabel, srcstatlabel, g, fmt): # Заменить текст в текстовом поле сериализацией графа
        box.delete(1.0, tk.END)
        self.statusbar_update(g, srclabel, srcstatlabel)
        Tk.update(fltwin)
        box.insert(1.0, serializerdf(fmt, g))

    def paste_and_box_update(self, box, srclabel, srcstatlabel, g, fmt, sems, username): # Разбор исходного графа из содержимого srcbox после вставки из буфера и обновление srcbox и статуса
        g.update({'txt': box.get("1.0", tk.END)}) # содержимое srcbox в srcg
        parsed = parserdf(g, fmt, sems, username)
        if parsed:
            if len(g['src']) == 0: g['src'].update({'label': _("Вставлено из буфера")})
            self.statusbar_update(g, srclabel, srcstatlabel)
        else:
            g['src'].clear()
            self.statusbar_update(g, srclabel, srcstatlabel)

    def parse_and_box_update(self, box, srclabel, srcstatlabel, g, fmt, sems, username): # Разбор исходного графа из содержимого srcbox и обновление srcbox и статуса
        if fmt == 'etalog': return
        g.update({'txt': box.get("1.0", tk.END)}) # содержимое srcbox в srcg
        parsed = parserdf(g, fmt, sems, username)
        if not parsed: g['src'].clear() # Если после редактирования графа не получилось - "нет данных"
        self.statusbar_update(g, srclabel, srcstatlabel)

    def filter_and_box_update(self, fltwin, box, sentflstentry, srelflstentry, schentry, strilstentry, reslabel, resstatlabel, srcg, outg, fmt): # Применить фильтр и обновить текст результата (условия в filters обновляются другими событиями)
        if len(srcg['grf']) > 0:
            self.update_filter(srcg, sentflstentry, 'cls') # Обновление условий фильтра для учета возможной правки текста вручную. Нужно из-за запаздывания в событиях FocusOut
            self.update_filter(srcg, srelflstentry, 'rel')
            self.update_filter_chain(srcg, schentry)
            self.update_filter_triplet(srcg, strilstentry)
            apply_filter(srcg, outg, filters, fdict)
            outg['src'].update({'label': _("Результат поиска")})
            self.statusbar_update(outg, reslabel, resstatlabel)
            Tk.update(fltwin)
            self.box_update(fltwin, box, outg, fmt)
        else: message('inf', _("Не загружен исходный граф. Откройте файл RDF или вставьте текст RDF из буфера в левое поле."))

    def disable_unless(self, widget, flag): # Отключение кнопок при отсутствии внешних инструментов
        if not flag: widget['state'] = DISABLED

    def dummy_event(self, event): # Позволить виджету обработать кнопку
        return

    def kill_event(self, event): # Отменить обработку события
        return "break"

    def app_quit(self, fltwin): # Выход с закрытием окна
        fltwin.quit()
        fltwin.destroy()
        exit()

    def open_file(self, fltwin, box, srclabel, statusbar, srcg, settings, rtparam): # Открыть исходный RDF через меню
        try:
            inputfile = filedialog.askopenfile(parent=fltwin, title=_("RDF-граф для поиска"), mode="r", filetypes=(("RDF", ("*.rdf", "*.ttl", "*.n3", "*.txt")), (_("Любые файлы"), "*.*")) ).name
            srcg.update({'txt': read_rdffile(inputfile)})
            parsed = parserdf(srcg, settings['format'], rtparam['sems'], rtparam['username'])
            srcg['src'].clear()
            if parsed: srcg['src'].update({'file': re.sub(r'.*[\/\\]', '', inputfile)}) # Сократим имя, чтобы помещалось в окошке
            self.box_and_status_update(fltwin, box, srclabel, statusbar, srcg, settings['format'])
        except: pass

    def save_file(self, fltwin, box, statusbar, outg, settings): # Открыть исходный RDF через меню
        try:
            if len(outg['grf']) == 0:
                message('inf', _("Нечего сохранять. Перед записью в файл следует задать критерии поиска и получить результат."))
                return
            outputfile = filedialog.asksaveasfile(parent=fltwin, title=_("Сохранение результата поиска в RDF"), mode="w", defaultextension=".txt", ).name
            save_rdffile(outputfile, serializerdf(settings['format'], outg))
        except: pass

    def node_color(self, uri, i, other_uri): # Подготовка графического изображения через graphviz
        if 'chn' in fdict:
            if '+' in fdict['chn']:
                for t in fdict['chn']['+']:
                    if uri in fdict['chn']['+'][t]: # and other_uri in fdict['tri']['+'][t]:
                        if i: return 'gold'
                        else: return 'goldenrod'

        if 'tri' in fdict:
            if '+' in fdict['tri']:
                for t in fdict['tri']['+']: # Сначала все S
                    if uri in fdict['tri']['+'][t]['s'] and other_uri in fdict['tri']['+'][t]['o']:
                        if i: return 'olivedrab1'
                        else: return 'olivedrab3'
                for t in fdict['tri']['+']:
                    if uri in fdict['tri']['+'][t]['o'] and other_uri in fdict['tri']['+'][t]['s']:
                        if i: return 'palegreen1'
                        else: return 'palegreen3'

            if '-' in fdict['tri']:
                for t in fdict['tri']['-']: # Если найдено другим типом фильтра при сложении результатов
                    if (uri in fdict['tri']['-'][t]['s'] and other_uri in fdict['tri']['-'][t]['o']) or (uri in fdict['tri']['-'][t]['o'] and other_uri in fdict['tri']['-'][t]['s']):
                        return 'dodgerblue'

        if 'cls' in fdict and '+' in fdict['cls'] and uri in fdict['cls']['+']:
            if i: return 'red'
            else: return 'red3'

        if 'cls' in fdict and '-' in fdict['cls'] and uri in fdict['cls']['-']: return 'dodgerblue' # Если найдено другим типом фильтра при сложении результатов

        if not i: return 'lightgrey'
        return 'white'

    def call_compare_graphs(self, grf_a, grf_b, msg, settings, rtparam): # Вызов программы сравнения
        if len(grf_a['grf']) > 0:
            rtparam.update({'rdfcomp_cmpmode': 'standard'})
            rtparam.update({'rdfcomp_guimode': True})
            grf_a['src'].update({'app': _('RDFilter (%s)') % msg}) # Параметры запуска и обновление метаданных передаваемого графа
            rdfcomp.compare(settings, rtparam, {}, grf_a, None,  None, None, None)
        else: message ('inf', _("Нечего сравнивать."))

    def draw_graph(self, g, rtparam): # Графический вывод графа
        if len(g['grf']) > 0:
            dot = Digraph(comment=_('Результат поиска'), format='jpg') # https://www.graphviz.org/doc/info/attrs.html
            dot.engine = 'sfdp' # dot, neato, fdp, sfdp
            dot.overlap = 'prism1000'
            dot.overlap_scaling = '1000'
            dot.overlap_shrink = '1000'
            dot.splines = 'curved'
            dot.esep = '+500,300'
            for s,p,o in g['grf']:  # Перебор всех триплетов
                ns, pfx, e, i = parseURI(s);
                if i: sname = "_".join([e, i])
                else: sname = e
                dot.node(sname, sname, style='filled', fillcolor = self.node_color(s, i, o))

                ns, pfx, e, i = parseURI(o);
                if i: oname = "_".join([e, i])
                else: oname = e
                dot.node(oname, oname, style='filled', fillcolor = self.node_color(o, i, s))

                ns, pfx, e, i = parseURI(p); pn = e
                dot.edge(sname, oname, label = pn, fontsize='9')
            dotfilename = append_str('rdfsearchres', rtparam['username'], '-')
            dot.render('rdfilter_tmp/'+dotfilename+'.gv', view=True, cleanup=True)
        else: message ('inf', _("Нечего рисовать."))

    def show_help(self):
        self.helpwin = tk.Toplevel(master = self.fltwin)
        self.helpwin.title(_("Справка"))
        #self.helpwin.attributes('-topmost', 1)
        self.helpbox = scrolledtext.ScrolledText(self.helpwin, wrap=tk.WORD)
        self.helpbox.config(width=160, height=60)
        self.helpbox.pack(fill='both', expand='y')
        self.helpbox.bind('<Key>',  self.kill_event)
        self.helpbox.insert(1.0, _("""RDF фильтр 1.9"""), 'title', _("""


    С помощью этого инструмента можно находить интересующие вас утверждения в графе RDF. Граф можно загрузить в текстовом виде из командной строки, с помощью меню "Файл/Открыть" или вставить из буфера обмена.
"""), 'body', _("""

Форматы графов RDF
"""), 'header', _("""
    Доступные форматы RDF-текста: turtle, nt, xml, etalog. Формат etalog отличается удобством чтения и используется в лингвистическом процессоре ЭТАП4.

    Граф семантической структуры предложения, которую строит семантический анализатор ЭТАП-4, может быть разделен с помощью специального комментария в тексте RDF на сегменты Базовой, Промежуточной и Расширенной семантической структуры. При наличии такого деления в загруженном тексте RDF вы можете выбрать желаемый сегмент с помощью меню "Структура". Полная структура используется по умолчанию и включает в себя все имеющиеся в графе триплеты. При выборе Базовой или Промежуточной структуры программа выделит часть загруженного графа и будет игнорировать остальные данные. Такое деление возможно только в форматах эталог и turtle.

    Имена узлов графа (URI) могут содержать необязательный суффикс, отделяемый символом '_'. Внутри могут быть два числа, разделенных тем же символом. Такой суффикс может использоваться для обозначения разных индивидов одного класса и имеет специальные поля выбора справа от полей ввода онтологического класса узла.
"""), 'body', _("""

Интерфейс поиска
"""), 'header', _("""
    Окно программы состоит из двух частей: зоны задания фильтров и зоны просмотра RDF внизу. Справа от зоны задания фильтров находятся кнопки открытия файла (папка), поиска (лупа) и графического просмотра (глаз). В левом текстовом поле находится исходный граф. В правом - результаты поиска в нем. Текстовые поля имеют заголовки, где можно видеть источник графа, число триплетов в нем, а также кнопки вызова программы сравнения. При их нажатии соответствующий граф будет передан на сравнение с другим графом.

    Есть три типа фильтров: фильтры сущностей, фильтр триплетов и фильтр поиска цепочек узлов. Одновременно можно задать много фильтров разных типов.
"""), 'body', _("""

Поиск сущностей
"""), 'header', _("""
Позволяет находить триплеты (не) содержащие заданные классы и индивиды в любой позиции и/или отношения. Фильтр сущностей имеет режимы черного списка (-) и белого списка (+). Введённые условия поиска отображаются в виде списка через запятую, например:
"""), 'body', ("""
	+Human,-Human_1_1
	+hasAgent
"""), 'example', _("""
означает требование найти триплеты, которые содержат отношение """), 'body', ("""hasAgent"""), 'inlinex', _(""" и любые узлы класса """), 'body', ("""Human"""), 'inlinex', _(""" кроме """), 'body', ("""Human_1_1"""), 'inlinex', _(""". Если в вашем графе используются суффиксы с номерами индивидов, то по запросу имени класса без такого суффикса будут найдены как сам класс, так и все входящие в него индивиды.
"""), 'body', _("""

Поиск триплетов
"""), 'header', _("""
Фильтр триплетов позволяет шаблоны триплетов и находить соответствующие им триплеты. Шаблоны могут включать в себя пустые поля (отображаются как *) и неполные обозначения сущностей. Пустому полю соответствует любой узел или отношение. Например:
"""), 'body', ("""
	+Dancing hasAgent *,+Talking * Human_1_1
"""), 'example', _("""
Фильтр триплетов также имеет режимы черного списка (-) и белого списка (+). При вводе нескольких шаблонов триплетов вы можете использовать переменные, чтобы потребовать идентичности узлов в находимых по разным шаблонам триплетах. Переменные могут замещать субъект, предикат или объект триплета, а также суффикс индивида. Например,
"""), 'body', ("""
	+EpistModality hasDegree *,+EpistModality hasObject Impelling_0_1
"""), 'example', _("""
означает задание найти все триплеты """), 'body', _("""EpistModality (любой индивид) -hasDegree→ (любое значение)"""), 'inlinex', _(""" и все триплеты """), 'body', _("""EpistModality (любой индивид) -hasObject→ Impelling_0_1"""), 'inlinex', _(""". В результате в тестовом файле будет найдено 111 триплетов. Запрос
"""), 'body', ("""
	+EpistModality_?z hasDegree *,+EpistModality_?z hasObject Impelling
"""), 'example', _("""
дополнительно накладывает требование, чтобы находимые триплеты содержали один и тот же индивид EpistModality, причем его номер заранее неизвестен. Это будет преобразовано в эквивалентный запрос
"""), 'body', ("""
	+EpistModality_1_12 hasDegree *,+EpistModality_1_12 hasObject Impelling
"""), 'example', _("""
В результате в тестовом файле будет найдено 2 триплета. Поиск с переменными реализован через генерацию запроса SPARQL 'SELECT' и требует, чтобы в графе имелись связи """), 'body', ("""rdf:type"""), 'inlinex', _(""" между индивидами и узлом обобщающего их класса. Запрос
"""), 'body', ("""
	+?z hasDegree *,+?z hasObject Impelling
"""), 'example', _("""
отличается отсутствием ограничения на класс субъекта и в тестовом файле обнаружит уже 6 триплетов (два """), 'body', ("""EvalModality"""), 'inlinex', _(""" в дополнение к """), 'body', ("""EpistModality_1_12"""), 'inlinex', _("""). Поиск триплетов по шаблону содержащему только отношение отличается от поиска отношений с помощью фильтра сущностей тем, что условия фильтра сущностей дополняют друг друга (логическое И), а поиск триплетов с отношением связан с поиском сущностей через логическое ИЛИ.
"""), 'body', _("""

Поиск цепочек
"""), 'header', _("""
Фильтр поиска цепочек позволяет найти в графе все узлы, которые соединяют два указанных индивида. Можно задать несколько пар индивидов. После нахождения кратчайшего пути алгоритм последовательно исключает из него узлы и ищет альтернативные пути, которые их минуют. Так находятся до 5 разных вариантов состоящих только из узлов цепочек для каждой из заданных пар. Например, запрос:
"""), 'body', ("""
	Masha Vasya,Masha Petya
"""), 'example', _("""
позволит узнать, что связывает Машу с Васей и Петей. При поиске цепочек игнорируются отношения """), 'body', ("""rdf:type"""), 'inlinex', _(""", обычно соединяющие индивиды с обозначениями соответствующих им абстрактных классов. Такие связи учитываются только при отсутствии другой альтернативы. Результат поиска будет включать в себя все отношения между входящими в найденные цепочки узлами, поэтому число найденных путей между парами индивидов может быть гораздо больше чем 5.

Если не задать никаких условий поиска, то на выходе будет копия исходного графа. Это можно использовать для конверсии исходного графа в другой текстовый формат представления RDF, например rdf-xml в turtle или эталог.
"""), 'body', _("""

Порядок применения фильтров:
"""), 'header', _("""
	1. фильтр поиска цепочек
	2. фильтр сущностей (черный список, белый список)
	3. фильтр триплетов (черный список, белый список)
"""), 'body', _("""

Цвета узлов при визуализации:
"""), 'header', _("""
	красный - узлы из белого списка сущностей
	желтый - узлы найденные поиском цепочек
	зеленый - триплеты из белого списка поиска триплетов
					(теплый оттенок - субъекты, холодный - объекты)
	синий - узлы из черного списка повторно найденные другим фильтром
	белый - сопутствующие узлы, которые не являются предметом поиска.

Узлы обозначающие абстрактные классы, а не индивиды, имеют более темную окраску.
"""), 'body', _("""

Горячие сочетания клавиш:
"""), 'header', _("""
	Ctrl+F, Ctrl+S - поиск, аналогично кнопке с лупой
	Ctrl+G - показать изображение графа результата
	Ctrl+R - заново сериализовать исходный граф.
"""), 'body', _("""

Опции командной строки:
"""), 'header', _("""
	-h - краткая справка
	-i  <файл> - прочесть исходный RDF из файла (ASCII, UTF-8 или UTF-16)
	-o <файл> - сохранить результат (последнего) поиска в файл при выходе
"""), 'body', _("""

Программа доступна на условиях лицензии GPLv3 или новее.
Разработка - Вячеслав Диконов: sdiconov@mail.ru
Поддержка формата RDF Эталог - Иван Рыгаев: irygaev@jent.ru
"""), 'sign')
        self.helpbox.tag_add('body', '2.0', tk.END)
        self.helpbox.tag_config('title', justify=tk.CENTER, font=('Times', 24, 'bold'))
        self.helpbox.tag_config('header', font=('Times', 14, 'bold'))
        self.helpbox.tag_config('body', font=('Times', 12))
        self.helpbox.tag_config('inlinex', font=('Times', 12, 'italic'))
        self.helpbox.tag_config('example', font=('Courier', 12))
        self.helpbox.tag_config('sign', font=('Times', 12, 'italic'))


######################################

def filter_rdf(settings, rtparam, context, srcg, outg, filterq): # Задать условия и выполнить поиск. Альт. точка входа
    if srcg is None: srcg = init_graphdata() # хранилище rdflib для исходного графа
    if outg is None: outg = init_graphdata() # хранилище rdflib для результата
    else: clear_graph(outg, srcg)
    fill_missing_settings(settings, rtparam) # Если нет каких-то параметров - подставим умолчания

    if 'rdfilter_guimode' in rtparam and rtparam['rdfilter_guimode']: # Есть запрос на рисование окна при вызове процедуры поиска.
        global fltwin # После входа из другого скрипта важно, чтобы переменная не терялась при повторном вызове compare
        try: fltwin.winfo_children()  # winfo_exists недостаточно
        except: fltwin = tk.Toplevel() # После входа в compare из другого скрипта cmpwin надо создать
        filter_gui = FilterGUI(fltwin, srcg, outg, filterq, settings, rtparam, context)
    else: message('err', _("Для задания критериев поиска нужен графический режим"))

def cmdline(argv, settings): # Разбор командной строки
    inputfile = ''
    outputfile = ''

    try:
        opts, args = getopt.getopt(argv,"hi:o:f:",["ifile=","ofile=","format="])
    except getopt.GetoptError:
        print (_('rdfilter.py -i <файл ввода> -o <файл вывода>'))
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            print (_('rdfilter.py -i <файл ввода> -o <файл вывода>'))
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
            print (_('Файл ввода: '), inputfile)
        elif opt in ("-o", "--ofile"):
            outputfile = arg
            print (_('Файл вывода: '), outputfile)
        elif opt in ("-f", "--format"):
            settings.update({'format': arg})
            print (_('Формат RDF: '), settings['format'])
    return inputfile, outputfile

if __name__ == "__main__": # Запуск в качестве автономного приложения
    srcg = init_graphdata(); outg = init_graphdata()
    settings = {}; rtparam = {}
    init_settings(settings, rtparam)
    inputfile, outputfile = cmdline(sys.argv[1:], settings)
    if (inputfile):
        srcg['txt'] = read_rdffile(inputfile)
        parsed = parserdf(srcg, settings['format'], rtparam['sems'], rtparam['username']) # Прочитать RDF из файла
        if parsed: srcg['src'].update({'file': inputfile})
    filter_rdf(settings, rtparam, {}, srcg, outg, None)
    if fltwin and autonomous: fltwin.mainloop() # Только при самостоятельном запуске. После вызова из другого скрипта используется mainloop того скрипта.
    if (outputfile): save_rdffile(outputfile, serializerdf(settings['format'], outg)) # После поиска в момент выхода сохранить результат в файл
