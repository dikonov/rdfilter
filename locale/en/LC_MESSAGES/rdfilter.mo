��    �      �  �   �	      �    �  �   �  �   �  �  �  z  �  &   &  2   M  "   �  .   �      �      �       8   1    j  "   v  9   �  �  �  #  z  �  �  �  f  �  #!  ^  �"  ]  G%  D   �&  w   �&  �   b'  �   !(  ,   �(     �(     )     )     .)     J)     Y)     v)  %   �)     �)  f   �)     -*     C*  �  Z*     J,  �  [,  x   H0  @  �0  U   2  E   X2     �2      �2  -   �2     3     3      3  �   53  ?   �3  ,   4  E   @4  P   �4  Q   �4  ?   )5  $   i5     �5  
   �5  �   �5  b   y6  h   �6  z   E7  �   �7  8   L8  "   �8     �8     �8     �8  
   �8      �8  �   9  T   �9  X   �9  ,  T:  �   �;  �   <  �   �<  y   �=     �=     >  �   ->  �   �>  "   z?  ;   �?     �?     �?     �?     @     "@     <@     X@     t@     �@  x   �@  o   A  n   A  $   �A  �   B  U   �B     �B     C  >   C  4   [C  
   �C     �C  
   �C     �C  7   �C  =   D  C   UD     �D     �D  .   �D  .   �D     (E     7E  #   WE  '   {E  *   �E  0   �E     �E     F  .   %F     TF     gF     zF  '   �F  #   �F     �F  0    G  -   1G  -   _G     �G     �G     �G     �G     �G     �G     �G     H     4H  	   TH     ^H     kH  
   ~H     �H     �H    �H  �   �I  g   dJ  �   �J  �  ~K  �   M     �M     �M     �M     �M     N     N     /N     =N  �   ZN     �N     O  U  &O  �   |Q    MR  �  eS  �   U  (  �U  �   W  =   �W  3   �W  I   &X  n   pX     �X     �X     Y     Y     Y     'Y     0Y     KY      cY     �Y  \   �Y  
   �Y  
   �Y  i   Z     j[    s[  F   x]  �   �]  5   �^  :   �^      _     _      _     :_     J_     X_  g   f_  $   �_     �_  ,   
`  L   7`  3   �`  !   �`     �`     �`     �`  �   �`  /   za  1   �a  D   �a  v   !b     �b     �b     �b  	   �b     �b     �b     �b  O   �b  0   Jc  3   {c  �   �c  >   Dd  Y   �d  h   �d  9   Fe     �e     �e  R   �e  L   �e     9f     Mf     Yf     ef     sf     �f     �f     �f     �f     �f     �f  B   �f  1   �f  >   .g     mg  U   �g  "   �g     �g     h     h     /h     Ih     Oh     ]h     bh     ph     �h     �h     �h     �h     �h     �h     �h     �h     i     (i     =i     Vi     li     si     |i  	   �i  
   �i  
   �i     �i     �i     �i     �i     �i     j     )j     .j     7j     @j     Mj     [j     mj     {j     �j     �j     �j     �j     �j     �j     �j     Z       U   h   I       ~   T   v         R       2   A       V   �   �       �   >   w   +   s   J               [   3   �              <   :   �       &   *       l              �       `   a               
   F       �   p         D   c       7   x       C       "   �   %          =   �       z   d                 i   �   �           #               |   \   �   5   �   (           -       /       N      9   t   G       O   B       r   u   b   M   j              g           �       m   �   o   $      Y                  S   k      @   y       !   }   �   f              E   ,   1           _       ?           W   �   q      L      4   �       n          )   P              {   �   �   ^         	       H   '   �          K      ]       �   8   .   �      ;   Q   e       �   �   X   �          0   6    
	-h - краткая справка
	-i  <файл> - прочесть исходный RDF из файла (ASCII, UTF-8 или UTF-16)
	-o <файл> - сохранить результат (последнего) поиска в файл при выходе
 
	1. фильтр поиска цепочек
	2. фильтр сущностей (черный список, белый список)
	3. фильтр триплетов (черный список, белый список)
 
	Ctrl+F, Ctrl+S - поиск, аналогично кнопке с лупой
	Ctrl+G - показать изображение графа результата
	Ctrl+R - заново сериализовать исходный граф.
 
	красный - узлы из белого списка сущностей
	желтый - узлы найденные поиском цепочек
	зеленый - триплеты из белого списка поиска триплетов
					(теплый оттенок - субъекты, холодный - объекты)
	синий - узлы из черного списка повторно найденные другим фильтром
	белый - сопутствующие узлы, которые не являются предметом поиска.

Узлы обозначающие абстрактные классы, а не индивиды, имеют более темную окраску.
 


    С помощью этого инструмента можно находить интересующие вас утверждения в графе RDF. Граф можно загрузить в текстовом виде из командной строки, с помощью меню "Файл/Открыть" или вставить из буфера обмена.
 

Ввод и вывод графов
 

Горячие сочетания клавиш:
 

Интерфейс поиска
 

Опции командной строки:
 

Поиск сущностей
 

Поиск триплетов
 

Поиск цепочек
 

Порядок применения фильтров:
 

Программа доступна на условиях лицензии GPLv3 или новее.
Разработка - Вячеслав Диконов: sdiconov@mail.ru
Поддержка формата RDF Эталог - Иван Рыгаев: irygaev@jent.ru
 

Форматы графов RDF
 

Цвета узлов при визуализации:
 
    Окно программы состоит из двух частей: зоны задания фильтров и зоны просмотра RDF внизу. Справа от зоны задания фильтров находятся кнопки открытия файла (папка), поиска (лупа) и графического просмотра (глаз). В левом текстовом поле находится исходный граф. В правом - результаты поиска в нем. Текстовые поля имеют заголовки, где можно видеть источник графа, число триплетов в нем, а также кнопки вызова программы сравнения. При их нажатии соответствующий граф будет передан на сравнение с другим графом.

    Есть три типа фильтров: фильтры сущностей, фильтр триплетов и фильтр поиска цепочек узлов. Одновременно можно задать много фильтров разных типов.
 
В результате в тестовом файле будет найдено 2 триплета. Поиск с переменными реализован через генерацию запроса SPARQL 'SELECT' и требует, чтобы в графе имелись связи  
Позволяет находить триплеты (не) содержащие заданные классы и индивиды в любой позиции и/или отношения. Фильтр сущностей имеет режимы черного списка (-) и белого списка (+). Введённые условия поиска отображаются в виде списка через запятую, например:
 
Фильтр поиска цепочек позволяет найти в графе все узлы, которые соединяют два указанных индивида. Можно задать несколько пар индивидов. После нахождения кратчайшего пути алгоритм последовательно исключает из него узлы и ищет альтернативные пути, которые их минуют. Так находятся до 5 разных вариантов состоящих только из узлов цепочек для каждой из заданных пар. Например, запрос:
 
Фильтр триплетов позволяет шаблоны триплетов и находить соответствующие им триплеты. Шаблоны могут включать в себя пустые поля (отображаются как *) и неполные обозначения сущностей. Пустому полю соответствует любой узел или отношение. Например:
 
Фильтр триплетов также имеет режимы черного списка (-) и белого списка (+). При вводе нескольких шаблонов триплетов вы можете использовать переменные, чтобы потребовать идентичности узлов в находимых по разным шаблонам триплетах. Переменные могут замещать субъект, предикат или объект триплета, а также суффикс индивида. Например,
 
дополнительно накладывает требование, чтобы находимые триплеты содержали один и тот же индивид EpistModality, причем его номер заранее неизвестен. Это будет преобразовано в эквивалентный запрос
 
означает задание найти все триплеты  
означает требование найти триплеты, которые содержат отношение  
отличается отсутствием ограничения на класс субъекта и в тестовом файле обнаружит уже 6 триплетов (два  
позволит узнать, что связывает Машу с Васей и Петей. При поиске цепочек игнорируются отношения   (%s индивидов / %s классов)  До:   От:   Поиск триплетов   Поиск цепочек   Фильтр   в дополнение к   и все триплеты   и любые узлы класса   кроме   между индивидами и узлом обобщающего их класса. Запрос
 %s триплетов %s триплетов  ). Поиск триплетов по шаблону содержащему только отношение отличается от поиска отношений с помощью фильтра сущностей тем, что условия фильтра сущностей дополняют друг друга (логическое И), а поиск триплетов с отношением связан с поиском сущностей через логическое ИЛИ.
 , %s узлов  , обычно соединяющие индивиды с обозначениями соответствующих им абстрактных классов. Такие связи учитываются только при отсутствии другой альтернативы. Результат поиска будет включать в себя все отношения между входящими в найденные цепочки узлами, поэтому число найденных путей между парами индивидов может быть гораздо больше чем 5.

Если не задать никаких условий поиска, то на выходе будет копия исходного графа. Это можно использовать для конверсии исходного графа в другой текстовый формат представления RDF, например rdf-xml в turtle или эталог.
 . В результате в тестовом файле будет найдено 111 триплетов. Запрос
 . Если в вашем графе используются суффиксы с номерами индивидов, то по запросу имени класса без такого суффикса будут найдены как сам класс, так и все входящие в него индивиды.
 EpistModality (любой индивид) -hasDegree→ (любое значение) EpistModality (любой индивид) -hasObject→ Impelling_0_1 RDF фильтр 1.9 RDF-граф для поиска RDF-граф для сравнения № %s RDFCompare (%s) RDFilter (%s) SPARQL запрос: rdfcomp.py -a <файл 1> -b <файл 2> -c <файл совпадений> -d <файл разницы 1> -e <файл разницы 2> -f <формат> rdfilter.py -i <файл ввода> -o <файл вывода> Базовая и промежуточная В графе не выделена базовая структура В графе не выделена запрошенная структура %s В графе не выделена промежуточная структура В пустом результате нечего искать. Вставлено из буфера Всё Выход Граф не содержит фрагмента соответствующего заданным критериям поиска триплетов.
Получен пустой ответ на SPARQL запрос. Для задания критериев поиска нужен графический режим Для поддержки перевода интерфейса необходим модуль Gettext. Для работы программы в интерактивном режиме необходим модуль Tkinter. Для работы программы необходим модуль rdflib для Python
Установка:
pip (--user) install rdflib Игнорировать номера индивидов Индивидов в группе Крупный Любые файлы Мелкий Найти Нарушен формат RDF
 Не загружен исходный граф. Откройте файл RDF или вставьте текст RDF из буфера в левое поле. Не найден файл программы поиска в графе rdfilter.py Не найден файл программы сравнения графов rdfcomp.py Не удается определить текущее имя пользователя. Возможны проблемы из-за конфликтов имен временных файлов, если несколько человек запустят программу одновременно. Не удалось преобразовать шаблон триплета
%s
SPARQL запрос нельзя сформировать. Не удалось сформировать корректный запрос SPARQL.
Для переменной %s (%s) не удается найти URI.
 Не удалось сформировать корректный запрос SPARQL.
Переменной %s может быть приписано более одного URI.
%s может означать:  Недостаточно параметров и нет поддержки графического интерфейса. Нет данных Нечего рисовать. Нечего сохранять. Перед записью в файл следует задать критерии поиска и получить результат. Нечего сохранять. Перед записью в файл следует получить результат сравнения двух графов. Нечего сравнивать. Общая часть сравниваемых графов Описание Ответ %s: Открыть Открыть файл 1 Открыть файл 2 Отличия графа 1 Отличия графа 2 Отношения:  Ошибка Ошибка разбора формата Эталог. Неверная ссылка на скобочный блок. Ошибка разбора формата Эталог. Член триплета - пустая строка. Переменная %s не может относиться к двум разным классам %s и %s. Поиск в результатах Поиск отменён из-за ошибки.
Исправьте, пожалуйста, шаблоны поиска триплетов. Поиск по Семантическим структурам в формате RDF Показать Получено от Попытка сравнения с пустым графом Промежуточная и расширенная Пусто Пустой блок %s Режим Результат поиска Совпадающая часть двух графов Сохранение результата поиска в RDF Сохранение результата сравнения в RDF Сохранить Сохранить общее Сохранить отличия файла 1 Сохранить отличия файла 2 Справка Сравнение RDF 1.3 beta Сравнение графов RDF Сравним. Число троек:  Сравнить исходный граф Сравнить результат поиска Средний Стандартный Стереть номера индивидов Структура Сущности:  Только базовая Только промежуточная Только расширенная Уникальные узлы Уникальные узлы  (Всего - %s) Уникальный узел в графе 1 Уникальный узел в графе 2 Файл Файл 1:  Файл 2:  Файл ввода:  Файл вывода:  Файл не найден
%s Файл разницы 1:  Файл разницы 2:  Файл совпадений:  Файл: Формат Формат RDF:  Шрифт исходный граф результат поиска Project-Id-Version: rdfilter
PO-Revision-Date: 2020-05-27 15:00+0300
Last-Translator: Viacheslav <sdiconov@mail.ru
Language-Team: en <en@US.org>
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Generated-By: Lingua 4.13
 
	-h - brief help
	-i <file> - Read the source RDF from file (ASCII, UTF-8 or UTF-16)
	-o <file> - Save (last) search result to file at exit
 
	1. Path search
	2. Entity search (black list, white list)
	3. Triple search (black list, white list)
 
	Ctrl+F, Ctrl+S - Search, same as the magnifying glass button
	Ctrl+G - Show a picture of the resulting graph
	Ctrl+R - Re-serialize the source RDF (refresh after wrong edit).
 
	red - nodes from the entity filter whilelist
	yellow - nodes discovered by the chain filter
	green - triples from the triplet filter whitelist
					(warmer tint - subjects, cooler tint - obects)
	blue - nodes from a blacklist re-discovered by a different filter
	white - miscellaneous parts of the found triples.

Nodes representing abstract classes (without an instance suffix) have a darker color.
 


    This tool helps to find logical statements in an RDF graph. You can load the graph as RDF text from command line, using the "File/Open" menu, or paste it from clipboard.
 

RDF input and output
 

Hotkeys:
 

Search GUI
 

Command line options:
 

Entity filter
 

Find triples
 

Find paths
 

Filter application order:
 

This software is distributed under the terms of GPLv3 or later.
Developer - Viacheslav Dikonov: sdiconov@mail.ru
Etalog format - Ivan Rygaev: irygaev@jent.ru
 

RDF graph formats
 

Graph node colors:
 
    The main application window has two parts: search criteria editor and RDF viewing boxes below. To the right of the search criteria editing controls are buttons that open a file (folder icon), run search (looking glass) and visualisation (eye icon). The left text field contains the full source graph. The right text box shows the search result. Their headers display graph origin number of triplets, and buttons that call the graph comparison tool.

    There are three type of filters: entity filter, triplet filter and chain filter. You can use several filters of different types together.
 
which finds 2 triples in the test file. The variables are resolved by means of an automatically generated SPARQL 'SELECT' query. In order to apply class constraints this method requires the qraph to include  
Finds RDF triples that contain the specified individual or class references in either subject or object position and/or predicates. The filter has blacklist (-) and whitelist (+) modes. The entered search terms are displayed as a comma-separated list. For example the strings: 
 
The chain filter allows to find all nodes in the source graph that form paths between two specified individual nodes. You can specify multiple pairs of nodes (entities). The algorithm finds the shortest path and than looks for alternative routes by excluding intermediate nodes one by one and finding "detours" around them. Up to 5 different sets of intermediate nodes will be found for each of the given pairs. For example: 
 
Finds triples matching the specified patterns. The patterns may include empty fields (shown as *) and partial URIs. An empty field matches any RDF node or relation in the corresponding position. For example:
 
The triplet filter also has blacklist (-) and whitelist (+) modes.When you enter multiple patterns, you can use variables to tell that the entities found in different triples must be identical. Variables can be used in place of the subject, predicate, object or instance suffixes. For example, 
 
imposes an additional requirement that the matching triplets must contain the same instance of EpistModality with an unknown suffix. This query will be converted to,
 
 is an instruction to find all triples matching the pattern  
are an instruction to find triples with predicate  
does not constrain the class of the subject and produces 6 triples (two  
will explain what connects Masha with Vasya and Petya. The search for the shortest path ignores the relations  (%s individuals /%s classes)  To:   From:   Find triples   Find paths   Filter   nodes in addition to the   and all triples where   and any instances of the class   except   links between individual instances and references to corresponding classes. The next query
 %s triples %s triples ).

Searching for triples with a pattern that specifies only the predicate is different from using the entity filter to find the same triples. All entity filter constraints are applied to each triple together (logical AND), while the equivalent triplet filter query is applied independently (logical OR) and its output augments the result of the entity filter.
 %s nodes , that connect individuals with corresponding class references. Such relations are taken into account only if there are no other alternatives. The chain search result will include all arcs between nodes forming the paths found, so the final number of routes between the start and the end points can be much greater than 5. 

If you do not specify any search terms, the output will be a copy of the source graph. This feature can be used to convert RDF to a different format, for example rdf-xml to turtle or etalog.
 . It finds 111 triples in the test file. The following modified query
 . If your graph makes use of numeric suffixes for individual instances, all class names without a suffix will be treated as a wildcard to find both the class reference itself and all instances of the class.
 EpistModality (any instance) -hasDegree→ (anything) EpistModality (any individual) -hasObject→ Impelling_0_1 RDF Filter 1.9 RDF to search in RDF-graph to compare # %s RDFCompare (%s) RDFilter (%s) SPARQL query: rdfcomp.py -a <file 1> -b <file 2> -c <common part file> -d <diff file 1> -e <difff file 2> -f <format> rdfilter.py -i <infile> -o <outfile> Basic and Intermediate The graph has no marked Basic Structure area The graph has no marked area corresponding to the %s structure you requested The graph has no marked Intermediate Structure area Nothing to find in a null result. Pasted from clipboard All Exit The source graph does not contain a set of triples matching the specified set of templates.
SPARQL query yields an empty answer. Setting search criteria is possible in GUI mode Gettext Python module is needed for localization. Tkinter python module is needed to run this program in the GUI mode. This program requires rdflib Python module to run.
Use the following command to istall it:
pip (--user) install rdflib Ignore individual numbers Individuals in group Large All files Small Search Incorrect RDF format
 No source graph loaded.
Open an RDF file or paste RDF text into the left field. Unable to find the graph search tool rdfilter.py Unable to find the graph comparison tool rdfcomp.py Unable to determine the current user name. It may cause problems due to filename collisions, if several users run this application at the same time. Unable to convert triple template
%s
Cannot form SPARQL query. Unable to form correct SPARQL query.
Cannot determine full URI for the variable %s (%s).
 Unable to form correct SPARQL query.
Cannot ascribe multiple URIs to variable %s.
%s may correspond to:  Not enough parameters given and no GUI support available. No data Nothing to draw. Nothing to save. Please, specify some search criteria and obtain a result to save. Nothing to save. Please, compare two graphs and obtain a difference to save. Nothing to compare. Common part Description Solution %s:  Open RDF Text Open file 1 Open file 2 Diff 1 Diff 2 Relations:  Error Error while parsing Etalog. Invalid reference to a bracketed bloc. Error while parsing Etalog. Empty triplet member. Variable %s may not belong to two different classes %s and %s. Search in the result Search aborted, because an error occured.
Please, change the triple search templates. RDF Semantic Structure Search Tool Display Received from Comparison with an emty graph Intermediate and Extended Empty Empty bloc %s Mode Search result Common part of the two graphs Saving search result to RDF Saving comparison result to RDF Save Save common part Save diff 1 Save diff 2 Help RDF Comparison 1.3 beta Compare RDF graphs Number of triplets:  Compare the source graph Compare search result Medium Standard Erase individual numbers Structure Entities:  Basic only Intermediate only Extended only Unique nodes Unique nodes  (%s total) Unique node in graph 1 Unique node in graph 2 File File 1:  File 2:  Input file:  Output file:  File not found
%s Diff file 1:  Diff file 2: Common part file:  File Format RDF Format: Font source graph search result 