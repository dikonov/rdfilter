# RDFilter

Простые инструменты для 1) фильтрации RDF и поиска нужных триплетов в RDF-графах 2) сравнения RDF-графов. Результаты поиска и сравнения можно преобразовать в картинку с помощью graphviz.
Это мои первые скрипты на Питоне.

Зависимости: python3, rdflib, graphviz, gettext (необязательно)

Simple GUI tools to 1) filter RDF and search for specific triplets in an RDF graph 2) compare RDF-graphs. Search and comparison results can be visualized using graphviz.
These are my first Python scripts.

Requirements: python3, rdflib, graphviz, gettext (optional)

* The apps' native UI language is Russian. The English localization is available in the 'en' locales and requires GNU gettext to work. Translations to other languages are welcome.
** The localization may be partial due to changes in the software. This will be fixed as soon as the UI becomes stable again.
