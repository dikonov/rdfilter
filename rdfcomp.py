#!/usr/bin/python3
# -*- coding: utf-8 -*-
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import sys, getopt, re, locale, collections
from collections import deque
from sys import modules

# Определение автономного запуска
if "rdflib" in modules: autonomous = False # Если rdflib уже импртирован, то вход был из другого скрипта
else: autonomous = True # Инициализация настройки. При вызове из другого модуля передается копия settings оттуда

try: # Локализация
    import gettext
    syslanguage, syscodepage = locale.getdefaultlocale(['LC_ALL', 'LANG'])
    if not syslanguage: syslanguage = 'en' # Язык по умолчанию, если системная переменная не установлена
    syslanguage = re.sub('_.*', '', syslanguage)
    try:
        cmptrans = gettext.translation('rdfcomp'); cmptrans.install()
    except FileNotFoundError:
        try: # Наверно это Windows
            cmptrans = gettext.translation('rdfcomp', localedir='locale', languages=[syslanguage]); cmptrans.install()
        except FileNotFoundError:
            if not syslanguage == 'ru': print ("Gettext localization file locale/%s/LC_MESSAGES/rdfilter.mo not available." % syslanguage)
            def _(i): return i
except ModuleNotFoundError:
    def _(i): return i # Заглушка для функции локализации при отсутствии gettext
    print (_("Для поддержки перевода интерфейса необходим модуль Gettext."))

try:
    import tkinter as tk
    import tkinter.ttk as ttk
    import tkinter.font as tkFont
    from tkinter import *
    #from tkinter import ttk
    from tkinter import scrolledtext
    from tkinter import messagebox
    from tkinter import filedialog
    if autonomous:
        cmpwin = tk.Tk()
        cmpwin.withdraw()
except ModuleNotFoundError:
    cmpwin = None
    print (_("Для работы программы в интерактивном режиме необходим модуль Tkinter."))

try:
    import rdflib
    from rdflib import URIRef
    from rdflib.term import Literal
    from rdflib.namespace import XSD
except ModuleNotFoundError:
    print(_("Для работы программы необходим модуль rdflib для Python\nУстановка:\npip (--user) install rdflib"))
    exit()

try:
    import graphviz
    from graphviz import *
except ModuleNotFoundError:
    pass

try:
    import json
except ModuleNotFoundError:
    pass

try:
    import os
except ModuleNotFoundError:
    pass
#from appdirs import * # https://pypi.org/project/appdirs/1.2.0/

try: # Программа поиска в графе
    import rdfilter
except ModuleNotFoundError:
    print (_("Не найден файл программы поиска в графе rdfilter.py"))

#settings # Сохраняемые в файл параметры. Заполняется в init_settings
#rtparam # Определяемые при запуске параметры
#context # Набор элементов интерфейса

#########################
def init_settings(settings, rtparam): # Создать полный набор параметров по умолчанию
    settings.clear()
    settings.update({'formats': ['turtle', 'nt', 'xml', 'pretty-xml', 'etalog']}) # Список поддерживаемых форматов RDF
    settings.update({'format': 'turtle'}) # Формат RDF по умолчанию для rdflib (turtle, n3...)
    settings.update({'fontscale': 1}) # Масштаб ширифта

    rtparam.clear()
    if "os" in modules:
        rtparam.update({'username': os.getlogin()}) # При запуске из другого модуля имя д.б. уже в переданных settings
        rtparam.update({'cfgfile': "semtools_settings_%s.json" % rtparam['username']}) # При запуске из другого модуля имя д.б. уже в переданных settings
    else:
        message('err', _("Не удается определить текущее имя пользователя. Возможны проблемы из-за конфликтов имен временных файлов, если несколько человек запустят программу одновременно."))
        rtparam.update({'username': ''}) # Имя пользователя в системе для имен врем. файлов настроек. Определяется позже.
        rtparam.update({'cfgfile': "semtools_settings.json"}) # Имя файла настроек. Определяется позже.

    if "graphviz" in modules: rtparam.update({'have_graphviz': True})
    else: rtparam.update({'have_graphviz': False})

    if "etalog" in modules: rtparam.update({'have_etalog': True})
    else: rtparam.update({'have_etalog': False})

    rtparam.update({'have_rdfcomp': True})

    if "rdfilter" in modules: rtparam.update({'have_rdfilter': True})
    else: rtparam.update({'have_rdfilter': False})

    if "tkinter" in modules:
        rtparam.update({'rdfcomp_guimode': 'True'})
        rtparam.update({'rdfilter_guimode': 'True'})
    else:
        rtparam.update({'rdfcomp_guimode': 'False'})
        rtparam.update({'rdfilter_guimode': 'False'})
    rtparam.update({'rdfcomp_cmpmode': 'standard'})  # (standard, eraseind, ignoreind) Режим сравнения по умолчанию
    #rtparam.update({'syslanguage': 'en'}) # Язык по умолчанию. Определяется позже.
    #rtparam.update({'syscodepage': 'UTF-8'}) # Кодировка по умолчанию. Определяется позже.
    rtparam.update({'sems': 'all'}) # all/bsems/isems/esems Какую часть графа от ЭТАП4 использовать Basic и Intermediate может не быть совсем

def fill_missing_settings(settings, rtparam): # Восполнить отсутствующие параметры умолчаниями
    defsettings = {}; defrtparam = {}
    init_settings(defsettings, defrtparam)
    for p in defsettings:
        if not p in settings: settings.update({p: defsettings[p]})
    for p in defrtparam:
        if not p in rtparam: rtparam.update({p: defrtparam[p]})

def save_settings(settings, rtparam): # Записать конфигурацию в файл
    with open(rtparam['cfgfile'], 'w') as configfile:
        json.dump(settings, configfile, ensure_ascii=False, indent=4, sort_keys=True)

def read_settings(settings, rtparam): # Прочесть конфигурацию из файла
    with open(rtparam['cfgfile']) as configfile:
        settings = json.load(configfile)

#########################

def message(type, msg): # Показать сообщение
    print (msg)
    if cmpwin:
        if type == 'err': mb = messagebox.showerror(title=_("Ошибка"), message=msg)
        if type == 'inf': mb = messagebox.showinfo(title=None, message=msg)

def append_str(a, b, delim):
    if b:
        if a: return a+delim+b
        else: return b
    else: return a

def unquot(s):
    l = len(s)
    if l > 0:
        if s[l-1] == '"': s = s[:l-1]
        if s[0] == '"': s = s[1:]
    return s

def deep_update(source, overrides): # Дополнение многоуровневого словаря или подобной структуры. Источник модифицируется "по месту"
    for key, value in overrides.items():
        if isinstance(value, collections.abc.Mapping) and value:
            returned = deep_update(source.get(key, {}), value)
            source[key] = returned
        else:
            source[key] = overrides[key]
    return source

def RDFobj(nd): # Преобразование строки с полным именем RDF сущности в объект URIRef или XSD литерал для хранилища троек
    if re.search('#', nd): n = URIRef(nd)
    elif re.search('\^\^xsd\:', nd): # Есть xsd тип данных
        m = re.match('^(.*)\^\^xsd\:(.*)$', nd)
        n = Literal(unquot(m.group(1)), datatype='XSD.' + m.group(2))
    elif re.match('^[0-9]+$', nd): n = Literal(unquot(nd), datatype=XSD.integer) # Аварийный выход - нет ни префикса, ни xsd типа
    elif re.match('^[0-9\,\.]+$', nd): n = Literal(unquot(nd), datatype=XSD.float)
    else: n = Literal(unquot(nd), datatype=XSD.string)
    return n

def parseURI(uri): # Нарезка на префикс, имя класса и номер индивида
    ns = ""; pfx = ""; e = ""; i = ""
    uri = str(uri) # Объекты xsd String в текст

    p = re.match("(.*?)#(.*)", uri)
    if p:
        ns = p.group(1)
        uri = p.group(2)
    else:
        p = re.match("(.*?):(.*)", uri)
        if p:
            pfx = p.group(1)
            uri = p.group(2)

    p = re.match("(.*)_(\d+_\d+)$", uri)
    if not p: p = re.match("(.*)_(\d+)$", uri)
    if not p: p = re.match("(.*)_(\?[A-Z\d]*)$", uri, re.IGNORECASE)
    if p:
        e = p.group(1)
        i = p.group(2)
    else: e = uri
    return ns, pfx, e, i

def getprefix (g, q): # Получение префикса по namespace URI
    for pfx, ns in g['grf'].namespaces():
        ns  = re.sub('#$', '', ns)
        if q == ns: return pfx
    return ''

def makerdfdict(g): # Создание словаря сущностей полученного графа и сбор статистики
    cnodes = {}

    g['edict'].clear()
    for s,p,o in g['grf']:  # Перебор всех триплетов для сборки словаря сущностей  - глобальная переменная
        s = str(s); p = str(p); o = str(o) # Преобразование объектов RDF в строки для сравнения с вводимыми пользователем строками

        ns, pfx, e, i = parseURI(s)  # Здесь pfx всегда пустй
        deep_update(g['edict'], {'ent': {s: {'ns': ns, 'e': e, 'i': i }}}) # компоненты полного уникального имени инд.сущности и ее роль в триплете
        deep_update(g['edict'], {'cls': {e: {'ns': {ns: None}, 'i': {i: None}, 'ent': {s: None}}}})
        deep_update(g['edict'], {'ind': {i: {'e': {e: None}, 'ent': {s: None}}}})
        deep_update(g['edict'], {'iref': {append_str(e, i, '_'): {'ent': {s: None}}}})
        ns, pfx, e, i = parseURI(o)
        deep_update(g['edict'], {'ent': {o: {'ns': ns, 'e': e, 'i': i }}})
        deep_update(g['edict'], {'cls': {e: {'ns': {ns: None}, 'i': {i: None}, 'ent': {o: None}}}})
        deep_update(g['edict'], {'ind': {i: {'e': {e: None}, 'ent': {o: None}}}})
        deep_update(g['edict'], {'iref': {append_str(e, i, '_'): {'ent': {s: None}}}})
        ns, pfx, e, i = parseURI(p)
        deep_update(g['edict'], {'rls': {p: {'ns': ns, 'e': e, 'i': i }}})
        deep_update(g['edict'], {'rel': {e: {'ns': {ns: None}, 'rls': {p: None}}}})

        if p == 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type': cnodes.update({o: ''}) # Подсчет узлов обзначающих классы индивидов

    if 'ent' in g['edict']:
        for ind in g['edict']['ent']: # Дополняем префиксами
            e = g['edict']['ent'][ind]['e']
            pfx = getprefix(g, g['edict']['ent'][ind]['ns'])
            deep_update(g['edict'], {'ent': {ind: {'pf': pfx}}})
            deep_update(g['edict'], {'cls': {e: {'pf': {pfx: None}}}})
            deep_update(g['edict'], {'pfx': {pfx: {'e': {e: None}}}})

            # ent - полные URI всех узлов
            # rls - все типы связей
            # ind - индекс по числовым суффиксам индивидов
            # cls - индекс по именам узлов без суффикосов и префиксов (совпадают с именами классов нумерованных индивидов)
            # iref - индекс по именам индивидов с числовыми суффиксами, но без префиксов
            # pfx - префикс пространства имен, сокращение для ns - префиксной части URI

    g['stat'].clear
    g['stat'].update({'triples': len(g['grf'])}) # число триплетов
    if g['stat']['triples'] > 0:
        g['stat'].update({'nodes': len(g['edict']['ent'])}) # число узлов
        g['stat'].update({'cnodes': len(cnodes)}) # число явно указанных классов
        g['stat'].update({'inodes': g['stat']['nodes'] - g['stat']['cnodes']}) # число индивидов

def bookmarksegments(g, fmt): # Определение наличия и запоминание начала и конца сегментов Базовой, Промежуточной, Расширенной Сем.С от ЭТАП4
    pos = 0
    txtend = len(g['txt'])
    if 'bmarks' in g: del g['bmarks'] # Стираем старые закладки
    if fmt != 'etalog' and fmt != 'turtle': return # Деление текста на сегменты комментариями работает только в эталоге и turtle
    else: deep_update(g, {'bmarks': {'ns': ''}})

    for line in g['txt'].splitlines(True):
        if fmt == 'turtle' and re.match(r'@*PREFIX', line):
            g['bmarks']['ns'] = append_str(g['bmarks']['ns'], line, '') # Сохраняем префиксы
        if re.match('(#|\/\/) *Basic semantic structure.*', line):
            deep_update(g, {'bmarks': {'bsems': {'start': pos, 'end': txtend}}})
            deep_update(g, {'bmarks': {'bisems': {'start': pos, 'end': txtend}}})
        if re.match('(#|\/\/) *Intermediate semantic structure.*', line):
            deep_update(g, {'bmarks': {'bsems': {'end': pos}}}) # БСемс идет перед ИСемс
            deep_update(g, {'bmarks': {'isems': {'start': pos, 'end': txtend}}})
            deep_update(g, {'bmarks': {'iesems': {'start': pos, 'end': txtend}}})
        if re.match('(#|\/\/) *Enhanced semantic structure.*', line):
            deep_update(g, {'bmarks': {'bisems': {'end': pos}}})
            deep_update(g, {'bmarks': {'isems': {'end': pos}}}) # БСемс идет перед ИСемс
            deep_update(g, {'bmarks': {'esems': {'start': pos, 'end': txtend}}})
            break
        pos = pos + len(line) # Не надо обрезать \n

def extractsegment(g, fmt, sems): # Извлечение части текста, соответствующей заданному сегменту Базовой, Промежуточной или Расширенной Сем.С от ЭТАП4
    if fmt != 'turtle' and fmt != 'etalog': return g['txt']
    if 'bmarks' in g:
        if sems in g['bmarks']:
            txt = ''
            if fmt == 'turtle' and 'ns' in g['bmarks']: txt = g['bmarks']['ns']
            txt += g['txt'][g['bmarks'][sems]['start']:g['bmarks'][sems]['end']]  # Вырезка подстроки по закладкам # g['bmarks'][sems]['start'] - g['bmarks'][sems]['end']
            return txt
        elif sems == 'all': return g['txt']
        else: # Если есть закладки, но требуемого сегмента нет - жалуемся
            if sems == 'bsems': message ('err', _("В графе не выделена базовая структура"))
            elif sems == 'isems' or sems == 'bisems' or sems == 'iesems': message ('err', _("В графе не выделена промежуточная структура"))
            else: message ('err', _("В графе не выделена запрошенная структура %s" % sems))
            return ''
    else: return g['txt']

def parserdf(g, fmt, sems, username): # Чтение RDF (разбираемый текст, формат, получаемый граф и его словарь)
    g['grf'].remove( (None, None, None) ) # Стирание старого
    bookmarksegments(g, fmt)    
    if fmt == 'etalog':
        parseetalog(extractsegment(g, fmt, sems), g['grf'], username)
        makerdfdict(g) # Строим словарь сущностей в графе. g['edict'] меняется
        if len(g['grf']) > 0: return True
    else:
        try:
           g['grf'].parse(data = extractsegment(g, fmt, sems), format = fmt)
           makerdfdict(g) # Строим словарь сущностей в графе. g['edict'] меняется
           if len(g['grf']) > 0: return True
        except rdflib.plugins.parsers.notation3.BadSyntax:
            g['grf'].remove( (None, None, None) ) # Чтобы не было частичного результата
            message('err', _("Нарушен формат RDF\n"))
    return False

def serializerdf(fmt, g): # Получить текст из хранилища rdflib
    if len(g['grf']) > 0:
        if fmt == 'etalog': return get_etalog_text(g['grf']).encode('utf-8') # Получаем str
        else: return g['grf'].serialize(format = fmt)
    return ''

def init_graphdata(): # Инициалиация структуры данных граф + метаданные
    return {'src': {}, 'nl': '', 'txt': '', 'grf': rdflib.Graph(), 'edict': {}, 'stat': {}}
    # src - записи о происхождении графа для информирования пользователя. Ключи: file app label tgt_file tgt_sent
    # nl - исходный текст на естественном языке, если есть.
    # txt - исходный текст графа, меняется только до parserdf
    # grf - хранилище троек rdflib
    # edict - словарь сущностей и связей в графе (см. makerdfdict), хранит URI и их части в виде СТРОК str
    # stat - статистические данные (см. makerdfdict)
    # * bmarks - сведения о начале и конце внутри txt сегментов базовой сем. стуктуры, если выделен комментарием, и промежуточной структуры, Присутствует только если выделены сегменты

def clear_graph(g, master): # Очистка графа + копирование префиксов из другого графа (если указан)
    g['grf'].remove( (None, None, None) )
    if master:
        for pfx, ns in master['grf'].namespaces(): g['grf'].bind(pfx, ns) # Инициализация пустого списка g и копирование в него префиксов
    g.update({'txt': ''})
    g.update({'nl': ''})
    g['src'].clear()
    g['edict'].clear()
    g['stat'].clear()
    if 'bmarks' in g: del g['bmarks']

#### Поддержка чтения формата Эталог

def appendkey(dic, key, s):
    if key not in dic:
        dic[key] = s
    else:
        dic[key] += s

def convertvar(s): # Конверсия class_1_1 (или ?class_1_1) в Class_1_1   # Переменные без цифры в конце не распознаются
#fix
    if re.match('[0-9]$', s):
        if s and s[0] == '?': s = re.sub(r'^\?', '', s)
        return s.title()
    else: return s

def converttypedec(triple): # Конверсия декларации индивида вида "Class class_1_1" (или "Class ?class_1_1") в "Class_1_1 rf:type Class"
#fix
    if len(triple) > 1 and re.match('^[A-Z]+', triple[0]):
        if re.match('^[a-z].*[0-9]$', triple[1]) or re.match('^\?.*[0-9]$', triple[1]):
            return [convertvar(triple[1]), 'type', triple[0]]
    return triple

def cleantxt(txt): # удаление комментариев и пустых строк
    ctxt = ''
    for line in txt.splitlines():
        line = re.sub(r' *//.*', '', line)
        line = line.rstrip()
        if (line): # Очищенные от пустых строк и комменариев строки
            if ctxt:
                ctxt += "\n"
            ctxt += line
    return ctxt

def parsesublocks(txt, etastruct, cref, ref): # Выделение блоков сокращенных троек
    quot = False
    if txt:
     for c in txt: # по символам
        if c == '"': # Игнорируем разделители внутри кавычек
            if not quot: quot = True
            else: quot = False

        if (c == '.' or c == ',') and not quot:
            while ref in etastruct['blk']: ref += 1 # след. незанятый номер
            cref = ref # Переход к новому блоку
        else:
            appendkey(etastruct['blk'], cref, c)  # Сбор текста скобочных и внешних блоков
    else:
        appendkey(etastruct['blk'], cref, '') # Пустой блок. Этого не должно быть
        print (_("Пустой блок %s" % cref))

def parseblocks(txt, etastruct): # Выделение скобочных блоков
    ref = 0 # Счетчик найденных блоков (только растет)
    cref = ref # Номер текущего собираемого блока
    refpath = [] # Учет вложенности скобок
    refpath.append(cref)
    quot = 0
    brblocks = {}

    if txt:
     for c in txt: # по символам
        if c == '"': # Игнорируем разделители внутри кавычек
            if quot: quot = 1
            else: quot = 0

        if c == '(' and not quot:
            refpath.append(cref) # Запомним номер родителького блока
            ref += 1   # Начат новый блок
            reflabel = "^^ref:" + str(ref) # текстовая ссылка на другой блок
            appendkey(brblocks, cref, reflabel)
            cref = ref  # Переходим к новому блоку
        elif c == ')' and not quot:
            cref = refpath.pop() # Закончен блок - возврат к предыдущему

        else:
            appendkey(brblocks, cref, c)  # Сбор текста скобочных блоков

     ref += 1
     for cref in brblocks: # Перебираем скобочные блоки и делим их на пакеты троек
        parsesublocks(brblocks[cref], etastruct, cref, ref)

def parseline(line): # Разбор одной строки на элементы
    quot = 0
    elem = 0
    buf = ''
    triple = []

    for c in line:
        if c == '"': # Игнорируем пробелы внутри кавычек
            if quot: quot = 1
            else: quot = 0
        if c == ' ' and not quot: # пробелы - разделитель
            if elem and len(buf) > 0:
                triple.append(buf) # Запомним член тройки и начнем сборку нового
                buf = ''
            elem = 0
        else: elem = 1
        if elem and not c == '\t': buf += c
    if len(buf): triple.append(buf)  # Последний элемент
    triple = converttypedec(triple) # Только декларации переменных в первлй строке. ?переменные конвертируются потом
    return triple

def parsetriplets(txt, blkref, etastruct): # Разбор одной строки на члены троек
    elems = []

    for line in txt.splitlines(): # Собираем все элементы троек в одну цепочку
        elems += parseline(line)         # Строки состоящие из одних пробелов дают пустые списки элементов и игнорируются

    if len(elems) > 0:
        head = convertvar(elems[0]) # Первый элемент, который является субъектом всех троек блока
        triplet = []
        global c
        c = 1 # отсчет позиций в тройках 0-1-2 1-2 1-2
        for e in elems:
            if c == 2:
                triplet.append(head)
                triplet.append(convertvar(e))
            if c == 3:
                triplet.append(convertvar(e))
                if not blkref in etastruct['triples']:
                    etastruct['triples'][blkref] = []
                etastruct['triples'][blkref].append([triplet[0], triplet[1], triplet[2]]) # Помещаем собранный триплет в общую структуру
                triplet.clear()
                c = 1
            c += 1

def fillnamespaces(g, etastruct, username): # Подготовка пространства имен для создаваемого графа RDF
    #g.bind('xsd', 'http://www.w3.org/2001/XMLSchema#')
    #g.bind('rdfs', 'http://www.w3.org/2000/01/rdf-schema#')
    g.bind('rdf', 'http://www.w3.org/1999/02/22-rdf-syntax-ns#')
    g.bind('common', 'http://proling.iitp.ru/ontologies/ontoetap.owl#')
    g.bind('owl', 'http://www.w3.org/2002/07/owl#')
    g.bind('md', 'http://proling.iitp.ru/ontologies/metadata.owl#')
    g.bind('football', 'http://proling.iitp.ru/ontologies/football.owl#')  # Лучше удалить
    g.bind(username, 'http://proling.iitp.ru/factbases/%s#' % username)
    for pfx, ns in g.namespaces(): # Словарик замены префиксов (etastruct['ns'])
        etastruct['ns'][str(pfx)] = str(ns)

def addprefix(e, etastruct, username): # Приписываем префиксы
    if len(e) > 0:
        if e == "Thing": return etastruct['ns']['owl'] + e
        elif e == "sameAs": return etastruct['ns']['owl'] + e
        elif e == "type": return etastruct['ns']['rdf'] + e
        elif re.search('\^\^xsd\:', e): return e # "xxx"^^xsd:string integer ... просто передаем далее в RDFObj
        elif e[0] == '"' and e[len(e)-1] == '"': # Просто закавыченная строка - угадываем тип сами
            m = unquot(e)
            if re.match('^[0-9]+$', m): return e + '^^xsd:integer'
            elif re.match('^[0-9\.\,]+$', m): return e + '^^xsd:float'
            else: return e + '^^xsd:string'
        elif re.search('\:', e): # Если получен ns префикс
            try:
                m = re.match('^(.*)\:(.*)')
                if m.group(1) in etastruct['ns']: return etastruct['ns'][m.group(1)] + m.group(2)
            except:
                return etastruct['ns']['common'] + re.sub('.*\:', '', e) 
        elif re.match('.*_[0-9]$', e): return etastruct['ns'][username] + e # индивиды в Этап
        else: return etastruct['ns']['common'] + e
    else: message('err', _("Ошибка разбора формата Эталог. Член триплета - пустая строка."))
    return e

def blockunref(e, etastruct): # Заменяем ссылку на блок первым концептом в блоке
    if re.match('\^\^ref\:', e): # Найдена ссылка на блок
        ref = int(re.sub('\^\^ref\:', '', e))
        if ref in etastruct['triples']:
            t = etastruct['triples'][ref][0]
            e = t[0]
        else: message('err', _("Ошибка разбора формата Эталог. Неверная ссылка на скобочный блок."))
    return e


def parseetalog(txt, g, username): # Анализ текста на Эталог

    etastruct = {'ref': 0, 'blk': {}, 'triples': {}, 'ns': {}} # Хранилище блоков, на которые делится текст # ref - счетчик нумерации блоков 
    fillnamespaces(g, etastruct, username)
    txt = cleantxt(txt) # текст очищен и снова собран в строку
    parseblocks(txt, etastruct)

    for blkref in etastruct['blk']: # Перебираем отдельные блоки и восстанавливаем из них триплеты. Наполняется etastruct['triples']
        #print ('BLK:', blkref, '\n', etastruct['blk'][blkref])
        parsetriplets(etastruct['blk'][blkref], blkref, etastruct)

    for blkref in etastruct['triples']: # Перебираем триплеты
        for tr in etastruct['triples'][blkref]:
            triplet = []
            for e in tr: # члены триплетов
                e = blockunref(e, etastruct) # восстанавливаем ссылки на блоки ^^ref:N -> Class
                e = addprefix(e, etastruct, username) # Добавляем префиксы и типы данных, где не было
                triplet.append(RDFobj(e))
            g.add( (triplet[0], triplet[1], triplet[2]) )

#### Поддержка формата Эталог

def extract_name(uri):
    name = uri[uri.find('#') + 1:]
    if isinstance(uri, rdflib.URIRef):
#        if name.count('_') == 2: return '?' + name[0].lower() + name[1:]
        if name.count('_') == 2: return name[0].lower() + name[1:]
        else: return name
    else: return '"' + uri + '"'

def get_sort_name(term):
    name = extract_name(term)
    parts = name.split('_')
    if len(parts) > 2 and int(parts[1]) == 0: sort_name = 'Z' + parts[0]
    else: sort_name = 'A' + parts[0]
    return sort_name + (str(int(parts[1])*10000 + int(parts[2])) if len(parts) > 2 else '')

def get_etalog_text(graph):
    expression = ''
    top_nodes, parents = calculate_heads(graph)
    top_nodes = sorted(top_nodes, key=get_sort_name)
    children = collections.defaultdict(dict)
    for child in parents:
        parent, property = parents[child]
        children[parent][child] = property
    for top_node in top_nodes:
        term_expression = get_term_expression(graph, top_node, children, 0, True, False)
        if len(expression) == 0: expression = term_expression
        else: expression += ",\n\n" + term_expression
    if len(expression) > 0: expression += ".\n"
    return expression + "\n"

def calculate_heads(graph):
    subjects = {subject for subject, property, value in graph}
    values = {value for subject, property, value in graph}
    top_nodes = subjects - values
    heads = {head: (head, 0) for head in top_nodes}
    parents = {}
    for head in top_nodes: calculate_sub_heads(graph, head, heads, parents)
    additional_top_nodes = (subjects | values) - heads.keys()
    while additional_top_nodes:
        random_head = sorted(additional_top_nodes, key=get_sort_name)[0]
        top_nodes.add(random_head)
        heads.update({random_head: (random_head, 0)})
        calculate_sub_heads(graph, random_head, heads, parents)
        additional_top_nodes = (subjects | values) - heads.keys()
    return top_nodes, parents

def calculate_sub_heads(graph, term, heads, parents):
    delta_cost = 2 + extract_name(term).count('_0_')
    for _, property, child in graph.triples((term, None, None)):
        if child not in heads or heads[child][1] > heads[term][1] + delta_cost:
            heads[child] = (heads[term][0], heads[term][1] + delta_cost)
            parents[child] = (term, property)
            calculate_sub_heads(graph, child, heads, parents)

def get_term_expression(graph, term, children, level, is_cascade, is_embedded):
    text = extract_name(term)
    has_class = False
    properties = []
    triples = list(graph.triples((term, None, None)))
    if not triples: return text
    is_local_cascade = is_cascade
    if len(triples) == 1: is_local_cascade = False
    for _, property_term, child in triples:
        if property_term == rdflib.term.URIRef('http://www.w3.org/1999/02/22-rdf-syntax-ns#type'):
            property_term = rdflib.term.URIRef('http://proling.iitp.ru/ontologies/ontoetap.owl#isA')
            if not has_class:
                text = extract_name(child) + ' ' + text
                has_class = True
        else:
            property = ''
            if child in children[term] and children[term][child] == property_term: term_string = get_term_expression(graph, child, children, level + 1, is_cascade, True)
            else: term_string = extract_name(child)
            if is_local_cascade or is_cascade and term_string.count(' ') > 1: property += "\n" + ' ' *(level * 4 + 2)
            else: property += " "
            property += extract_name(property_term)
            if '\n' in term_string: property += "\n" + ' ' * (level * 4 + 3) + term_string
            else: property += " " + term_string
            properties.append(property)

    properties.sort()
    text += ''.join(properties)
    return ('(' if is_embedded else '') + text + (('\n' if is_embedded else '') + ' ' * (level * 4 - 1) if is_cascade and '\n' in text else '') + (')' if is_embedded else '')

#### Чтение и запись файлов

def read_rdffile(inputfile): # Чтение RDF из файла
    txt = []
    try:
        f1 = open(inputfile, mode='r', encoding='utf-8-sig')
        f1.read(4)
        f1.seek(0)
    except UnicodeDecodeError:
        f1.close()
        f1 = open(inputfile, mode='r', encoding='utf-16')
    except FileNotFoundError:
        message('inf', _("Файл не найден\n%s") % inputfile)
        return ''

    for line in f1.readlines():
        line = line.rstrip('\r\n') # Удаление переносов строки (чтобы не мешал стандарт windows)
        txt.append(line)
    f1.close()
    return "\n".join(txt)

def save_rdffile(commonfile, txt): # Сохранение RDF
    f2 = open(commonfile, mode='w', newline=os.linesep)
    print(txt.decode('utf-8-sig'), file=f2)
    f2.flush()
    f2.close()

##############################################

def make_cmpdict(d, g):
    #d.clear()
    d.update({'dir': {}})
    d.update({'inv': {}})
    for s,p,o in g['grf']:
        if not s in d:
            sc = remove_indsuffix(s)
            d['dir'].update({s: sc})
            deep_update(d['inv'], {sc: {s: None}})
        if not o in d:
            oc = remove_indsuffix(o)
            d['dir'].update({o: oc})
            deep_update(d['inv'], {oc: {o: None}})

def cmp_graph_slice(g, d, sc, pcand, ocand): # Просмотреть часть графа
    for s, p, o in g['grf'].triples((None, pcand, ocand)): # Все триплеты с заданными предикатом и объектом
        if s in d['inv'][sc]: return True# если субъект в списке соответствий sc - очищенному s из grf_a
    return False

def cmp_graph_gen(g, d, sc, p, oc): # Перебрать возможные в g соответствия oc и проверить части графа g на соответствие шаблону sc p oc
    for ocand in d['inv'][oc]: # cdb так как ищем в grf_b, объекты ранообразнее, так будут более короткие циклы
        if cmp_graph_slice(g, d, sc, p, ocand): return True
    return False

def compare_rdf_ignoreind(grf_a, grf_b, commong, diff_a, diff_b): # Произвести сравнение графов игнорируя различие суффиксов индивида
    cda = {}; cdb = {}
    make_cmpdict(cda, grf_a)
    make_cmpdict(cdb, grf_b)

    for s,p,o in grf_a['grf']:
        sc = cda['dir'][s]
        oc = cda['dir'][o]
        if sc in cdb['inv'] and oc in cdb['inv']: # очищенные s и o должны быть в словаре графа b (иначе совпадения не будет)
            if not cmp_graph_gen(grf_b, cdb, sc, p, oc): diff_a['grf'].add((s, p, o))
        else: diff_a['grf'].add((s, p, o))

    for s,p,o in grf_b['grf']:
        sc = cdb['dir'][s]
        oc = cdb['dir'][o]
        if sc in cda['inv'] and oc in cda['inv']: # очищенные s и o должны быть в словаре графа a (иначе совпадения не будет)
            if not cmp_graph_gen(grf_a, cda, sc, p, oc): diff_b['grf'].add((s, p, o))
        else: diff_b['grf'].add((s, p, o))
#            # Общая часть не имеет смысла после удаления суффиксов индивидов
#            if cmp_graph_gen(grf_b, cdb, sc, p, oc): commong['grf'].add((sc, p, oc))
#            else: diff_a['grf'].add((s, p, o))

def remove_indsuffix(uri): # Удалить суффикс индивида
    p = re.match("(.*)_(\d+_\d+)$", uri)
    if not p: p = re.match("(.*)_(\d+)$", uri)
    if not p: p = re.match("(.*)_(\?[A-Z\d]*)$", uri, re.IGNORECASE)
    if p: e = RDFobj(p.group(1))
    else: e = uri
    return e

def remove_all_indsuffixes(ing, outg): # Удаление всех суффиксов индивида в графе
    clear_graph(outg, ing)
    for s,p,o in ing['grf']:
        sc = remove_indsuffix(s)
        oc = remove_indsuffix(o)
        outg['grf'].add((sc,p,oc))

def compare_rdf_eraseind(grf_a, grf_b, commong, diff_a, diff_b): # Произвести сравнение графов после стирания всех суффиксов индивидов (триплеты отличавшиеся только номерами индивидов схлопываются вместе)
    ga = init_graphdata()
    gb = init_graphdata()
    dummy = init_graphdata() # Общая часть не имеет смысла после удаления суффиксов индивидов
    remove_all_indsuffixes(grf_a, ga)
    remove_all_indsuffixes(grf_b, gb)
    compare_rdf_standard(ga, gb, dummy, diff_a, diff_b)

def compare_rdf_standard(grf_a, grf_b, commong, diff_a, diff_b): # Произвести сравнение графов
    bcache={}
    for s,p,o in grf_b['grf']:
        bcache.update({(s, p, o): None})

    for s,p,o in grf_a['grf']:
        if (s,p,o) in bcache:
            commong['grf'].add((s,p,o))
            bcache.pop((s, p, o))
        else:
            diff_a['grf'].add((s,p,o))
    for t in bcache:
        diff_b['grf'].add(t)

def compare_rdf(grf_a, grf_b, commong, diff_a, diff_b, mode): # Селектор метода сравнения
    if len(grf_a['grf']) > 0 and len(grf_b['grf']) > 0:
        print (_("Сравним. Число троек: "), len(grf_a['grf']), len(grf_b['grf']))
        clear_graph(commong, None)
        clear_graph(diff_a, grf_a)
        clear_graph(diff_b, grf_b)

        if mode == 'standard': compare_rdf_standard(grf_a, grf_b, commong, diff_a, diff_b)
        elif mode == 'eraseind': compare_rdf_eraseind(grf_a, grf_b, commong, diff_a, diff_b)
        elif mode == 'ignoreind': compare_rdf_ignoreind(grf_a, grf_b, commong, diff_a, diff_b)

        makerdfdict(diff_a) # Строим словарь сущностей в графе. g['edict'] меняется
        makerdfdict(diff_b)
        makerdfdict(commong)

        unlabeled_common_triples = getuas(grf_a, grf_b)
        labeled_common_triples = len(commong['grf'])
        uas = ( (unlabeled_common_triples/len(grf_a['grf'])) + (unlabeled_common_triples/len(grf_b['grf'])) ) /2
        las = ( (labeled_common_triples/len(grf_a['grf'])) + (labeled_common_triples/len(grf_b['grf'])) ) /2
        print ("UAS:", uas, "LAS:", las)
        commong['stat'].update({'LAS': las})
        commong['stat'].update({'UAS': uas})

    else: print(_("Попытка сравнения с пустым графом"))

def getuas(grf_a, grf_b): # Подсчет числа триплетов совпадающих без учета предиката (для UAS)
    cm = 0
    sla = {}
    slb = {}
    for s in grf_a['grf'].subjects(predicate=None, object=None): sla.update({s: {}})
    for s in grf_b['grf'].subjects(predicate=None, object=None): slb.update({s: {}})

    for s in sla:
        if s in slb: # Найден общий субъект
            ola = {}
            for o in grf_a['grf'].objects(subject=s, predicate=None): ola.update({o: {}})
            for o in grf_b['grf'].objects(subject=s, predicate=None): #olb.update({o: {}})
                if o in ola: # Найден общий объект
                    cm = cm + 1
    return cm



###################################### Интерфейс

class CompareGUI(object):

    def __init__ (self, cmpwin, grf_a, grf_b, commong, diff_a, diff_b, settings, rtparam, context): # Граф. интерфейс
        self.cmpwin = cmpwin

        if self.cmpwin.winfo_children(): # Если дочерние элементы уже есть, не надо их заново создавать
            self.refresh_gui(cmpwin, grf_a, grf_b, commong, diff_a, diff_b, settings, rtparam, context)
        else: self.draw_gui(grf_a, grf_b, commong, diff_a, diff_b, settings, rtparam)

    def draw_gui (self, grf_a, grf_b, commong, diff_a, diff_b, settings, rtparam): # Рисуем содержимое окна

        CMPOPEN = """
#define open_width 48
#define open_height 48
static unsigned char open_bits[] = {
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0xc0, 0xff, 0xff, 0xff, 0x7f, 0x00, 0xe0, 0xff, 0xff, 0xff, 0x7f, 0x00,
   0xe0, 0xff, 0xff, 0xff, 0x7f, 0x00, 0xe0, 0x00, 0x00, 0x00, 0x70, 0x00,
   0xe0, 0x00, 0x00, 0x00, 0x70, 0x00, 0xe0, 0x00, 0x00, 0x00, 0x70, 0x00,
   0xe0, 0x00, 0x00, 0x00, 0x70, 0x00, 0xe0, 0xc0, 0xff, 0xff, 0xff, 0x3f,
   0xe0, 0xc0, 0xff, 0xff, 0xff, 0x3f, 0xe0, 0xc0, 0xff, 0xff, 0xff, 0x1f,
   0xe0, 0xe0, 0x00, 0x00, 0x00, 0x1c, 0xe0, 0xe0, 0x00, 0x00, 0x00, 0x1c,
   0xe0, 0xe0, 0x00, 0x00, 0x00, 0x0e, 0xe0, 0x70, 0x00, 0x00, 0x00, 0x0e,
   0xe0, 0x70, 0x00, 0x00, 0x00, 0x0e, 0xe0, 0x70, 0x00, 0x00, 0x00, 0x07,
   0xe0, 0x38, 0x00, 0x00, 0x00, 0x07, 0xe0, 0x38, 0x00, 0x00, 0x00, 0x07,
   0xe0, 0x38, 0x00, 0x00, 0x80, 0x03, 0xe0, 0x1c, 0x00, 0x00, 0x80, 0x03,
   0xe0, 0x1c, 0x00, 0x00, 0x80, 0x03, 0xe0, 0x1c, 0x00, 0x00, 0xc0, 0x01,
   0xe0, 0x0e, 0x00, 0x00, 0xc0, 0x01, 0xe0, 0x0e, 0x00, 0x00, 0xc0, 0x01,
   0xe0, 0x0e, 0x00, 0x00, 0xe0, 0x00, 0xe0, 0x07, 0x00, 0x00, 0xe0, 0x00,
   0xe0, 0xff, 0xff, 0xff, 0xff, 0x00, 0xe0, 0xff, 0xff, 0xff, 0x7f, 0x00,
   0xc0, 0xff, 0xff, 0xff, 0x7f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
"""
        CMPEYE = """
#define eye_width 48
#define eye_height 48
static unsigned char eye_bits[] = {
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xf8, 0x1f, 0x00, 0x00,
   0x00, 0x00, 0xff, 0xff, 0x00, 0x00, 0x00, 0xe0, 0xff, 0xff, 0x07, 0x00,
   0x00, 0xf8, 0xff, 0xff, 0x1f, 0x00, 0x00, 0xfe, 0x1f, 0xf8, 0x7f, 0x00,
   0x00, 0x7f, 0x06, 0xe0, 0xfe, 0x00, 0x80, 0x1f, 0x00, 0xc0, 0xf9, 0x01,
   0xe0, 0x07, 0xe0, 0x87, 0xe3, 0x07, 0xf0, 0x03, 0xf0, 0x0f, 0xc3, 0x0f,
   0xf8, 0x00, 0xe0, 0x1f, 0x07, 0x1f, 0x7c, 0x20, 0xe0, 0x3f, 0x06, 0x3e,
   0x3e, 0x60, 0xcc, 0x3f, 0x06, 0x7c, 0x1f, 0x60, 0xfc, 0x3f, 0x06, 0xf8,
   0x1f, 0x60, 0xfc, 0x3f, 0x06, 0xf8, 0x3e, 0x60, 0xfc, 0x3f, 0x06, 0x7c,
   0x7c, 0x60, 0xfc, 0x3f, 0x06, 0x3e, 0xf8, 0xe0, 0xf8, 0x1f, 0x07, 0x1f,
   0xf0, 0xc3, 0xf0, 0x0f, 0xc3, 0x0f, 0xe0, 0xc7, 0xe1, 0x87, 0xe3, 0x07,
   0x80, 0x9f, 0x03, 0xc0, 0xf9, 0x01, 0x00, 0x7f, 0x07, 0xe0, 0xfe, 0x00,
   0x00, 0xfe, 0x1f, 0xf8, 0x7f, 0x00, 0x00, 0xf8, 0xff, 0xff, 0x1f, 0x00,
   0x00, 0xe0, 0xff, 0xff, 0x07, 0x00, 0x00, 0x00, 0xff, 0xff, 0x00, 0x00,
   0x00, 0x00, 0xf8, 0x1f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
"""

        CMPEYE1 = """
#define eye1_width 48
#define eye1_height 48
static unsigned char eye1_bits[] = {
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xf8, 0x1f, 0x00, 0x00,
   0x00, 0x00, 0xff, 0xff, 0x00, 0x00, 0x00, 0xe0, 0xff, 0xff, 0x07, 0x00,
   0x00, 0xf8, 0xff, 0xff, 0x1f, 0x00, 0x00, 0xfe, 0x1f, 0xf8, 0x7f, 0x00,
   0x00, 0x7f, 0x06, 0xe0, 0xfe, 0x00, 0x80, 0x1f, 0x80, 0xc3, 0xf9, 0x01,
   0xe0, 0x07, 0xc0, 0x83, 0xe3, 0x07, 0xf0, 0x03, 0xe0, 0x03, 0xc3, 0x0f,
   0xf8, 0x00, 0xf0, 0x03, 0x07, 0x1f, 0x7c, 0x20, 0xf8, 0x03, 0x06, 0x3e,
   0x3e, 0x60, 0xc0, 0x03, 0x06, 0x7c, 0x1f, 0x60, 0xc0, 0x03, 0x06, 0xf8,
   0x1f, 0x60, 0xc0, 0x03, 0x06, 0xf8, 0x3e, 0x60, 0xc0, 0x03, 0x06, 0x7c,
   0x7c, 0x60, 0xc0, 0x03, 0x06, 0x3e, 0xf8, 0xe0, 0xc0, 0x03, 0x07, 0x1f,
   0xf0, 0xc3, 0xc0, 0x03, 0xc3, 0x0f, 0xe0, 0xc7, 0xe1, 0x87, 0xe3, 0x07,
   0x80, 0x9f, 0x03, 0xc0, 0xf9, 0x01, 0x00, 0x7f, 0x07, 0xe0, 0xfe, 0x00,
   0x00, 0xfe, 0x1f, 0xf8, 0x7f, 0x00, 0x00, 0xf8, 0xff, 0xff, 0x1f, 0x00,
   0x00, 0xe0, 0xff, 0xff, 0x07, 0x00, 0x00, 0x00, 0xff, 0xff, 0x00, 0x00,
   0x00, 0x00, 0xf8, 0x1f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
"""

        CMPEYE2 = """
#define eye2_width 48
#define eye2_height 48
static unsigned char eye2_bits[] = {
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xf8, 0x1f, 0x00, 0x00,
   0x00, 0x00, 0xff, 0xff, 0x00, 0x00, 0x00, 0xe0, 0xff, 0xff, 0x07, 0x00,
   0x00, 0xf8, 0xff, 0xff, 0x1f, 0x00, 0x00, 0xfe, 0x1f, 0xf8, 0x7f, 0x00,
   0x00, 0x7f, 0x06, 0xe0, 0xfe, 0x00, 0x80, 0x1f, 0xe0, 0xc7, 0xf9, 0x01,
   0xe0, 0x07, 0xf0, 0x8f, 0xe3, 0x07, 0xf0, 0x03, 0x78, 0x1e, 0xc3, 0x0f,
   0xf8, 0x00, 0x38, 0x1c, 0x07, 0x1f, 0x7c, 0x20, 0x38, 0x1c, 0x06, 0x3e,
   0x3e, 0x60, 0x00, 0x1e, 0x06, 0x7c, 0x1f, 0x60, 0x00, 0x0f, 0x06, 0xf8,
   0x1f, 0x60, 0x80, 0x07, 0x06, 0xf8, 0x3e, 0x60, 0xc0, 0x03, 0x06, 0x7c,
   0x7c, 0x60, 0xe0, 0x01, 0x06, 0x3e, 0xf8, 0xe0, 0xf0, 0x1f, 0x07, 0x1f,
   0xf0, 0xc3, 0xf0, 0x1f, 0xc3, 0x0f, 0xe0, 0xc7, 0xf1, 0x9f, 0xe3, 0x07,
   0x80, 0x9f, 0x03, 0xc0, 0xf9, 0x01, 0x00, 0x7f, 0x07, 0xe0, 0xfe, 0x00,
   0x00, 0xfe, 0x1f, 0xf8, 0x7f, 0x00, 0x00, 0xf8, 0xff, 0xff, 0x1f, 0x00,
   0x00, 0xe0, 0xff, 0xff, 0x07, 0x00, 0x00, 0x00, 0xff, 0xff, 0x00, 0x00,
   0x00, 0x00, 0xf8, 0x1f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
"""

        CMPSEARCH1 = """
#define search1_width 46
#define search1_height 46
static unsigned char search1_bits[] = {
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x80, 0xff, 0x01, 0x00, 0x00, 0x00, 0xf0, 0xff, 0x0f, 0x00, 0x00,
   0x00, 0xfc, 0xff, 0x3f, 0x00, 0x00, 0x00, 0xfe, 0x81, 0x7f, 0x00, 0x00,
   0x00, 0x3f, 0x00, 0xfc, 0x00, 0x00, 0x80, 0x0f, 0x00, 0xf0, 0x01, 0x00,
   0xc0, 0x07, 0x00, 0xe0, 0x03, 0x00, 0xc0, 0x03, 0x00, 0xc0, 0x03, 0x00,
   0xe0, 0x01, 0x00, 0x80, 0x07, 0x00, 0xe0, 0x01, 0x38, 0x80, 0x07, 0x00,
   0xe0, 0x00, 0x3c, 0x00, 0x07, 0x00, 0xf0, 0x00, 0x3e, 0x00, 0x0f, 0x00,
   0xf0, 0x00, 0x3f, 0x00, 0x0f, 0x00, 0x70, 0x80, 0x3f, 0x00, 0x0e, 0x00,
   0x70, 0x00, 0x3c, 0x00, 0x0e, 0x00, 0x70, 0x00, 0x3c, 0x00, 0x0e, 0x08,
   0x70, 0x00, 0x3c, 0x00, 0x0e, 0x08, 0x70, 0x00, 0x3c, 0x00, 0x0e, 0x00,
   0x70, 0x00, 0x3c, 0x00, 0x0f, 0x00, 0xf0, 0x00, 0x3c, 0x00, 0x0f, 0x00,
   0xf0, 0x00, 0x3c, 0x00, 0x0f, 0x00, 0xe0, 0x00, 0x7e, 0x80, 0x07, 0x00,
   0xe0, 0x01, 0x00, 0x80, 0x07, 0x00, 0xe0, 0x01, 0x00, 0xc0, 0x03, 0x00,
   0xc0, 0x03, 0x00, 0xe0, 0x03, 0x00, 0xc0, 0x07, 0x00, 0xf0, 0x07, 0x00,
   0x80, 0x0f, 0x00, 0xf8, 0x0f, 0x00, 0x00, 0x3f, 0x00, 0xfe, 0x1f, 0x00,
   0x00, 0xfe, 0xc1, 0xff, 0x3f, 0x00, 0x00, 0xfc, 0xff, 0xff, 0x7f, 0x00,
   0x00, 0xf0, 0xff, 0xe7, 0xff, 0x00, 0x00, 0x80, 0xff, 0xc0, 0xff, 0x01,
   0x00, 0x00, 0x00, 0x80, 0xff, 0x03, 0x00, 0x00, 0x00, 0x00, 0xff, 0x07,
   0x00, 0x00, 0x00, 0x00, 0xfe, 0x07, 0x00, 0x00, 0x00, 0x00, 0xfc, 0x07,
   0x00, 0x00, 0x00, 0x00, 0xf8, 0x07, 0x00, 0x00, 0x00, 0x00, 0xf0, 0x07,
   0x00, 0x00, 0x00, 0x00, 0xe0, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
"""

        CMPSEARCH2 = """
#define search2_width 46
#define search2_height 46
static unsigned char search2_bits[] = {
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x80, 0xff, 0x01, 0x00, 0x00, 0x00, 0xf0, 0xff, 0x0f, 0x00, 0x00,
   0x00, 0xfc, 0xff, 0x3f, 0x00, 0x00, 0x00, 0xfe, 0x81, 0x7f, 0x00, 0x00,
   0x00, 0x3f, 0x00, 0xfc, 0x00, 0x00, 0x80, 0x0f, 0x00, 0xf0, 0x01, 0x00,
   0xc0, 0x07, 0x00, 0xe0, 0x03, 0x00, 0xc0, 0x03, 0x00, 0xc0, 0x03, 0x00,
   0xe0, 0x01, 0x00, 0x80, 0x07, 0x00, 0xe0, 0x01, 0x7e, 0x80, 0x07, 0x00,
   0xe0, 0x00, 0xff, 0x00, 0x07, 0x00, 0xf0, 0x80, 0xe7, 0x01, 0x0f, 0x00,
   0xf0, 0x80, 0xc3, 0x01, 0x0f, 0x00, 0x70, 0x80, 0xc3, 0x01, 0x0e, 0x00,
   0x70, 0x00, 0xe0, 0x01, 0x0e, 0x00, 0x70, 0x00, 0xf0, 0x00, 0x0e, 0x00,
   0x70, 0x00, 0x78, 0x00, 0x0e, 0x00, 0x70, 0x00, 0x3c, 0x00, 0x0e, 0x00,
   0x70, 0x00, 0x1e, 0x00, 0x0f, 0x00, 0xf0, 0x00, 0xff, 0x01, 0x0f, 0x00,
   0xf0, 0x00, 0xff, 0x01, 0x0f, 0x00, 0xe0, 0x00, 0xff, 0x81, 0x07, 0x00,
   0xe0, 0x01, 0x00, 0x80, 0x07, 0x00, 0xe0, 0x01, 0x00, 0xc0, 0x03, 0x00,
   0xc0, 0x03, 0x00, 0xe0, 0x03, 0x00, 0xc0, 0x07, 0x00, 0xf0, 0x07, 0x00,
   0x80, 0x0f, 0x00, 0xf8, 0x0f, 0x00, 0x00, 0x3f, 0x00, 0xfe, 0x1f, 0x00,
   0x00, 0xfe, 0xc1, 0xff, 0x3f, 0x00, 0x00, 0xfc, 0xff, 0xff, 0x7f, 0x00,
   0x00, 0xf0, 0xff, 0xe7, 0xff, 0x00, 0x00, 0x80, 0xff, 0xc0, 0xff, 0x01,
   0x00, 0x00, 0x00, 0x80, 0xff, 0x03, 0x00, 0x00, 0x00, 0x00, 0xff, 0x07,
   0x00, 0x00, 0x00, 0x00, 0xfe, 0x07, 0x00, 0x00, 0x00, 0x00, 0xfc, 0x07,
   0x00, 0x00, 0x00, 0x00, 0xf8, 0x07, 0x00, 0x00, 0x00, 0x00, 0xf0, 0x07,
   0x00, 0x00, 0x00, 0x00, 0xe0, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
"""

        CMPSEARCH = """
#define searchc_width 46
#define searchc_height 46
static unsigned char searchc_bits[] = {
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x80, 0xff, 0x01, 0x00, 0x00, 0x00, 0xf0, 0xff, 0x0f, 0x00, 0x00,
   0x00, 0xfc, 0xff, 0x3f, 0x00, 0x00, 0x00, 0xfe, 0x81, 0x7f, 0x00, 0x00,
   0x00, 0x3f, 0x00, 0xfc, 0x00, 0x00, 0x80, 0x0f, 0x00, 0xf0, 0x01, 0x00,
   0xc0, 0x07, 0x00, 0xe0, 0x03, 0x00, 0xc0, 0x03, 0x00, 0xc0, 0x03, 0x00,
   0xe0, 0x01, 0x00, 0x80, 0x07, 0x00, 0xe0, 0x01, 0x00, 0x80, 0x07, 0x00,
   0xe0, 0x00, 0x00, 0x00, 0x07, 0x00, 0xf0, 0x00, 0x00, 0x00, 0x0f, 0x00,
   0xf0, 0x00, 0x00, 0x00, 0x0f, 0x00, 0x70, 0x00, 0x00, 0x00, 0x0e, 0x00,
   0x70, 0x00, 0x00, 0x00, 0x0e, 0x00, 0x70, 0x00, 0x00, 0x00, 0x0e, 0x00,
   0x70, 0x00, 0x00, 0x00, 0x0e, 0x00, 0x70, 0x00, 0x00, 0x00, 0x0e, 0x00,
   0x70, 0x00, 0x00, 0x00, 0x0f, 0x00, 0xf0, 0x00, 0x00, 0x00, 0x0f, 0x00,
   0xf0, 0x00, 0x00, 0x00, 0x0f, 0x00, 0xe0, 0x00, 0x00, 0x80, 0x07, 0x00,
   0xe0, 0x01, 0x00, 0x80, 0x07, 0x00, 0xe0, 0x01, 0x00, 0xc0, 0x03, 0x00,
   0xc0, 0x03, 0x00, 0xe0, 0x03, 0x00, 0xc0, 0x07, 0x00, 0xf0, 0x07, 0x00,
   0x80, 0x0f, 0x00, 0xf8, 0x0f, 0x00, 0x00, 0x3f, 0x00, 0xfe, 0x1f, 0x00,
   0x00, 0xfe, 0xc1, 0xff, 0x3f, 0x00, 0x00, 0xfc, 0xff, 0xff, 0x7f, 0x00,
   0x00, 0xf0, 0xff, 0xe7, 0xff, 0x00, 0x00, 0x80, 0xff, 0xc0, 0xff, 0x01,
   0x00, 0x00, 0x00, 0x80, 0xff, 0x03, 0x00, 0x00, 0x00, 0x00, 0xff, 0x07,
   0x00, 0x00, 0x00, 0x00, 0xfe, 0x07, 0x00, 0x00, 0x00, 0x00, 0xfc, 0x0f,
   0x00, 0x00, 0x00, 0x00, 0xf8, 0x0f, 0x00, 0x00, 0x00, 0x00, 0xf0, 0x0f,
   0x00, 0x00, 0x00, 0x00, 0xe0, 0x07, 0x00, 0x00, 0x00, 0x00, 0x80, 0x03,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
"""


        CMPSEARCHBIG = """
#define search_width 48
#define search_height 48
static unsigned char search_bits[] = {
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x7e, 0x00, 0x00, 0x00, 0x00, 0xc0, 0xff, 0x07, 0x00, 0x00,
   0x00, 0xf0, 0xff, 0x1f, 0x00, 0x00, 0x00, 0xfc, 0xff, 0x7f, 0x00, 0x00,
   0x00, 0xfe, 0xff, 0xff, 0x00, 0x00, 0x00, 0xff, 0x00, 0xfe, 0x01, 0x00,
   0x80, 0x1f, 0x00, 0xf8, 0x03, 0x00, 0xc0, 0x0f, 0x00, 0xe0, 0x07, 0x00,
   0xe0, 0x07, 0x00, 0xc0, 0x07, 0x00, 0xe0, 0x03, 0x00, 0x80, 0x0f, 0x00,
   0xf0, 0x01, 0x00, 0x00, 0x1f, 0x00, 0xf0, 0x01, 0x00, 0x00, 0x1f, 0x00,
   0xf8, 0x00, 0x00, 0x00, 0x1e, 0x00, 0xf8, 0x00, 0x00, 0x00, 0x3e, 0x00,
   0x78, 0x00, 0x00, 0x00, 0x3e, 0x00, 0x7c, 0x00, 0x00, 0x00, 0x3c, 0x00,
   0x7c, 0x00, 0x00, 0x00, 0x3c, 0x00, 0x7c, 0x00, 0x00, 0x00, 0x3c, 0x00,
   0x7c, 0x00, 0x00, 0x00, 0x3c, 0x00, 0x7c, 0x00, 0x00, 0x00, 0x3c, 0x00,
   0x7c, 0x00, 0x00, 0x00, 0x3c, 0x00, 0x78, 0x00, 0x00, 0x00, 0x3c, 0x00,
   0xf8, 0x00, 0x00, 0x00, 0x3e, 0x00, 0xf8, 0x00, 0x00, 0x00, 0x3e, 0x00,
   0xf8, 0x00, 0x00, 0x00, 0x1f, 0x00, 0xf0, 0x01, 0x00, 0x00, 0x1f, 0x00,
   0xf0, 0x01, 0x00, 0x80, 0x0f, 0x00, 0xe0, 0x03, 0x00, 0xc0, 0x0f, 0x00,
   0xe0, 0x07, 0x00, 0xe0, 0x1f, 0x00, 0xc0, 0x0f, 0x00, 0xf0, 0x3f, 0x00,
   0x80, 0x3f, 0x00, 0xfc, 0x7f, 0x00, 0x00, 0xff, 0x81, 0xff, 0xff, 0x00,
   0x00, 0xfe, 0xff, 0xff, 0xff, 0x01, 0x00, 0xf8, 0xff, 0xff, 0xff, 0x03,
   0x00, 0xe0, 0xff, 0xcf, 0xff, 0x07, 0x00, 0x80, 0xff, 0x81, 0xff, 0x0f,
   0x00, 0x00, 0x00, 0x00, 0xff, 0x1f, 0x00, 0x00, 0x00, 0x00, 0xfe, 0x3f,
   0x00, 0x00, 0x00, 0x00, 0xfc, 0x3f, 0x00, 0x00, 0x00, 0x00, 0xf8, 0x7f,
   0x00, 0x00, 0x00, 0x00, 0xf0, 0x7f, 0x00, 0x00, 0x00, 0x00, 0xe0, 0x7f,
   0x00, 0x00, 0x00, 0x00, 0xc0, 0x3f, 0x00, 0x00, 0x00, 0x00, 0x80, 0x1f,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x0e, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
"""

        self.cmpwin.deiconify()
        self.cmpwin.title(_("Сравнение графов RDF"))

        self.cmpopen_icon = BitmapImage(data=CMPOPEN, foreground="black", master=self.cmpwin)
        self.cmpeye_icon = BitmapImage(data=CMPEYE, foreground="black", master=self.cmpwin)
        self.cmpeye1_icon = BitmapImage(data=CMPEYE1, foreground="black", master=self.cmpwin)
        self.cmpeye2_icon = BitmapImage(data=CMPEYE2, foreground="black", master=self.cmpwin)
        self.cmpsearch_icon = BitmapImage(data=CMPSEARCH, foreground="black", master=self.cmpwin)
        self.cmpsearch1_icon = BitmapImage(data=CMPSEARCH1, foreground="black", master=self.cmpwin)
        self.cmpsearch2_icon = BitmapImage(data=CMPSEARCH2, foreground="black", master=self.cmpwin)

        self.boxfont = tkFont.nametofont("TkDefaultFont")
        if int(self.boxfont.actual('size')) < 10: self.boxfont.configure(size=10)  # Если шрифт по-умолчанию слишком мелкий
        #font.nametofont('TkHeadingFont').configure(size = int(self.boxfont.actual('size')))
        self.cmpwin.option_add("*Font", self.boxfont)

        self.cmp_menu_bar = Menu(cmpwin, relief=tk.FLAT) # Меню
        self.cmpwin.config(menu=self.cmp_menu_bar)
        self.cmp_file_menu = Menu(self.cmp_menu_bar, tearoff=0)
        self.cmp_menu_bar.add_cascade(label=_("Файл"), menu=self.cmp_file_menu)
        self.cmp_sems_menu = Menu(self.cmp_menu_bar, tearoff=0)
        self.cmp_menu_bar.add_cascade(label=_("Структура"), menu=self.cmp_sems_menu)
        self.cmp_format_menu = Menu(self.cmp_menu_bar, tearoff=0)
        self.cmp_menu_bar.add_cascade(label=_("Формат"), menu=self.cmp_format_menu)
        self.cmp_font_menu = Menu(self.cmp_menu_bar, tearoff=0)
        self.cmp_menu_bar.add_cascade(label=_("Шрифт"), menu=self.cmp_font_menu)
        self.cmp_cmpmode_menu = Menu(self.cmp_menu_bar, tearoff=0)
        self.cmp_menu_bar.add_cascade(label=_("Режим"), menu=self.cmp_cmpmode_menu)
        self.cmp_help_menu = Menu(self.cmp_menu_bar, tearoff=0)
        self.cmp_menu_bar.add_cascade(label=_("Справка"), menu=self.cmp_help_menu)


        self.cmpvframe = Frame(cmpwin, padx=4, pady=2, width=200, height=100, relief=tk.FLAT) # Вертикальное деление (разность - общее)
        self.cmpvframe.pack(expand='y', fill='both')
#        cmpvpane = PanedWindow(cmpvframe, orient='horizontal')
        self.cmpvpane = PanedWindow(self.cmpvframe, orient='vertical')
        self.cmpdifframe = Frame(self.cmpvframe) # Два поля различий
        self.cmpvpane.add(child=self.cmpdifframe, stretch="always")
        self.cmpcommonframe = Frame(self.cmpvframe, bd=1, relief=tk.SUNKEN)
        self.cmpvpane.add(child=self.cmpcommonframe)
        self.cmpvpane.pack(fill='both', expand='y')


        self.cmpcommontoolframe = Frame(self.cmpcommonframe)
        self.cmpcommontoolframe.pack(side='top', expand='n', fill='x')
        self.cmpcommondrbutton = Button(self.cmpcommontoolframe, image=self.cmpeye_icon, text = _("Показать"), relief=tk.FLAT)
        self.cmpcommondrbutton.pack(side = 'right', padx=4)
        self.cmpcommonsrbutton = Button(self.cmpcommontoolframe, image=self.cmpsearch_icon, text = _("Поиск в результатах"), relief=tk.FLAT)
        self.cmpcommonsrbutton.pack(side = 'right', padx=4)
        self.cmpcommoninfframe = Frame(self.cmpcommontoolframe, relief=tk.FLAT)
        self.cmpcommoninfframe.pack(side='right', expand='y', fill='x', padx=4)
        self.cmpcommonsrclabel = tk.Label(self.cmpcommoninfframe, bd=1, anchor='w')
        self.cmpcommonsrclabel.pack(side='top', fill='x')
        self.cmpcommonstatus = tk.Label(self.cmpcommoninfframe, bd=1, anchor='w')
        self.cmpcommonstatus.pack(side='top', fill='x')
        self.cmpcommonbox = scrolledtext.ScrolledText(self.cmpcommonframe, font=self.boxfont)
        self.cmpcommonbox.pack(side='top', expand='y', fill='both', pady=2)
        self.cmpcommonbox.config(font=tkFont.Font(family=self.boxfont.actual('family'), size=int(self.boxfont.actual('size') * settings['fontscale'])))


        self.cmphframe = Frame(self.cmpdifframe) # Горизонтальное деление - Два текстовых поля различий
        self.cmphframe.pack(expand='y', fill='both')
        self.cmphpane = PanedWindow(self.cmphframe, orient='horizontal')
        self.cmpdifframe1 = Frame(self.cmphframe, bd=1, relief=tk.SUNKEN)
        self.cmphpane.add(child=self.cmpdifframe1, stretch="always")
        self.cmpdifframe2 = Frame(self.cmphframe, bd=1, relief=tk.SUNKEN)
        self.cmphpane.add(child=self.cmpdifframe2, stretch="always")
        self.cmphpane.pack(fill='both', expand='y')


        self.cmpdiff1toolframe = Frame(self.cmpdifframe1)
        self.cmpdiff1toolframe.pack(side='top', expand='n', fill='x')
        self.cmpdiff1drbutton = Button(self.cmpdiff1toolframe, image=self.cmpeye1_icon, text = _("Показать"), relief=tk.FLAT)
        self.cmpdiff1drbutton.pack(side = 'right', padx=2)
        self.cmpdiff1srbutton = Button(self.cmpdiff1toolframe, image=self.cmpsearch1_icon, text = _("Поиск в результатах"), relief=tk.FLAT)
        self.cmpdiff1srbutton.pack(side = 'right', padx=2)
        self.cmpdiff1ofbutton = Button(self.cmpdiff1toolframe, image=self.cmpopen_icon, text = _("Открыть"), relief=tk.FLAT)
        self.cmpdiff1ofbutton.pack(side = 'right', padx=2)
        self.cmpdiff1infframe = Frame(self.cmpdiff1toolframe, relief=tk.FLAT)
        self.cmpdiff1infframe.pack(side='right', expand='y', fill='x', padx=4)
        self.cmpdiff1srclabel = tk.Label(self.cmpdiff1infframe, anchor='w')
        self.cmpdiff1srclabel.pack(side='top', fill='x')
        self.cmpdiff1status = tk.Label(self.cmpdiff1infframe, bd=1, anchor='w')
        self.cmpdiff1status.pack(side='top', fill='x')

        self.cmpdiff1vpane = PanedWindow(self.cmpdifframe1, orient='vertical') # деление внутри поля различий 1 (тройки и список сущностей)
        self.cmpdifframe1lst = Frame(self.cmpdifframe1)
        self.cmpdifframe1tri = Frame(self.cmpdifframe1)
        self.cmpdiff1vpane.add(child=self.cmpdifframe1tri, stretch="always")
        self.cmpdiff1vpane.pack(fill='both', expand='y')
        self.cmpdiff1statree = ttk.Treeview(self.cmpdifframe1lst, columns=('ind_number'), height=20)
        self.cmpdiff1statree.pack(side='top', fill='both', expand='y')
        self.cmpdiff1box = scrolledtext.ScrolledText(self.cmpdifframe1tri, font=self.boxfont)
        self.cmpdiff1box.pack(side='top', expand='y', fill='both', pady=0)
        self.cmpdiff1box.config(font=tkFont.Font(family=self.boxfont.actual('family'), size=int(self.boxfont.actual('size') * settings['fontscale'])))


        self.cmpdiff2toolframe = Frame(self.cmpdifframe2)
        self.cmpdiff2toolframe.pack(side='top', expand='n', fill='x')
        self.cmpdiff2drbutton = Button(self.cmpdiff2toolframe, image=self.cmpeye2_icon, text = _("Показать"), relief=tk.FLAT)
        self.cmpdiff2drbutton.pack(side = 'right', padx=2)
        self.cmpdiff2srbutton = Button(self.cmpdiff2toolframe, image=self.cmpsearch2_icon, text = _("Поиск в результатах"), relief=tk.FLAT)
        self.cmpdiff2srbutton.pack(side = 'right', padx=2)
        self.cmpdiff2ofbutton = Button(self.cmpdiff2toolframe, image=self.cmpopen_icon, text = _("Открыть"), relief=tk.FLAT)
        self.cmpdiff2ofbutton.pack(side = 'right', padx=2)
        self.cmpdiff2infframe = Frame(self.cmpdiff2toolframe, relief=tk.FLAT)
        self.cmpdiff2infframe.pack(side='right', expand='y', fill='x', padx=4)
        self.cmpdiff2srclabel = tk.Label(self.cmpdiff2infframe, bd=1, anchor='w')
        self.cmpdiff2srclabel.pack(side='top', fill='x')
        self.cmpdiff2status = tk.Label(self.cmpdiff2infframe, bd=1, anchor='w')
        self.cmpdiff2status.pack(side='top', fill='x')

        self.cmpdiff2vpane = PanedWindow(self.cmpdifframe2, orient='vertical') # деление внутри поля различий 1 (тройки и список сущностей)
        self.cmpdifframe2lst = Frame(self.cmpdifframe2)
        self.cmpdifframe2tri = Frame(self.cmpdifframe2)
        self.cmpdiff2vpane.add(child=self.cmpdifframe2tri, stretch="always")
        self.cmpdiff2vpane.pack(fill='both', expand='y')
        self.cmpdiff2statree = ttk.Treeview(self.cmpdifframe2lst, columns=('ind_number'), height=20)
        self.cmpdiff2statree.pack(side='top', fill='both', expand='y')
        self.cmpdiff2box = scrolledtext.ScrolledText(self.cmpdifframe2tri, font=self.boxfont)
        self.cmpdiff2box.pack(side='top', expand='y', fill='both', pady=0)
        self.cmpdiff2box.config(font=tkFont.Font(family=self.boxfont.actual('family'), size=int(self.boxfont.actual('size') * settings['fontscale'])))

        # Команды и обработка событий
        # Команды меню
        self.cmp_file_menu.add_command(label=_("Открыть файл 1"), command=lambda: self.open_file('1', cmpwin, grf_a, grf_b, commong, diff_a, diff_b, settings, rtparam, {'boxes': [self.cmpcommonbox, self.cmpdiff1box, self.cmpdiff2box], 'src': [self.cmpcommonsrclabel, self.cmpdiff1srclabel, self.cmpdiff2srclabel], 'status': [self.cmpcommonstatus, self.cmpdiff1status, self.cmpdiff2status], 'uents': [self.cmpdiff1statree, self.cmpdiff2statree] }))
        self.cmp_file_menu.add_command(label=_("Открыть файл 2"), command=lambda: self.open_file('2', cmpwin, grf_a, grf_b, commong, diff_a, diff_b, settings, rtparam, {'boxes': [self.cmpcommonbox, self.cmpdiff1box, self.cmpdiff2box], 'src': [self.cmpcommonsrclabel, self.cmpdiff1srclabel, self.cmpdiff2srclabel], 'status': [self.cmpcommonstatus, self.cmpdiff1status, self.cmpdiff2status], 'uents': [self.cmpdiff1statree, self.cmpdiff2statree] }))
        self.cmp_file_menu.add_command(label=_("Сохранить общее"), command=lambda: self.save_file(self.cmpwin, commong, settings))
        self.cmp_file_menu.add_command(label=_("Сохранить отличия файла 1"), command=lambda: self.save_file(self.cmpwin, diff_a, settings))
        self.cmp_file_menu.add_command(label=_("Сохранить отличия файла 2"), command=lambda: self.save_file(self.cmpwin, diff_b, settings))
        self.cmp_file_menu.add_command(label=_("Выход"), command=lambda: self.app_quit())
        self.cmp_font_menu.add_command(label=_("Мелкий"), command=lambda: self.set_font(self.cmpdiff1box, self.cmpdiff2box, self.cmpcommonbox, self.boxfont, settings, 1))
        self.cmp_font_menu.add_command(label=_("Средний"), command=lambda: self.set_font(self.cmpdiff1box, self.cmpdiff2box, self.cmpcommonbox, self.boxfont, settings, 1.5))
        self.cmp_font_menu.add_command(label=_("Крупный"), command=lambda: self.set_font(self.cmpdiff1box, self.cmpdiff2box, self.cmpcommonbox, self.boxfont, settings, 2))

        cmpmodevar = tk.StringVar()
        cmpmodevar.set(rtparam['rdfcomp_cmpmode'])
        cmpmodevar.trace_add('write', lambda var, indx, mode: self.set_cmpmode(cmpmodevar.get(), cmpwin, grf_a, grf_b, commong, diff_a, diff_b, settings, rtparam, {'vpane': self.cmpvpane, 'cframe': self.cmpcommonframe, 'boxes': [self.cmpcommonbox, self.cmpdiff1box, self.cmpdiff2box], 'src': [self.cmpcommonsrclabel, self.cmpdiff1srclabel, self.cmpdiff2srclabel], 'status': [self.cmpcommonstatus, self.cmpdiff1status, self.cmpdiff2status], 'uents': [self.cmpdiff1statree, self.cmpdiff2statree] }))
        self.cmp_cmpmode_menu.add_radiobutton(label=_("Стандартный"), value='standard', variable=cmpmodevar)
        self.cmp_cmpmode_menu.add_radiobutton(label=_("Стереть номера индивидов"), value='eraseind', variable=cmpmodevar)
        self.cmp_cmpmode_menu.add_radiobutton(label=_("Игнорировать номера индивидов"), value='ignoreind', variable=cmpmodevar)

        fmtvar = tk.StringVar()
        fmtvar.set(settings['format'])
        fmtvar.trace_add('write', lambda var, indx, mode: self.set_format(self.cmpwin, self.cmpdiff1box, diff_a, self.cmpdiff2box, diff_b, self.cmpcommonbox, commong, fmtvar.get(), settings))
        for f in settings['formats']: self.cmp_format_menu.add_radiobutton(label=f, value=f, variable=fmtvar)

        semsvar = tk.StringVar()
        semsvar.set(rtparam['sems'])
        semsvar.trace_add('write', lambda var, indx, mode: self.set_sems(semsvar.get(), cmpwin, grf_a, grf_b, commong, diff_a, diff_b, settings, rtparam, {'boxes': [self.cmpcommonbox, self.cmpdiff1box, self.cmpdiff2box], 'src': [self.cmpcommonsrclabel, self.cmpdiff1srclabel, self.cmpdiff2srclabel], 'status': [self.cmpcommonstatus, self.cmpdiff1status, self.cmpdiff2status], 'uents': [self.cmpdiff1statree, self.cmpdiff2statree] }))
        self.cmp_sems_menu.add_radiobutton(label=_("Всё"), value='all', variable=semsvar)
        self.cmp_sems_menu.add_radiobutton(label=_("Только базовая"), value='bsems', variable=semsvar)
        self.cmp_sems_menu.add_radiobutton(label=_("Только промежуточная"), value='isems', variable=semsvar)
        self.cmp_sems_menu.add_radiobutton(label=_("Только расширенная"), value='esems', variable=semsvar)
        self.cmp_sems_menu.add_radiobutton(label=_("Базовая и промежуточная"), value='bisems', variable=semsvar)
        self.cmp_sems_menu.add_radiobutton(label=_("Промежуточная и расширенная"), value='iesems', variable=semsvar)

        self.cmp_help_menu.add_command(label=_("Описание"), command=lambda: self.show_help())

        self.cmpdiff1drbutton.bind('<Map>', lambda event: self.disable_unless(self.cmpdiff1drbutton, rtparam['have_graphviz']))
        self.cmpdiff2drbutton.bind('<Map>', lambda event: self.disable_unless(self.cmpdiff2drbutton, rtparam['have_graphviz']))
        self.cmpcommondrbutton.bind('<Map>', lambda event: self.disable_unless(self.cmpcommondrbutton, rtparam['have_graphviz']))
        self.cmpdiff1srbutton.bind('<Map>', lambda event: self.disable_unless(self.cmpdiff1srbutton, rtparam['have_rdfilter']))
        self.cmpdiff2srbutton.bind('<Map>', lambda event: self.disable_unless(self.cmpdiff2srbutton, rtparam['have_rdfilter']))
        self.cmpcommonsrbutton.bind('<Map>', lambda event: self.disable_unless(self.cmpcommonsrbutton, rtparam['have_rdfilter']))
        self.cmpdiff1ofbutton.config(command = lambda: self.open_file('1', cmpwin, grf_a, grf_b, commong, diff_a, diff_b, settings, rtparam, {'boxes': [self.cmpcommonbox, self.cmpdiff1box, self.cmpdiff2box], 'src': [self.cmpcommonsrclabel, self.cmpdiff1srclabel, self.cmpdiff2srclabel], 'status': [self.cmpcommonstatus, self.cmpdiff1status, self.cmpdiff2status], 'uents': [self.cmpdiff1statree, self.cmpdiff2statree] }))
        self.cmpdiff2ofbutton.config(command = lambda: self.open_file('2', cmpwin, grf_a, grf_b, commong, diff_a, diff_b, settings, rtparam, {'boxes': [self.cmpcommonbox, self.cmpdiff1box, self.cmpdiff2box], 'src': [self.cmpcommonsrclabel, self.cmpdiff1srclabel, self.cmpdiff2srclabel], 'status': [self.cmpcommonstatus, self.cmpdiff1status, self.cmpdiff2status], 'uents': [self.cmpdiff1statree, self.cmpdiff2statree] }))
        self.cmpdiff1drbutton.config(command = lambda: self.draw_graph(diff_a, rtparam))
        self.cmpdiff2drbutton.config(command = lambda: self.draw_graph(diff_b, rtparam))
        self.cmpcommondrbutton.config(command = lambda: self.draw_graph(commong, rtparam))
        self.cmpdiff1srbutton.config(command = lambda: self.call_filter_rdf(diff_a, settings, rtparam, _("Отличия графа 1"), None))
        self.cmpdiff2srbutton.config(command = lambda: self.call_filter_rdf(diff_b, settings, rtparam, _("Отличия графа 2"), None))
        self.cmpcommonsrbutton.config(command = lambda: self.call_filter_rdf(commong, settings, rtparam, _("Общая часть сравниваемых графов"), None))

        self.cmpcommonbox.bind('<<Paste>>',  self.kill_event)
        self.cmpcommonbox.bind('<Control-a>', self.box_select_all)
        self.cmpcommonbox.bind('<Control-A>', self.box_select_all)
        self.cmpcommonbox.bind('<Control-c>', self.dummy_event)
        self.cmpcommonbox.bind('<Control-C>', self.dummy_event)
        self.cmpcommonbox.bind('<Up>', self.dummy_event)
        self.cmpcommonbox.bind('<Down>', self.dummy_event)
        self.cmpcommonbox.bind('<Key>',  self.kill_event)

        self.cmpdiff1box.bind('<<Paste>>', lambda event: self.paste_text(cmpwin, self.cmpdiff1box, grf_a, grf_a, grf_b, commong, diff_a, diff_b, settings, rtparam, {'boxes': [self.cmpcommonbox, self.cmpdiff1box, self.cmpdiff2box], 'src': [self.cmpcommonsrclabel, self.cmpdiff1srclabel, self.cmpdiff2srclabel], 'status': [self.cmpcommonstatus, self.cmpdiff1status, self.cmpdiff2status], 'uents': [self.cmpdiff1statree, self.cmpdiff2statree] }))
        self.cmpdiff1box.bind('<Control-a>', self.box_select_all)
        self.cmpdiff1box.bind('<Control-A>', self.box_select_all)
        self.cmpdiff1box.bind('<Control-c>', self.dummy_event)
        self.cmpdiff1box.bind('<Control-C>', self.dummy_event)
        self.cmpdiff1box.bind('<Up>', self.dummy_event)
        self.cmpdiff1box.bind('<Down>', self.dummy_event)
        self.cmpdiff1box.bind('<Key>',  self.kill_event)

        self.cmpdiff2box.bind('<<Paste>>', lambda event: self.paste_text(cmpwin, self.cmpdiff2box, grf_b, grf_a, grf_b, commong, diff_a, diff_b, settings, rtparam, {'boxes': [self.cmpcommonbox, self.cmpdiff1box, self.cmpdiff2box], 'src': [self.cmpcommonsrclabel, self.cmpdiff1srclabel, self.cmpdiff2srclabel], 'status': [self.cmpcommonstatus, self.cmpdiff1status, self.cmpdiff2status], 'uents': [self.cmpdiff1statree, self.cmpdiff2statree] }))
        self.cmpdiff2box.bind('<Control-a>', self.box_select_all)
        self.cmpdiff2box.bind('<Control-A>', self.box_select_all)
        self.cmpdiff2box.bind('<Control-c>', self.dummy_event)
        self.cmpdiff2box.bind('<Control-C>', self.dummy_event)
        self.cmpdiff2box.bind('<Up>', self.dummy_event)
        self.cmpdiff2box.bind('<Down>', self.dummy_event)
        self.cmpdiff2box.bind('<Key>',  self.kill_event)

        self.cmpdiff1srclabel.bind("<Button-1>", lambda event: self.show_newstat(self.cmpdiff1vpane, self.cmpdifframe1lst, self.cmpdifframe1tri, self.cmpdiff1statree, diff_a, diff_b, commong, 'rdfcomp_unodes_1', settings, rtparam))
        self.cmpdiff1status.bind("<Button-1>", lambda event: self.show_newstat(self.cmpdiff1vpane, self.cmpdifframe1lst, self.cmpdifframe1tri, self.cmpdiff1statree, diff_a, diff_b, commong, 'rdfcomp_unodes_1', settings, rtparam))
        self.cmpdiff1statree.bind("<Double-1>", lambda event: self.call_filter_with_entity(self.cmpdiff1statree, diff_a, _("Уникальный узел в графе 1"), settings, rtparam))

        self.cmpdiff2srclabel.bind("<Button-1>", lambda event: self.show_newstat(self.cmpdiff2vpane, self.cmpdifframe2lst, self.cmpdifframe2tri, self.cmpdiff2statree, diff_b, diff_a, commong, 'rdfcomp_unodes_2', settings, rtparam))
        self.cmpdiff2status.bind("<Button-1>", lambda event: self.show_newstat(self.cmpdiff2vpane, self.cmpdifframe2lst, self.cmpdifframe2tri, self.cmpdiff2statree, diff_b, diff_a, commong, 'rdfcomp_unodes_2', settings, rtparam))
        self.cmpdiff2statree.bind("<Double-1>", lambda event: self.call_filter_with_entity(self.cmpdiff2statree, diff_b, _("Уникальный узел в графе 2"), settings, rtparam))

#        self.cmpwin.bind("<<Map>>", self.refresh_gui(cmpwin, grf_a, grf_b, commong, diff_a, diff_b, settings, rtparam, {'boxes': [self.cmpcommonbox, self.cmpdiff1box, self.cmpdiff2box], 'src': [self.cmpcommonsrclabel, self.cmpdiff1srclabel, self.cmpdiff2srclabel], 'status': [self.cmpcommonstatus, self.cmpdiff1status, self.cmpdiff2status], 'uents': [self.cmpdiff1statree, self.cmpdiff2statree] }))
        self.cmpwin.bind("<<Map>>", self.refresh_gui(cmpwin, grf_a, grf_b, commong, diff_a, diff_b, settings, rtparam, {'boxes': [self.cmpcommonbox, self.cmpdiff1box, self.cmpdiff2box], 'src': [self.cmpcommonsrclabel, self.cmpdiff1srclabel, self.cmpdiff2srclabel], 'status': [self.cmpcommonstatus, self.cmpdiff1status, self.cmpdiff2status] }))


######################################

    def set_sems(self, sems, cmpwin, grf_a, grf_b, commong, diff_a, diff_b, settings, rtparam, context): # Переключение между сегментами графов от ЭТАП4
        rtparam.update({'sems': sems}) # Запомним выбор
        parserdf(grf_a, settings['format'], sems, rtparam['username'])
        parserdf(grf_b, settings['format'], sems, rtparam['username'])
        compare_rdf(grf_a, grf_b, commong, diff_a, diff_b, rtparam['rdfcomp_cmpmode'])
        self.refresh_gui(cmpwin, grf_a, grf_b, commong, diff_a, diff_b, settings, rtparam, context)

    def set_font(self, box1, box2, box3, boxfont, settings, scale): # Изменяет размер шрифта для текстовых полей srcboх, cmpbox2
        settings.update({'fontscale': scale})
        fname = self.boxfont.actual('family')
        fsize = int(self.boxfont.actual('size') * scale)
        box1.config(font=tkFont.Font(family=fname, size=fsize))
        box2.config(font=tkFont.Font(family=fname, size=fsize))
        box3.config(font=tkFont.Font(family=fname, size=fsize))

    def set_format(self, cmpwin, box1, grf_a, box2, grf_b, box3, commong, f, settings): # Именить формат RDF и обновить все текстовые области
        settings.update({'format': f})
        self.box_update(cmpwin, box1, grf_a, f)
        self.box_update(cmpwin, box2, grf_b, f)
        self.box_update(cmpwin, box3, commong, f)

    def set_cmpmode(self, mode, cmpwin, grf_a, grf_b, commong, diff_a, diff_b, settings, rtparam, context): # Изменить способ сравнения графов
        if mode == 'eraseind' or mode == 'ignoreind': context['vpane'].forget(child=context['cframe'])
        else: context['vpane'].add(child=context['cframe'])
        rtparam.update({'rdfcomp_cmpmode': mode}) # Передача параметра
        compare_rdf(grf_a, grf_b, commong, diff_a, diff_b, rtparam['rdfcomp_cmpmode'])
        self.refresh_gui(cmpwin, grf_a, grf_b, commong, diff_a, diff_b, settings, rtparam, context)

    def box_select_all(self, event): # Выделить весь текст в текстовом поле
        box = event.widget
        box.tag_add(tk.SEL, 1.0, tk.END)
        box.mark_set(tk.INSERT, 1.0)
        box.see(tk.INSERT)
        return "break"

    def box_update(self, cmpwin, box, g, fmt): # Заменить текст в текстовом поле сериализацией графа
        box.delete(1.0, tk.END)
        cmpwin.update()
        box.insert(1.0, serializerdf(fmt, g))

    def srclabel_update(self, g, label): # Обновление строки источника данных графа
        if 'file' in g['src']: label.config(text=append_str(_("Файл:"), g['src']['file'], ' '), bg=cmpwin.cget("bg"))
        elif 'app' in g['src']: label.config(text=append_str(_("Получено от"), g['src']['app'], ' '), bg='green yellow')
        elif 'label' in g['src']: label.config(text=g['src']['label'], bg=cmpwin.cget("bg"))
        else: label.config(text=_("Нет данных"), bg='red')

    def statlabel_update(self, g, srcg, label):
        stattxt = ''
        if 'stat' in srcg and 'triples' in srcg['stat'] and srcg['stat']['triples'] > 0:
            if 'stat' in g and 'triples' in g['stat']: stattxt += "Из "
            stattxt += _("%s триплетов ") % str(srcg['stat']['triples'])
            if 'stat' in g and 'triples' in g['stat']: stattxt += "выделено "
            else: stattxt + 'ничего не найдено'

        if 'stat' in g and 'triples' in g['stat']:
            stattxt += _("%s триплетов") % str(g['stat']['triples'])
            if 'nodes' in g['stat']: stattxt += _(", %s узлов ") % str(g['stat']['nodes'])
            if 'inodes' in g['stat']: stattxt += _(" (%s индивидов / %s классов)") % (str(g['stat']['inodes']), str(g['stat']['cnodes']))
            if 'LAS' in g['stat']: stattxt += "	LAS: %s" % str(g['stat']['LAS'])
            if 'UAS' in g['stat']: stattxt += "   UAS: %s" % str(g['stat']['UAS'])

        if stattxt == '': stattxt = _("Пусто")
        label.config(text = stattxt)

    def paste_text(self, cmpwin, box, g, grf_a, grf_b, commong, diff_a, diff_b, settings, rtparam, context): # Получение текста путем вставки из буфера
        g.update({'txt': box.clipboard_get()}) # содержимое буфера в g['txt'], далее оно будет порезано на сегменты (если они там есть) в parserdf
        parsed = parserdf(g, settings['format'], rtparam['sems'], rtparam['username'])
        if parsed:
            if len(g['src']) == 0: g['src'].update({'label': _("Вставлено из буфера")})
        else:
            clear_graph(g, None)
        compare(settings, rtparam, context, grf_a, grf_b, commong, diff_a, diff_b)
        return "break"

    def refresh_gui(self, cmpwin, grf_a, grf_b, commong, diff_a, diff_b, settings, rtparam, widgets): # Обновление окна после сравнения графов
        commong['src'].update({'label': _("Совпадающая часть двух графов")})
        diff_a['src'].update(grf_a['src']) # Копируем метаданные о источнике grf_a в diff_a
        diff_b['src'].update(grf_b['src'])

        if 'src' in widgets: # обновим строку источника у всех 3-х текстовых полей
            for (label, g) in zip(widgets['src'], (commong, diff_a, diff_b)): self.srclabel_update(g, label)
        if 'status' in widgets: # обновим статистику троек
            for (label, g, srcg) in zip(widgets['status'], (commong, diff_a, diff_b), ({'grf': []}, grf_a, grf_b)): self.statlabel_update(g, srcg, label)
        if 'boxes' in widgets: # Обновим содержимое полей
            for (box, g) in zip(widgets['boxes'], (commong, diff_a, diff_b)): self.box_update(cmpwin, box, g, settings['format'])
        if 'uents' in widgets: # Обновление списков уникальных узлов
            for (tree, g, altg, commong) in zip(widgets['uents'], (diff_a, diff_b), (diff_b, diff_a), (commong, commong)): self.uent_update(tree, g, altg, commong)

    def disable_unless(self, widget, flag): # Отключение кнопок при отсутствии внешних инструментов
        if not flag: widget['state'] = DISABLED

    def dummy_event(self, event): # Позволить виджету обработать кнопку
        return

    def kill_event(self, event): # Отменить обработку события
        return "break"

    def app_quit(self): # Выход с закрытием окна
        self.cmpwin.quit()
        self.cmpwin.destroy()
        exit()

    def import_rdffile(self, g, inputfile, fmt, sems, username): # Загрузка RDF и обновление записи о его происхождени
        clear_graph(g, None)
        g.update({'txt': read_rdffile(inputfile)})
        parsed = parserdf(g, fmt, sems, username)
        if parsed: g['src'].update({'file': re.sub(r'.*[\/\\]', '', inputfile)})  # Сократим имя, чтобы помещалось в окошке

    def open_file(self, dst, cmpwin, grf_a, grf_b, commong, diff_a, diff_b, settings, rtparam, context): # Открыть исходный RDF через меню
        #TMP try:
            inputfile = filedialog.askopenfile(parent=cmpwin, title=_("RDF-граф для сравнения № %s") % dst, mode="r", filetypes=(("RDF", ("*.rdf", "*.ttl", "*.n3", "*.txt")), (_("Любые файлы"), "*.*")) ).name
            if dst == '1': self.import_rdffile(grf_a, inputfile, settings['format'], rtparam['sems'], rtparam['username'])
            elif dst == '2': self.import_rdffile(grf_b, inputfile, settings['format'], rtparam['sems'], rtparam['username'])
            compare(settings, rtparam, context, grf_a, grf_b, commong, diff_a, diff_b)
        #except: pass

    def save_file(self, cmpwin, g, settings): # Открыть исходный RDF через меню
        try:
            if len(g['grf']) == 0:
                message('inf', _("Нечего сохранять. Перед записью в файл следует получить результат сравнения двух графов."))
                return
            outfile = filedialog.asksaveasfile(parent=cmpwin, title=_("Сохранение результата сравнения в RDF"), mode="w", defaultextension=".txt", ).name
            save_rdffile(outfile, serializerdf(settings['format'], g))
        except: pass

#########################

    def call_filter_rdf(self, g, settings, rtparam, msg, filterq): # Вызов rdfiter с передачей одного из результатов поиска
        if len(g['grf']) > 0:
            rtparam.update({'rdfilter_guimode': True})
            g['src'].update({'app': _('RDFCompare (%s)') % msg})
            if 'file' in g['src']: del g['src']['file']
            rdfilter.makerdfdict(g)
            rdfilter.filter_rdf(settings, rtparam, {}, g, None, filterq) # Задать условия и выполнить поиск. Альт. точка входа
        else: message ('inf', _("В пустом результате нечего искать."))

    def draw_graph(self, g, rtparam): # Графический вывод графа
        if len(g['grf']) > 0:
            dot = Digraph(comment=_('Результат поиска'), format='jpg') # https://www.graphviz.org/doc/info/attrs.html
            if (len(g['grf']) > 50): dot.engine = 'dot'
            else: dot.engine = 'sfdp' # dot, neato, fdp, sfdp
            dot.overlap = 'prism1000'
            dot.overlap_scaling = '1000'
            dot.overlap_shrink = '1000'
            dot.splines = 'curved'
            dot.esep = '+500,300'
            for s,p,o in g['grf']:  # Перебор всех триплетов
                ns, pfx, e, i = parseURI(s);
                if i: sname = "_".join([e, i])
                else: sname = e
                dot.node(sname, sname, style='filled') #, fillcolor = node_color(s, i, o))

                ns, pfx, e, i = parseURI(o);
                if i: oname = "_".join([e, i])
                else: oname = e
                dot.node(oname, oname, style='filled') #, fillcolor = node_color(o, i, s))

                ns, pfx, e, i = parseURI(p); pn = e
                dot.edge(sname, oname, label = pn, fontsize='9')
            dotfilename = append_str('rdfsearchres', rtparam['username'], '-')
            dot.render('rdfilter_tmp/'+dotfilename+'.gv', view=True, cleanup=True)
        else: message ('inf', _("Нечего рисовать."))


    def show_newstat(self, pane, lstframe, triframe, tree, g, altg, commong, param, settings, rtparam): # Показать статистику уникальных узлов
        if rtparam.get(param) or rtparam.get('rdfcomp_cmpmode') == 'eraseind':
            rtparam.update({param: False})
            pane.forget(child=lstframe)
        else:
            rtparam.update({param: True})
            self.uent_update(tree, g, altg, commong)
            pane.add(child=lstframe, stretch="always", before=triframe)

    def list_unique_entities(self, lst, g, altg, commong): # Составить список узлов, которые есть только в g
        c = None
        if 'ent' in g['edict'] and 'ent' in altg['edict']: # Всегда есть
            c = 0
            for ent in g['edict']['ent']:
                if not ent in altg['edict']['ent']:
                    if ('ent' in commong['edict'] and not ent in commong['edict']['ent']) or not 'ent' in commong['edict']: # ent в commong может отсутствовать в режиме игнорирования номеров, где нет нормальной общей части
                        ns, pfx, e, i = parseURI(ent)
                        deep_update(lst, {e: {ent: ''}})
                        c += 1
        return c

    def uent_update(self, tree, g, altg, commong): # Составление списка уникальных для графа узлов. Требуется результат сравнения двух разных графов
        tree.delete(*tree.get_children()) # Стереть все
        tree.heading('#0', text=_("Уникальные узлы"), anchor='w')
        tree.heading('ind_number', text=_("Индивидов в группе"), anchor='w')
        ulst = {}
        uentnum = self.list_unique_entities(ulst, g, altg, commong)
        if not uentnum is None: # Есть уникальные узлы
            tree.heading('#0', text=_("Уникальные узлы  (Всего - %s)" % uentnum), anchor='w')
            for c in ulst:
                tree.insert("", tk.END, c, text=c, values=(len(ulst[c])))
                for i in ulst[c]:
                    shorti = re.sub('.*#', '', i)
                    n=0
                    while tree.exists(i):
                        n+=1; i=append_str(i, str(n), '_')
                    tree.insert(c, tk.END, i, text=shorti, values=('')) #values=('', i))

    def call_filter_with_entity(self, tree, g, msg, settings, rtparam): # вызов rdfilter с запросом найти связи выбранной сущности
        item = tree.selection()[0]
        self.call_filter_rdf(g, settings, rtparam, msg, {'cls': '+'+tree.item(item,"text")} ) # последний параметер filterq - хэш с готовыми строками запросов, которые вставляются в UI rdfilter

    def show_help(self): # Показать справку
        self.cmphelpwin = tk.Toplevel(master = self.cmpwin)
        self.cmphelpwin.title(_("Справка"))
        #self.cmphelpwin.attributes('-topmost', 1)
        self.cmphelpbox = scrolledtext.ScrolledText(self.cmphelpwin, wrap=tk.WORD)
        self.cmphelpbox.config(width=100, height=40)
        self.cmphelpbox.pack(fill='both', expand='y')
        self.cmphelpbox.bind('<Key>',  self.kill_event)
        self.cmphelpbox.insert(1.0, _("""Сравнение RDF 1.3 beta"""), 'title', _("""


    С помощью этого инструмента можно сравнить два графа в формате RDF и увидеть, какие триплеты уникальны для каждого из них, а какие являются общими. 
"""), 'body', _("""

Ввод и вывод графов
"""), 'header', _("""
    Графы можно: 
    1. Загрузить из текстовых файлов.  Используйте ключи командной строки -a и -b или меню "Файл/Открыть файл 1(2)"
    2. Вставить текст RDF из буфера обмена.  Для этого нужно щелкнуть мышкой левое или правое поле сравнения и нажать комбинацию клавиш Ctrl+V. Вставляемый текст должен соответствовать выбранному в меню Формат способу записи RDF.
    3. Получить из RDFilter с помощью кнопки сравнения графов.  При этом будет получен только один граф. Второй нужно будет открыть из файла или вставить из буфера обмена.

    Результаты сравнения можно сохранять в файл с помощью меню "Файл/Сохранить общее (отличия файла 1 / отличия файла 2)" и показать графически с помощью кнопок "Глаз". Их также можно копировать в буфер обмена с помощью Ctrl+C.
"""), 'body', _("""

Форматы графов RDF
"""), 'header', _("""
    Доступные форматы RDF-текста: turtle, nt, xml, etalog. Формат etalog отличается удобством чтения и используется в лингвистическом процессоре ЭТАП4.

    Граф семантической структуры предложения, которую строит семантический анализатор ЭТАП-4, может быть разделен с помощью специального комментария в тексте RDF на сегменты Базовой, Промежуточной и Расширенной семантической структуры. При наличии такого деления в загруженном тексте RDF вы можете выбрать желаемый сегмент с помощью меню "Структура". Полная структура используется по умолчанию и включает в себя все имеющиеся в графе триплеты. При выборе Базовой или Промежуточной структуры программа выделит часть загруженного графа и будет игнорировать остальные данные.

    Имена узлов графа (URI) могут содержать необязательный суффикс, отделяемый символом '_'. Внутри могут быть два числа, разделенных тем же символом. Такой суффикс может использоваться для обозначения разных индивидов одного класса и имеет специальные поля выбора справа от полей ввода онтологического класса узла.
"""), 'body', _("""

Интерфейс
"""), 'header', _("""
    Окно программы состоит из 3х частей: зоны отличий 1-го графа, зоны отличий 2-го графа и общей зоны, где отображаются триплеты общие для всех графов. Над каждым из трех текстовых полей имеется область информации, где можно увидеть источник стравниваемого графа (файл, программа) и число триплетов в исходном графе и выделенных при сравнении.

    Заголовки текстовых полей также содержат кнопки открытия файла (папка), поиска в содержимом поля (лупа) и визуализации содержимого поля (глаз).
"""), 'body', ("""  * Функции поиска и визуализации требуют наличия модуля RDFilter и внешней программы рисования графов graphviz.
"""), 'inlinex', ("""

Список уникальных узлов
"""), 'header', ("""
    Щелчок левой кнопкой мыши на заголовке графа 1 или 2 открывает древовидный список всех индивидов, которые уникальны для выбранного графа. Двойной щелчок на любом из них открывает окно поиска выделенного индивида в сравниваемом графе.
"""), 'body', ("""

Статистика
"""), 'header', ("""
    В заголовке общей зоны внизу также можно увидеть общестатистические параметры LAS / UAS (Labeled / Unlabeled Attacment Score), которые отражают степень подобия графов.
    LAS показывает соотношение числа общих для обоих графов триплетов к различающимся с учетом типа связи между узлами (предиката). UAS - без учета типа связи. При полном совпадении они равны 1.0, а при отсутствии совпадающих триплетов - 0. Совпадение LAS и UAS означает, что в сравниваемых графах 1 и 2 нет триплетов, которые отличаются только именем предиката.
"""), 'body', ("""

Режимы сравнения
"""), 'header', ("""
    Есть три режима сравнения:
    - Стандартный
    - Стереть номера индивидов
    - Игнорировать номера индивидов.

    Стандартный режим сравнивает все триплеты двух графов и делит их на те, которые есть в обоих графах, и те, которые есть только в одном из двух графов.

    Режим стирания номеров индивидов доступен для графов, которые содержат суффиксы индивидов. В этом режиме перед сравнением все суффиксы полностью удаляются. При этом отличающиеся лишь суффиксами триплеты сольются воедино, и сравниваемые реально графы будут отличаться от загруженных.

    Режим игнорирования номеров индивидов сохраняет содержимое исходных графов, но считает отличающиеся только суффиксами триплеты одинаковыми. Результат такого сравнения отличается от результата стирания всех суффиксов.

    В случае стирания или игнорирования различий индивидов нельзя корректно выделить общую часть графов. Поэтому, общая зона в этих режимах скрывается.
     
"""), 'body', ("""
Программа доступна на условиях лицензии GPLv3 или новее.
Разработка - Вячеслав Диконов: sdiconov@mail.ru
Поддержка формата RDF Эталог - Иван Рыгаев: irygaev@jent.ru
"""), 'sign')
        self.cmphelpbox.tag_add('body', '2.0', tk.END)
        self.cmphelpbox.tag_config('title', justify=tk.CENTER, font=('Times', 24, 'bold'))
        self.cmphelpbox.tag_config('header', font=('Times', 14, 'bold'))
        self.cmphelpbox.tag_config('body', font=('Times', 12))
        self.cmphelpbox.tag_config('inlinex', font=('Times', 12, 'italic'))
        self.cmphelpbox.tag_config('example', font=('Courier', 12))
        self.cmphelpbox.tag_config('sign', font=('Times', 12, 'italic'))


######################################

def compare(settings, rtparam, context, grf_a, grf_b, commong, diff_a, diff_b): # Получить или инициализировать графы, запустить сравнение, показать окно. Альт. точка входа. Вызывается из rdfilter
    if grf_a is None: grf_a = init_graphdata() # хранилище rdflib для исходного графа 1
    if grf_b is None: grf_b = init_graphdata() # хранилище rdflib для исходного графа 2
    if commong is None: commong = init_graphdata() # хранилище rdflib для результатов сравнения
    else: clear_graph(commong, grf_a)
    if diff_a is None: diff_a = init_graphdata() # хранилище rdflib для результатов сравнения - разница 1
    else: clear_graph(diff_a, grf_a)
    if diff_b is None: diff_b = init_graphdata() # хранилище rdflib для результатов сравнения - разница 2
    else: clear_graph(diff_b, grf_b)
    fill_missing_settings(settings, rtparam) # Если нет каких-то параметров - подставим умолчания

    compare_rdf(grf_a, grf_b, commong, diff_a, diff_b, rtparam['rdfcomp_cmpmode'])

    if 'rdfcomp_guimode' in rtparam and rtparam['rdfcomp_guimode']: # Есть запрос на рисование окна при вызове процедуры сравнения.
        global cmpwin # После входа из другого скрипта важно, чтобы переменная не терялась при повторном вызове compare
        try: cmpwin.winfo_children()  # winfo_exists недостаточно
        except: cmpwin = tk.Toplevel() # После входа в compare из другого скрипта cmpwin надо создать
        compare_gui = CompareGUI(cmpwin, grf_a, grf_b, commong, diff_a, diff_b, settings, rtparam, context) # Рисуем или перерисовываем окно

    # context - списки элементов интерфейса перерисовываемых после сравнения


def cmdline(argv, settings): # Разбор командной строки
   inputfile_a = ''; inputfile_b = ''
   commonfile = ''; diffile_a = ''; diffile_b = ''

   try:
       opts, args = getopt.getopt(argv,"ha:b:c:d:e:f:",["file1=","file2=","common=","diff1=","diff2=","format="])
   except getopt.GetoptError:
       print (_('rdfcomp.py -a <файл 1> -b <файл 2> -c <файл совпадений> -d <файл разницы 1> -e <файл разницы 2> -f <формат>'))
       sys.exit(2)
   for opt, arg in opts:
       if opt in ("-h", "--help"):
           print (_('rdfcomp.py -a <файл 1> -b <файл 2> -c <файл совпадений> -d <файл разницы 1> -e <файл разницы 2> -f <формат>'))
           sys.exit()
       elif opt in ("-a", "--file1"):
           inputfile_a = arg
           print (_('Файл 1: '), inputfile_a)
       elif opt in ("-b", "--file2"):
           inputfile_b = arg
           print (_('Файл 2: '), inputfile_b)
       elif opt in ("-c", "--common"):
           commonfile = arg
           print (_('Файл совпадений: '), commonfile)
       elif opt in ("-d", "--diff1"):
           diffile_a = arg
           print (_('Файл разницы 1: '), diffile_a)
       elif opt in ("-e", "--diff2"):
           diffile_b = arg
           print (_('Файл разницы 2: '), diffile_b)
       elif opt in ("-f", "--format"):
           settings.update({'format': arg})
           print (_('Формат RDF: '), settings['format'])
   return inputfile_a, inputfile_b, commonfile, diffile_a, diffile_b


if __name__ == "__main__": # Запуск в качестве автономного приложения (вызов из rdfilter сразу переходит к процедуре compare)
    grf_a = init_graphdata(); grf_b = init_graphdata(); commong = init_graphdata(); diff_a = init_graphdata(); diff_b = init_graphdata()
    settings = {}; rtparam = {}
    init_settings(settings, rtparam)
    inputfile_a, inputfile_b, commonfile, diffile_a, diffile_b = cmdline(sys.argv[1:], settings)
    parsed_a = False
    if inputfile_a:
        grf_a.update({'txt': read_rdffile(inputfile_a)}) # Прочитать RDF 1 из файла
        parsed_a = parserdf(grf_a, settings['format'], rtparam['sems'], rtparam['username'])
        if parsed_a: grf_a['src'].update({'file': inputfile_a})
    parsed_b = False
    if inputfile_b:
        grf_b.update({'txt': read_rdffile(inputfile_b)}) # Прочитать RDF 2 из файла
        parsed_b = parserdf(grf_b, settings['format'], rtparam['sems'], rtparam['username'])
        if parsed_b: grf_b['src'].update({'file': inputfile_b})

    if parsed_a and parsed_b and (commonfile or diffile_a or diffile_b): # Если распознано 2 графа и есть параметры вывода, то
        rtparam.update({'rdfcomp_guimode': False})
        compare(settings, rtparam, {}, grf_a, grf_b, commong, diff_a, diff_b) # Произвести сравнение без граф. окна
        if commonfile: save_rdffile(commonfile, serializerdf(settings['format'], commong)) # сохранить общее в файл
        if diffile_a: save_rdffile(diffile_a, serializerdf(settings['format'], diff_a)) # сохранить разное в файл
        if diffile_b: save_rdffile(diffile_b, serializerdf(settings['format'], diff_b)) # сохранить разное в файл

    elif cmpwin: # Если параметров недостаточно и есть поддержка графики
        rtparam.update({'rdfcomp_guimode': True})
        compare(settings, rtparam, {}, grf_a, grf_b, commong, diff_a, diff_b) # Произвести сравнение в графическом режиме
        if autonomous: cmpwin.mainloop() # Только при самостоятельном запуске. После вызова из другого скрипта используется mainloop того скрипта.

    else: print (_("Недостаточно параметров и нет поддержки графического интерфейса."))
